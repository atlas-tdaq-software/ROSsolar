/****************************************************************/
/* File: dolar.h                                                */
/* Author: Stefan Haas, CERN EP-ATE                             */
/* 15-Jun-04 STH created                                        */
/* 10-Jun-13 M. Joos: support for 12 channel F/W added          */
/*                                                              */
/****************************************************************/

#ifndef DOLAR_H
#define DOLAR_H

#define DOLAR_VID                0x10dc 
#define DOLAR_DID                0x0018
#define DOLAR_FIRMWARE_VERSION   0x23

#define DOLAR12_VID              0x10dc  
#define DOLAR12_DID              0x019c  
#define DOLAR12_FIRMWARE_VERSION 0x00  

#define PROG_NUM_OF_WORDS  256 // number of program memory entries
#define TRANS_NUM_OF_WORDS 64  // number of entries in the lookup table

#define PROG_ADDR_MAX_OFFSET 8
#define PROG_ADDR_MAX_FIELD  0xff

#define PROG_ADDR_INIT_OFFSET 22
#define PROG_ADDR_INIT_FIELD  0xff

// Operation control register bits
#define RESET 		0x01000
#define PROG_MODE 	0x02000
#define STOP_MODE 	0x00000
#define RUN_MODE       	0x04000
#define FRAME_MODE      0x08000
#define STEP_MODE       0x0C000
#define STEP_FORWARD    0x10000

// OPCODES
//shifts 
#define OPCODE_SHIFT_LEFT 26
//simple
#define OPCODE_END_FRAME  0x1
#define OPCODE_CONTROL    0x2
#define OPCODE_RAND       0x3
#define OPCODE_COUNTER    0x4
#define OPCODE_LOAD       0x5
#define OPCODE_ALT        0x6
#define OPCODE_CONST      0x7
#define OPCODE_LOAD_INIT  0x8
#define OPCODE_SHIFT      0x9

// CODES
// shifts
#define CODE_SHIFT_LEFT         13
#define CODE_COUNTER_SHIFT_LEFT 8
#define CODE_ALT_SHIFT_LEFT     4
#define CODE_DIRECTION_SHIFT    12
// fields
#define ADDR_FIELD     0x3f
#define COUNTER_FIELD  0x1f
// simple
#define CODE_END_FRAME 0x0
#define CODE_RANDOM    0x0

// macros
#define CODE_CONTROL(n)    ((n) & ADDR_FIELD)
#define CODE_CONST(n)      ((n) &  ADDR_FIELD)
#define CODE_LOAD(n,m)     ((((n) & COUNTER_FIELD) << CODE_COUNTER_SHIFT_LEFT) | ((m) & ADDR_FIELD))
#define CODE_LOAD_INIT(n,m) ((((n) & COUNTER_FIELD) << CODE_COUNTER_SHIFT_LEFT) | ((m) & ADDR_FIELD))
#define CODE_COUNTER(n,m)  ((((n) & COUNTER_FIELD) << CODE_COUNTER_SHIFT_LEFT) | ((m) & ADDR_FIELD))
#define CODE_ALT(n,m)      ((((n) & 0xf) << CODE_ALT_SHIFT_LEFT) | ((m) & 0xf))
#define CODE_SHIFT(n,m)    ((((n) & 0x1) << CODE_DIRECTION_SHIFT) | ((m) & ADDR_FIELD)) 

// NUMBER
// fields
#define NUMBER_FIELD 0x1fff
//macros
#define NUMBER(n)       ((n) & NUMBER_FIELD)

//#define MEM_REGISTER(n) (((n) & PROG_ADDR_MAX_FIELD) << PROG_ADDR_MAX_OFFSET ) 
#define MEM_REGISTER(n,m) (((m) & PROG_ADDR_INIT_FIELD) << PROG_ADDR_INIT_OFFSET ) |(((n) & PROG_ADDR_MAX_FIELD) << PROG_ADDR_MAX_OFFSET ) 

// All
#define END            (OPCODE_END_FRAME << OPCODE_SHIFT_LEFT)
#define CONTROL(n,m)   (OPCODE_CONTROL << OPCODE_SHIFT_LEFT | CODE_CONTROL(n) << CODE_SHIFT_LEFT | NUMBER(m))
#define RANDOM(n)      (OPCODE_RAND << OPCODE_SHIFT_LEFT | NUMBER(n))
#define COUNTER(n,m,p) (OPCODE_COUNTER << OPCODE_SHIFT_LEFT | CODE_COUNTER(n,m) << CODE_SHIFT_LEFT | NUMBER(p))
#define LOAD(n,m)      (OPCODE_LOAD << OPCODE_SHIFT_LEFT | CODE_LOAD(n,m) << CODE_SHIFT_LEFT | NUMBER(1))
#define LOAD_INIT(n,m) (OPCODE_LOAD_INIT << OPCODE_SHIFT_LEFT | CODE_LOAD_INIT(n,m) << CODE_SHIFT_LEFT | NUMBER(1))
#define ALT(n,m,p)     (OPCODE_ALT << OPCODE_SHIFT_LEFT | CODE_ALT(n,m) << CODE_SHIFT_LEFT | NUMBER(p))
#define CONST(n,m)     (OPCODE_CONST << OPCODE_SHIFT_LEFT | CODE_CONST(n) << CODE_SHIFT_LEFT | NUMBER(m))
#define SHIFT(n,m,p)   (OPCODE_SHIFT << OPCODE_SHIFT_LEFT | CODE_SHIFT(n,m) << CODE_SHIFT_LEFT | NUMBER(p))

#define BLK_CNT_MASK   0xFFFFFF

//types
typedef volatile u_int vu_int;
typedef u_int prog_array [PROG_NUM_OF_WORDS];
typedef u_int trans_array [TRANS_NUM_OF_WORDS];

	  
typedef struct
{
  vu_int ctrl;            // 0x000 
  vu_int sts;             // 0x004 
  vu_int wrdcnt;          // 0x008 
  vu_int blkcnt;          // 0x00c 
  vu_int freq;            // 0x010 
  vu_int mem;             // 0x014 
  vu_int prog;            // 0x018 
  vu_int trans;           // 0x01c 
  vu_int reserved[8];     // 0x020 - 0x3F
} slidas_T;


typedef struct
{
  vu_int opctrl1;         // 0x000 
  vu_int opstat1;         // 0x004 
  vu_int cardtemp;        // 0x008 
  vu_int opctrl2;         // 0x00C 
  vu_int opctrl3;         // 0x010 
  vu_int opstat2;         // 0x014 
  vu_int opstat3;         // 0x018 
  vu_int reserved1[9];    // 0x01C - 0x003F
  slidas_T slidas[12];
} T_dolar_regs;

#endif
