/************************************************************************/
/*									*/
/* File: quest.h							*/
/*									*/
/* This is the public header file for QUEST library			*/
/*									*/
/* 10. Feb. 04  MAJO  created						*/
/*									*/
/************ C 2004 - The software with that certain something *********/

#ifndef _QUEST_H 
#define _QUEST_H

#include <linux/types.h>
#include "quest_common.h"

/*******/
/*Types*/
/*******/
typedef unsigned int   QUEST_ErrorCode_t;


/************/
/*Prototypes*/
/************/
#ifdef __cplusplus
extern "C" {
#endif


/*******************************/ 
/*Official functions of the API*/
/*******************************/ 
QUEST_ErrorCode_t QUEST_Open(void);
QUEST_ErrorCode_t QUEST_Close(void);
QUEST_ErrorCode_t QUEST_Info(QUEST_info_t *info);
QUEST_ErrorCode_t QUEST_Init(QUEST_config_t *config);
QUEST_ErrorCode_t QUEST_Reset(u_int card);
QUEST_ErrorCode_t QUEST_LinkReset(u_int channel);
QUEST_ErrorCode_t QUEST_LinkStatus(u_int channel, QUEST_status_t *status);
QUEST_ErrorCode_t QUEST_PagesIn(QUEST_in_t *fifoin);
QUEST_ErrorCode_t QUEST_Flush(u_int channel);
QUEST_ErrorCode_t QUEST_InFree(u_int channel, u_int *nfree);

/******************************/
/* Internal service functions */
/******************************/
QUEST_ErrorCode_t QUEST_err_get(err_pack err, err_str pid, err_str code);
int quest_in_fifo_push(u_int channel, u_int data1, u_int data2);

#ifdef __cplusplus
}
#endif

#endif
