/************************************************************************/
/*									*/
/* File: quest_common.h							*/
/*									*/
/* This is the common public header file for QUEST library		*/
/* and driver								*/
/*									*/
/*10. Feb. 04   MAJO  created						*/
/*									*/
/************ C 2004 - The software with that certain something *********/

#ifndef _QUEST_COMMON_H 
#define _QUEST_COMMON_H

#ifdef __KERNEL__
  #include <linux/types.h>
  #define P_ID_QUEST 11   // Needs to be re-defined here since we do not want to include rcc_error.h at this level
#else
  #include <sys/types.h>
#endif

#define P_ID_QUEST_STR "QUEST library"

// User modifyable constants
#define MAXQUESTCARDS      10   // Max. number of QUEST cards
#define MAXQUESTCHANNELS   4    // Max. number channels on one card
#define MAXQUESTROLS       (MAXQUESTCARDS * MAXQUESTCHANNELS)
#define MAXQUESTINFIFO     200  // MAX number of entries in the IN FIFO
#define MAXQUESTREQFIFO    64   // MAX number of entries in the REQ FIFO

/**********/
/* Macros */
/**********/
#define QUEST_IN_INDEX(a, b)     (2 * (a * MAXQUESTINFIFO + b))

/*************/
/*ioctl codes*/
/*************/
enum 
{
  QUEST_OPEN = 1,
  QUEST_CLOSE,
  QUEST_CONFIG,
  QUEST_RESET,
  QUEST_INFO,
  QUEST_FIFOIN,
  QUEST_FLUSH,
  QUEST_LINKRESET,
  QUEST_LINKSTATUS
};

/********************/
/* Type definitions */
/********************/
typedef struct 
{
  u_long physbase;  //PCI base address of shared memory 
  u_int size;      //total size of shared memory 
  u_int insize;    //size of all S/W FIFOs
} QUEST_open_t;

typedef struct 
{
  u_int bswap;
  u_int wswap;
  u_int scw;
  u_int ecw;
} QUEST_config_t;

typedef struct 
{
  u_int ncards;
  u_int cards[MAXQUESTCARDS];
  u_int channel[MAXQUESTROLS];
} QUEST_info_t;

typedef struct 
{
  u_int nvalid;
  u_int channel; 
  u_int pciaddr[MAXQUESTINFIFO];
  u_int size[MAXQUESTINFIFO];
  u_int scw[MAXQUESTINFIFO];
  u_int ecw[MAXQUESTINFIFO];
} QUEST_in_t;

typedef struct 
{
  u_int channel;
  u_int ldown;
  u_int infifo;
  u_int reqfifo;
} QUEST_status_t;

/*************/
/*error codes*/
/*************/
enum
{
  QUEST_SUCCESS = 0,
  QUEST_NOTKNOWN = (P_ID_QUEST <<8)+1,
  QUEST_EFAULT,
  QUEST_EIO,
  QUEST_ENOSYS,
  QUEST_ILLREV,
  QUEST_IOREMAP,
  QUEST_REQIRQ,
  QUEST_FILE,
  QUEST_NOTOPEN,
  QUEST_FIFOFULL,
  QUEST_ERROR_FAIL,
  QUEST_KMALLOC,
  QUEST_MMAP,
  QUEST_MUNMAP,
  QUEST_NOHW,
  QUEST_STUCK,
  QUEST_USED,
  QUEST_TOOMANYCARDS,
  QUEST_NO_CODE
};

#endif
