/************************************************************************/
/*									*/
/* File: quest_driver.h							*/
/*									*/
/* This is the private header file for QUEST driver			*/
/*									*/
/* 10. Feb. 04  MAJO  created						*/
/*									*/
/************ C 2004 - The software with that certain something *********/

#ifndef _QUEST_DRIVER_H 
#define _QUEST_DRIVER_H

#include <linux/types.h>
#include "quest_common.h"

#define QUEST_PCI_DEVICE_ID 0x1b
#define MINREV              1     // Oldest supported revision
#define REQBLKSIZE          0x200 // Max. size of the request block
#define REQFIFODEPTH        63    // Depth of the Request FIFO
#define INTERRUPT_THRESHOLD 62    // REQFIFODEPTH - 1
#define MAX_PROC_TEXT_SIZE  0x10000 //The output of "more /proc/quest" must not generate more characters than that
#define BAR0                0
#define MAXIRQ              256

/********/
/*Macros*/
/********/
#ifdef DRIVER_DEBUG
  #define kdebug(x) {if (debug) printk x;}
#else
  #define kdebug(x)
#endif

#ifdef DRIVER_ERROR
  #define kerror(x) {if (errorlog) printk x;}
#else
  #define kerror(x)
#endif


/******************/
/*Type definitions*/
/******************/
typedef struct
{
  u_int opctl;          /*0x000*/
  u_int opstat;         /*0x004*/
  u_int intmask;        /*0x008*/
  u_int intctl;         /*0x00c*/
  u_int blklen;         /*0x010*/
  u_int reqadd;         /*0x014*/
  u_int sctl;           /*0x018*/
  u_int ectl;           /*0x01c*/
  u_int fstat;          /*0x020*/
  u_int estat;          /*0x024*/
} T_quest_regs;

typedef struct
{
  struct pci_dev *pciDevice;
  volatile T_quest_regs *regs;
  u_char pci_revision;
  u_int pci_memaddr;
  u_char irq_line;
} T_quest_card;

struct quest_proc_data_t
{
  char name[10];
  char value[100];
};

/******************************/
/*Standard function prototypes*/
/******************************/
static int quest_open(struct inode *ino, struct file *filep);
static int quest_release(struct inode *ino, struct file *filep);
static long quest_ioctl(struct file *file, u_int cmd, u_long arg);
static int quest_mmap(struct file *file, struct vm_area_struct *vma);
static ssize_t quest_proc_write(struct file *file, const char *buffer, size_t count, loff_t *startOffset);
int quest_proc_open(struct inode *inode, struct file *file); 

/*****************************/
/*Special function prototypes*/
/*****************************/
static int in_fifo_pop(int card, u_int *data1, u_int *data2);
static void refill_req_fifo_dma(int card);
int quest_proc_show(struct seq_file *sfile, void *p);

#endif
