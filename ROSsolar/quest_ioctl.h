/************************************************************************/
/*									*/
/* File: quest_ioctl.h							*/
/*									*/
/* This header file contains definitions for the ioctl function calls	*/
/* linking the QUEST library to the driver				*/
/*									*/
/* 10. Feb. 04  MAJO  created						*/
/*									*/
/************ C 2004 - The software with that certain something *********/

#ifndef _QUEST_IOCTL_H 
#define _QUEST_IOCTL_H

#include <linux/types.h>

/*************/
/*ioctl codes*/
/*************/
#define QUEST_MAGIC 'x'

#define OPEN       _IOW(QUEST_MAGIC, 58, QUEST_open_t)
#define CLOSE      _IO(QUEST_MAGIC, 59)
#define CONFIG     _IOW(QUEST_MAGIC, 60, QUEST_config_t)
#define RESET      _IOW(QUEST_MAGIC, 61, int)
#define INFO       _IOR(QUEST_MAGIC, 62, QUEST_info_t)
#define FIFOIN     _IOW(QUEST_MAGIC, 63, int)
#define LINKRESET  _IOW(QUEST_MAGIC, 64, int)
#define LINKSTATUS _IOR(QUEST_MAGIC, 65, QUEST_status_t)

/*
enum 
{
  OPEN = 1,
  CLOSE,
  CONFIG,
  RESET,
  INFO,
  FIFOIN,
  FLUSH,
  LINKRESET,
  LINKSTATUS
};
*/

#endif

