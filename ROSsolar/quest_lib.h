/************************************************************************/
/*									*/
/* File: quest_lib.h							*/
/*									*/
/* This is the private header file for QUEST library			*/
/*									*/
/* 10. Feb. 04  MAJO  created						*/
/*									*/
/************ C 2004 - The software with that certain something *********/

#ifndef _QUEST_LIB_H 
#define _QUEST_LIB_H

#include <linux/types.h>
#include "quest_common.h"

/***************/
/*error strings*/
/***************/
#define QUEST_SUCCESS_STR        "Function successfully executed"
#define QUEST_NOTKNOWN_STR       "Input parameter has illegal value"
#define QUEST_EFAULT_STR         "EFAULT error from driver"
#define QUEST_EIO_STR            "EIO error from driver"
#define QUEST_ENOSYS_STR         "ENOSYS error from driver"
#define QUEST_ILLREV_STR         "QUEST has too old revision"
#define QUEST_IOREMAP_STR        "Error from ioremap system call"
#define QUEST_REQIRQ_STR         "Error from request_irq system call"
#define QUEST_FILE_STR           "Failed to open / close device file"
#define QUEST_NOTOPEN_STR        "The library has not yet been opened"
#define QUEST_NO_CODE_STR        "Unknown error"
#define QUEST_KMALLOC_STR        "Error from kmalloc in the driver"
#define QUEST_FIFOFULL_STR       "Cannot write to full FIFO"
#define QUEST_MMAP_STR           "Error from mmap()"
#define QUEST_MUNMAP_STR         "Error from munmap()"
#define QUEST_STUCK_STR          "A link did not come up again after a reset"
#define QUEST_USED_STR           "The driver is already used by another process"
#define QUEST_NOHW_STR           "card or channel does not exist"

/********/
/*Macros*/
/********/
#define ISOPEN {if(!is_open) return(QUEST_NOTOPEN);}
#define PE(x) {printf("Error from quest library: %s\n",x);     break;}

#endif
