/************************************************************************/
/*									*/
/* File: solar.h							*/
/*									*/
/* This is the public header file for SOLAR library			*/
/*									*/
/* 4. Apr. 03  MAJO  created						*/
/*									*/
/************ C 2003 - The software with that certain something *********/

#ifndef _SOLAR_H 
#define _SOLAR_H

#include <linux/types.h>
#include "solar_common.h"

/*******/
/*Types*/
/*******/
typedef unsigned int   SOLAR_ErrorCode_t;


/************/
/*Prototypes*/
/************/
#ifdef __cplusplus
extern "C" {
#endif


/*******************************/ 
/*Official functions of the API*/
/*******************************/ 
SOLAR_ErrorCode_t SOLAR_Open(void);
SOLAR_ErrorCode_t SOLAR_Close(void);
SOLAR_ErrorCode_t SOLAR_Info(SOLAR_info_t *info);
SOLAR_ErrorCode_t SOLAR_Init(SOLAR_config_t *config);
SOLAR_ErrorCode_t SOLAR_Reset(u_int card);
SOLAR_ErrorCode_t SOLAR_LinkReset(u_int card);
SOLAR_ErrorCode_t SOLAR_LinkStatus(u_int card, SOLAR_status_t *status);
SOLAR_ErrorCode_t SOLAR_PagesIn(SOLAR_in_t *fifoin);
SOLAR_ErrorCode_t SOLAR_InFree(u_int channel, u_int *nfree);

/******************************/
/* Internal service functions */
/******************************/
SOLAR_ErrorCode_t SOLAR_err_get(err_pack err, err_str pid, err_str code);
int in_fifo_push(u_int card, u_int data1, u_int data2);

#ifdef __cplusplus
}
#endif

#endif
