/************************************************************************/
/*									*/
/* File: solar_common.h							*/
/*									*/
/* This is the common public header file for SOLAR library		*/
/* and driver								*/
/*									*/
/* 4. Apr. 03  MAJO  created						*/
/*									*/
/************ C 2003 - The software with that certain something *********/

#ifndef _SOLAR_COMMON_H 
#define _SOLAR_COMMON_H

#ifdef __KERNEL__
  #include <linux/types.h>
  #define P_ID_SOLAR 10   // Needs to be re-defined here since we do not want to include rcc_error.h at this level
#else
  #include <sys/types.h>
#endif

#define P_ID_SOLAR_STR "SOLAR library"

// Usewr modifyable constants
#define MAXCARDS      10   // Max. number of SOLAR cards
#define MAXINFIFO     200  // MAX number of entries in the IN FIFO
#define MAXREQFIFO    64   // MAX number of entries in the REQ FIFO

/**********/
/* Macros */
/**********/
#define IN_INDEX(a, b)     (2 * (a * MAXINFIFO + b))

/*************/
/*ioctl codes*/
/*************/
enum 
{
  OPEN = 1,
  CONFIG,
  RESET,
  INFO,
  FIFOIN,
  FLUSH,
  LINKRESET,
  LINKSTATUS
};

/********************/
/* Type definitions */
/********************/
typedef struct 
{
  u_int physbase;
  u_int size;
  u_int insize;
} SOLAR_open_t;

typedef struct 
{
  u_int enable[MAXCARDS];
  u_int bswap;
  u_int wswap;
  u_int scw;
  u_int ecw;
  u_int udw;
  u_int clksel;
  u_int clkdiv;
  u_int lffi;
  u_int ldi;
} SOLAR_config_t;

typedef struct 
{
  u_int ncards;
  u_int cards[MAXCARDS];
  u_int enabled[MAXCARDS];
} SOLAR_info_t;

typedef struct 
{
  u_int nvalid;
  u_int card; 
  u_int pciaddr[MAXINFIFO];
  u_int size[MAXINFIFO];
  u_int scw[MAXINFIFO];
  u_int ecw[MAXINFIFO];
} SOLAR_in_t;

typedef struct 
{
  u_int card;
  u_int ldown;
  u_int infifo;
  u_int reqfifo;
} SOLAR_status_t;

/*************/
/*error codes*/
/*************/
enum
{
  SOLAR_SUCCESS = 0,
  SOLAR_NOTKNOWN = (P_ID_SOLAR <<8)+1,
  SOLAR_EFAULT,
  SOLAR_EIO,
  SOLAR_ENOSYS,
  SOLAR_ILLREV,
  SOLAR_IOREMAP,
  SOLAR_REQIRQ,
  SOLAR_FILE,
  SOLAR_NOTOPEN,
  SOLAR_FIFOFULL,
  SOLAR_ERROR_FAIL,
  SOLAR_KMALLOC,
  SOLAR_MMAP,
  SOLAR_MUNMAP,
  SOLAR_NOHW,
  SOLAR_STUCK,
  SOLAR_NO_CODE
};

#endif
