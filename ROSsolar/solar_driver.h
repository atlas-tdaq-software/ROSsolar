/************************************************************************/
/*									*/
/* File: solar_driver.h							*/
/*									*/
/* This is the private header file for SOLAR driver			*/
/*									*/
/* 2. Apr. 03  MAJO  created						*/
/*									*/
/************ C 2003 - The software with that certain something *********/

#ifndef _SOLAR_DRIVER_H 
#define _SOLAR_DRIVER_H

#include <linux/types.h>
#include "solar_common.h"

#define SOLAR_PCI_DEVICE_ID 0x17
#define MINREV              1   //Oldest supported revision
#define REQFIFODEPTH        32  //Depth of the Request FIFO
#define INTERRUPT_THRESHOLD 32  //=REQFIFODEPTH
#define MAX_PROC_TEXT_SIZE  0x10000 //The output of "more /proc/solar" must not generate more characters than that
#define BAR0                0
#define MAXIRQ              256

/********/
/*Macros*/
/********/
#ifdef DRIVER_DEBUG
  #define kdebug(x) {if (debug) printk x;}
#else
  #define kdebug(x)
#endif

#ifdef DRIVER_ERROR
  #define kerror(x) {if (errorlog) printk x;}
#else
  #define kerror(x)
#endif


/******************/
/*Type definitions*/
/******************/
typedef struct
{
  u_int opctrl;         /*0x000*/
  u_int opstat;         /*0x004*/
  u_int intmask;        /*0x008*/
  u_int dummy_tstout;   /*0x00c*/
  u_int opfeat;         /*0x010*/
  u_int dummy_opefcnt;  /*0x014*/
  u_int bctrlw;         /*0x018*/
  u_int ectrlw;         /*0x01c*/
  u_int reserved[56];   /*0x020 - 0x0fc*/
  u_int reqfifo1;       /*0x100*/
  u_int reqfifo2;       /*0x104*/
} T_solar_regs;

typedef struct
{
  struct pci_dev *pciDevice;
  T_solar_regs *regs;
  u_char pci_revision;
  u_int pci_memaddr;
  u_char irq_line;
} T_solar_card;

struct solar_proc_data_t
{
  char name[10];
  char value[100];
};

/******************************/
/*Standard function prototypes*/
/******************************/
static int solar_open(struct inode *ino, struct file *filep);
static int solar_release(struct inode *ino, struct file *filep);
static long solar_ioctl(struct file *file, u_int cmd, u_long arg);
static int solar_mmap(struct file *file, struct vm_area_struct *vma);
static int solar_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data);
static int solar_write_procmem(struct file *file, const char *buffer, u_long count, void *data);

/*****************************/
/*Special function prototypes*/
/*****************************/
static int in_fifo_pop(int card, u_int *data1, u_int *data2);
static void refill_req_fifo(void);

#endif
