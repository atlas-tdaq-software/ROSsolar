/************************************************************************/
/*									*/
/* File: solar_ioctl.h							*/
/*									*/
/* This header file contains definitions for the ioctl function calls	*/
/* linking the SOLAR library to the driver				*/
/*									*/
/* 2. Apr. 03  MAJO  created						*/
/*									*/
/************ C 2003 - The software with that certain something *********/

#ifndef _SOLAR_IOCTL_H 
#define _SOLAR_IOCTL_H

#include <linux/types.h>

/*************/
/*ioctl codes*/
/*************/
#define SOLAR_MAGIC 'x'

#define OPEN       _IOW(SOLAR_MAGIC, 50, SOLAR_open_t)
#define CONFIG     _IOW(SOLAR_MAGIC, 51, SOLAR_config_t)
#define RESET      _IOW(SOLAR_MAGIC, 52, int)
#define INFO       _IOR(SOLAR_MAGIC, 53, SOLAR_info_t)
#define FIFOIN     _IOW(SOLAR_MAGIC, 54, int)
#define LINKRESET  _IOW(SOLAR_MAGIC, 56, int)
#define LINKSTATUS _IOR(SOLAR_MAGIC, 57, SOLAR_status_t)

/*
enum 
{
  OPEN = 1,
  CONFIG,
  RESET,
  INFO,
  FIFOIN,
  FLUSH,
  LINKRESET,
  LINKSTATUS
};
*/


#endif

