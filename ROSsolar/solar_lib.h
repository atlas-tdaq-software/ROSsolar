/************************************************************************/
/*									*/
/* File: solar_lib.h							*/
/*									*/
/* This is the private header file for SOLAR library			*/
/*									*/
/* 4. Apr. 03  MAJO  created						*/
/*									*/
/************ C 2003 - The software with that certain something *********/

#ifndef _SOLAR_LIB_H 
#define _SOLAR_LIB_H

#include <linux/types.h>
#include "solar_common.h"

/***************/
/*error strings*/
/***************/
#define SOLAR_SUCCESS_STR        "Function successfully executed"
#define SOLAR_NOTKNOWN_STR       "Input parameter has illegal value"
#define SOLAR_EFAULT_STR         "EFAULT error from driver"
#define SOLAR_EIO_STR            "EIO error from driver"
#define SOLAR_ENOSYS_STR         "ENOSYS error from driver"
#define SOLAR_ILLREV_STR         "SOLAR has too old revision"
#define SOLAR_IOREMAP_STR        "Error from ioremap system call"
#define SOLAR_REQIRQ_STR         "Error from request_irq system call"
#define SOLAR_FILE_STR           "Failed to open / close device file"
#define SOLAR_NOTOPEN_STR        "The library has not yet been opened"
#define SOLAR_NO_CODE_STR        "Unknown error"
#define SOLAR_KMALLOC_STR        "Error from kmalloc in the driver"
#define SOLAR_FIFOFULL_STR       "Cannot write to full FIFO"
#define SOLAR_MMAP_STR           "Error from mmap()"
#define SOLAR_MUNMAP_STR         "Error from munmap()"
#define SOLAR_STUCK_STR          "A link did not come up again after a reset"
#define SOLAR_NOHW_STR           "card or channel does not exist"

/********/
/*Macros*/
/********/
#define ISOPEN {if(!is_open) return(SOLAR_NOTOPEN);}
#define PE(x) {printf("Error from solar library: %s\n",x);     break;}

#endif
