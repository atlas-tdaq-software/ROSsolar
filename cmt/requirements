package ROSsolar

author  markus.joos@cern.ch
manager markus.joos@cern.ch

#==========================================================
public
#==========================================================
use TDAQPolicy
use tmgr
use ROSEventFragment
use io_rcc
use cmdl
use cmem_rcc 
use DFDebug
use rcc_error
use rcc_time_stamp
use ROSGetInput
use ROSMemoryPool

#==========================================================
private
#==========================================================
macro base_binaries  "solar quest solar_test quest_test solarscope dolarscope questscope quest_slink_src solar_slink_src dolar_slink_src"
macro constituents   "$(base_binaries)"
#----------------------------------------------------------
library solar  "lib/solar_lib.cpp"
library quest  "lib/quest_lib.cpp"
#----------------------------------------------------------
application solar_test         test/solar_test.cpp
application quest_test         test/quest_test.cpp
application solarscope         test/solarscope.cpp  
application dolarscope         test/dolarscope.cpp
application questscope         test/questscope.cpp
application solar_slink_src    test/solar_slink_src.cpp
application quest_slink_src    test/quest_slink_src.cpp
application dolar_slink_src    test/dolar_slink_src.cpp
#----------------------------------------------------------
macro_remove cflags            "-ansi"
macro_remove cflags            "-pedantic"
macro_remove cflags            "-fPIC"

macro solar_cppflags           "-DnoDEBUG -DTSTAMP"
macro quest_cppflags           "-DnoDEBUG -DTSTAMP"

macro solar_testlinkopts       "-lcmem_rcc -lsolar -lgetinput"
macro quest_testlinkopts       "-lcmem_rcc -lquest -lgetinput"
macro solarscopelinkopts       "-lcmem_rcc -lgetinput -lrcc_time_stamp -lio_rcc"
macro dolarscopelinkopts       "-lio_rcc -lgetinput -lrcc_time_stamp"
macro questscopelinkopts       "-lcmem_rcc -lio_rcc -lgetinput -lrcc_time_stamp"
macro solar_slink_srclinkopts  "-lsolar -lROSEventFragment -lrcc_time_stamp"
macro quest_slink_srclinkopts  "-lquest -lROSEventFragment -lrcc_time_stamp"
macro dolar_slink_srclinkopts  "-lio_rcc -lgetinput -lROSEventFragment -lrcc_time_stamp"
   
macro solar_test_dependencies       solar
macro quest_test_dependencies       quest
macro solar_slink_src_dependencies  solar
macro quest_slink_src_dependencies  quest

#==========================================================
public
#==========================================================
macro   solar_libs "libsolar.so libquest.so"
apply_pattern install_libs files="$(solar_libs)"

macro   solar_apps "solar_slink_src quest_slink_src solar_test quest_test solarscope questscope dolarscope dolar_slink_src"
apply_pattern install_apps files="$(solar_apps)"

apply_pattern ld_library_path
#----------------------------------------------------------
macro solar_shlibflags "-lrcc_error"
macro quest_shlibflags "-lrcc_error"

macro sw.repository.binary.dolar_slink_src:name        "dolar_slink_src"
macro sw.repository.binary.dolar_slink_src:description  "Generates SLINK fragments using a Dolar"

macro sw.repository.binary.solarscope:name             "solarscope"
macro sw.repository.binary.solarscope:description      "Binary for solarscope"

macro sw.repository.binary.dolarscope:name             "dolarscope"
macro sw.repository.binary.dolarscope:description      "Binary for dolarscope"

macro sw.repository.binary.quest_test:name             "quest_test"
macro sw.repository.binary.quest_test:description      "Binary for quest_test"
