/************************************************************************/
/*									*/
/* File: quest_driver.c							*/
/*									*/
/* driver for the QUEST S-Link interface				*/
/*									*/
/* 9. Dec. 03  MAJO  created						*/
/*									*/
/************ C 2004 - The software with that certain something *********/

/************************************************************************/
/*NOTES:								*/
/*- This driver should work on 2.4 kernels				*/
/************************************************************************/

// Module Versioning a la Rubini p.316
#include <linux/config.h>

#if defined(CONFIG_MODVERSIONS) && !defined(MODVERSIONS)
  #define MODVERSIONS
#endif

#if defined(MODVERSIONS)
  #include <linux/modversions.h>
#endif

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/mman.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include "ROSsolar/quest_driver.h"

#ifdef MODULE
  MODULE_PARM (debug, "i");
  MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging");
  MODULE_DESCRIPTION("S-Link QUEST");
  MODULE_AUTHOR("Markus Joos, CERN/EP");
  #ifdef MODULE_LICENSE
    MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
  #endif
#endif


/*********/
/*Globals*/
/*********/
static int debug = 0;
static int quest_major = 0; // use dynamic allocation
static volatile T_quest_regs *quest[MAXQUESTCARDS];
static u_int quest_irq_line[MAXQUESTCARDS];
static u_int maxcards, pci_memaddr[MAXQUESTCARDS];
static u_char pci_revision[MAXQUESTCARDS];
static u_int opid, served[MAXQUESTROLS];
static u_int *infifo, *infifon;
static u_int reqadd[MAXQUESTCARDS], reqbuf[MAXQUESTCARDS], infifor[MAXQUESTROLS], req_dma_active[MAXQUESTCARDS];
static u_int order, swfifobasephys, swfifobase, infifodatasize, fifoptrsize, tsize = 0;
static struct proc_dir_entry *quest_file;
struct quest_proc_data_t quest_proc_data;
static char *proc_read_text;

/************/
/*Prototypes*/
/************/
static void quest_irq_handler (int irq, void *dev_id, struct pt_regs *regs);
void quest_vclose(struct vm_area_struct *vma);
static int init_quests(void);
static int cleanup_quests(void);

static struct file_operations fops = 
{
  owner:   THIS_MODULE,
  mmap:    quest_mmap,
  ioctl:   quest_ioctl,
  open:    quest_open,    
  release: quest_release
};

// memory handler functions
static struct vm_operations_struct quest_vm_ops = 
{
  close:  quest_vclose,   // mmap-close
};


/*****************************/
/* Standard driver functions */
/*****************************/


/**********************************************************/
static int quest_open(struct inode *ino, struct file *filep)
/**********************************************************/
{
  return(0);
}


/*************************************************************/
static int quest_release(struct inode *ino, struct file *filep)
/*************************************************************/
{
  opid = 0;
  return(0);
}


/******************************************************************************************/
static int quest_ioctl(struct inode *inode, struct file *file, u_int cmd, unsigned long arg)
/******************************************************************************************/
{
  switch (cmd)
  {    
    case QUEST_OPEN:
    {
      QUEST_open_t params;
      
      kdebug(("quest(ioctl,QUEST_OPEN): called from process %d\n", current->pid))
      if (!opid)
        opid = current->pid;
      else
  	return(-QUEST_USED);    	
      
      params.size = tsize;
      params.insize = infifodatasize;
      params.physbase = swfifobasephys;
      kdebug(("quest(ioctl,QUEST_OPEN): params.size     = 0x%08x\n", params.size));
      kdebug(("quest(ioctl,QUEST_OPEN): params.physbase = 0x%08x\n", params.physbase));
      kdebug(("quest(ioctl,QUEST_OPEN): params.insize   = 0x%08x\n", params.insize));
  
      if (copy_to_user((void *)arg, &params, sizeof(QUEST_open_t)) != 0)
      {
	kdebug(("quest(ioctl, QUEST_OPEN): error from copy_to_user\n"));
	return(-QUEST_EFAULT);
      }      
      return(0);
      break;
    }
    
    case QUEST_CLOSE:
    {
      kdebug(("quest(ioctl,QUEST_CLOSE): called from process %d\n", current->pid))
      opid = 0;
  
      return(0);
      break;
    }
    
    case QUEST_CONFIG:
    { 
      QUEST_config_t params;
      u_int channel, card, data;
      
      if (copy_from_user(&params, (void *)arg, sizeof(QUEST_config_t)) !=0 )
      {
	kdebug(("quest(ioctl,QUEST_CONFIG): error from copy_from_user\n"));
	return(-QUEST_EFAULT);
      }
      kdebug(("quest(ioctl,QUEST_CONFIG): \n"));        
      kdebug(("quest(ioctl,QUEST_CONFIG): params.bswap  = %d\n", params.bswap));
      kdebug(("quest(ioctl,QUEST_CONFIG): params.wswap  = %d\n", params.wswap));
      kdebug(("quest(ioctl,QUEST_CONFIG): params.scw    = 0x%08x\n", params.scw));
      kdebug(("quest(ioctl,QUEST_CONFIG): params.ecw    = 0x%08x\n", params.ecw));

      for(card = 0; card < maxcards; card++)
      {
        data = 0;  
        quest[card]->opctl = (params.wswap << 2) + (params.bswap << 1);
        quest[card]->sctl  = params.scw & 0xfffffffc;
        quest[card]->ectl  = params.ecw & 0xfffffffc;
        
        //The LFF and LDOWN interrupts are not yet supported
	quest[card]->intctl = 0xFBEFBE05; // threshold fixed at 62 for all channels, clear any outstanding interrupts
	quest[card]->intmask = 0x1;
      }

      for(channel = 0; channel < MAXQUESTROLS; channel++)
        served[channel] = 0;
	
      break;
    }
    
    case QUEST_RESET:
    {
      u_int rol, data, card, channel;
      
      if (copy_from_user(&card, (void *)arg, sizeof(int)) !=0 )
      {
	kdebug(("quest(ioctl,QUEST_RESET): error from copy_from_user\n"));
	return(-QUEST_EFAULT);
      }
     
      kdebug(("quest(ioctl,QUEST_RESET): Resetting card %d\n", card));
      data = quest[card]->opctl;
      data |= 0x1;
      quest[card]->opctl = data;
      // Delay for at least one us
      udelay(2);
      data &= 0xfffffffe;
      quest[card]->opctl = data;

      req_dma_active[card] = 0;

      //reset the FIFOs
      for (channel = 0; channel < MAXQUESTCHANNELS; channel++)
      {
        rol = (card << 2) + channel;     
        infifor[rol] = 0;
        infifon[rol] = 0;
      }
      
      return(0);
      break;
    }
    
    case QUEST_LINKRESET:
    {   
      u_int data1, data2, waited = 0, card, channel;
 
      if (copy_from_user(&channel, (void *)arg, sizeof(int)) !=0 )
      {
	kdebug(("quest(ioctl,QUEST_LINKRESET): error from copy_from_user\n"));
	return(-QUEST_EFAULT);
      }

      card = channel >> 2;      
      channel &= 0x3;
      kdebug(("quest(ioctl,QUEST_LINKRESET): Resetting channel %d of card %d\n", channel, card));

      data1 = 1 << (16 + channel * 4);
      data2 = 1 << (4 + channel * 7);
 
      // Set the URESET bit
      quest[card]->opctl |= data1;
      
      // Delay for at least two ms
      udelay(2000);

      // Now wait for LDOWN to come up again
      while(quest[card]->opstat & data2)
      {
        waited++;
        if (waited > 10000)
        {
          kdebug(("quest(ioctl,QUEST_LINKRESET): card %d does not come up again\n", card));
          // Reset the URESET bit
          quest[card]->opctl &= ~data1;
	  return(-QUEST_STUCK);
        }          
      }

      // Reset the URESET bit
      quest[card]->opctl &= ~data1;

      served[4* card + channel] = 0;
      return(0);
      break;
    }
    
    case QUEST_LINKSTATUS:
    {              
      unsigned long flags;
      u_int data, card;
      QUEST_status_t params;
      
      if (copy_from_user(&params, (void *)arg, sizeof(QUEST_status_t)) !=0 )
      {
	kdebug(("quest(ioctl, QUEST_LINKSTATUS): error from copy_from_user\n"));
	return(-QUEST_EFAULT);
      }
       
      card = params.channel >> 2;
      data = 1 << (4 + (params.channel & 0x3) * 7);
      if (quest[card]->opstat & data)
        params.ldown = 1;
      else
        params.ldown = 0;
      
      //We have to make sure that this code does not get interrupted. 
      save_flags(flags);
      cli();       
      params.infifo = infifon[params.channel];
      params.reqfifo = REQFIFODEPTH - ((quest[card]->fstat >> (8 * (params.channel & 0x3))) & 0xff); 
      kdebug(("quest(ioctl, QUEST_LINKSTATUS): params.ldown = %d\n", params.ldown));
      kdebug(("quest(ioctl, QUEST_LINKSTATUS): params.infifo = %d\n", params.infifo));
      kdebug(("quest(ioctl, QUEST_LINKSTATUS): params.reqfifo = %d\n", params.reqfifo));
      restore_flags(flags);

      if (copy_to_user((void *)arg, &params, sizeof(QUEST_status_t)) != 0)
      {
	kdebug(("quest(ioctl, QUEST_LINKSTATUS): error from copy_to_user\n"));
	return(-QUEST_EFAULT);
      } 
      
      return(0);
      break;
    }

    case QUEST_INFO:
    {
      QUEST_info_t info;
      u_int data, card, channel;
            
      info.ncards = maxcards;  
      kdebug(("quest(ioctl, QUEST_INFO): maxcards = %d\n", maxcards));
      
      for(card = 0; card < MAXQUESTCARDS; card++)
      {
        if (card < maxcards)
	  info.cards[card] = 1;
	else  
	  info.cards[card] = 0;
        kdebug(("quest(ioctl, QUEST_INFO): info.cards[%d] = %d\n", card, info.cards[card]));
      }
      
      for(channel = 0; channel < MAXQUESTCHANNELS; channel++)
      {
        card = channel >> 2;
	if (info.cards[card] == 1)
	{
	  data = quest[card]->opstat & (1 << (10 + 7 * (channel & 0x3))); 
	  if (data)
	    info.channel[channel] = 0;
	  else
	    info.channel[channel] = 1;
	}
	else
	  info.channel[channel] = 0;
        kdebug(("quest(ioctl, QUEST_INFO): info.channel[%d] = %d\n", channel, info.channel[channel]));
      } 

      if (copy_to_user((void *)arg, &info, sizeof(QUEST_info_t)) != 0)
      {
	kdebug(("quest(ioctl, QUEST_INFO): error from copy_to_user\n"));
	return(-QUEST_EFAULT);
      }      
      return(0);
      break;
    }
    
    case QUEST_FIFOIN:
    { 
      u_int channel;

      if (copy_from_user(&channel, (void *)arg, sizeof(int)) !=0)
      {
	kdebug(("quest(ioctl,QUEST_FIFOIN): error from copy_from_user\n"));
	return(-QUEST_EFAULT);
      }
      kdebug(("quest(ioctl,QUEST_FIFOIN): channel = %d\n", channel));

      //Try to post a new request block on the card of this channel
      refill_req_fifo_dma(channel >> 2);

      return(0);
      break;
    }
  }   
  return(0);
}


/****************************************************************************************************/
static int quest_write_procmem(struct file *file, const char *buffer, unsigned long count, void *data)
/****************************************************************************************************/
{
  int len;
  struct quest_proc_data_t *fb_data = (struct quest_proc_data_t *)data;

  kdebug(("quest(quest_write_procmem): quest_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(fb_data->value, buffer, len))
  {
    kdebug(("quest(quest_write_procmem): error from copy_from_user\n"));
    return(-QUEST_EFAULT);
  }

  kdebug(("quest(quest_write_procmem): len = %d\n", len));
  fb_data->value[len - 1] = '\0';
  kdebug(("quest(quest_write_procmem): text passed = %s\n", fb_data->value));

  if (!strcmp(fb_data->value, "debug"))
  {
    debug = 1;
    kdebug(("quest(quest_write_procmem): debugging enabled\n"));
  }

  if (!strcmp(fb_data->value, "nodebug"))
  {
    kdebug(("quest(quest_write_procmem): debugging disabled\n"));
    debug = 0;
  }
  
  return len;
}


/***************************************************************************************************/
static int quest_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data)
/***************************************************************************************************/
{
  u_int card, channel, fstat, ocr, osr, imask, estat, value;
  int loop, nchars = 0;
  static int len = 0;
  
  kdebug(("quest(quest_read_procmem): Called with buf    = 0x%08x\n", (u_int)buf));
  kdebug(("quest(quest_read_procmem): Called with *start = 0x%08x\n", (u_int)*start));
  kdebug(("quest(quest_read_procmem): Called with offset = %d\n", (u_int)offset));
  kdebug(("quest(quest_read_procmem): Called with count  = %d\n", count));

  if (offset == 0)
  {
    kdebug(("exp(exp_read_procmem): Creating text....\n"));
    len = 0;
    len += sprintf(proc_read_text + len, "\n\n\nQUEST driver for release %s (based on CVS tag %s)\n", RELEASE_NAME, CVSTAG);

    if (opid)
      len += sprintf(proc_read_text + len, "The QUEST(s) are currently used by process %d\n", opid);

    len += sprintf(proc_read_text + len, "==================================================\n");
    len += sprintf(proc_read_text + len, "Card|IRQ line|  REQ IRQ| DONE IRQ|revision|PCI MEM addr.|wswap|bswap|Temperature\n");
    len += sprintf(proc_read_text + len, "----|--------|---------|---------|--------|-------------|-----|-----|-----------\n");
    for(card = 0; card < maxcards; card++)
    {
      ocr = quest[card]->opctl;
      osr = quest[card]->opstat;
      imask = quest[card]->intmask;
      estat = quest[card]->estat;
      len += sprintf(proc_read_text + len, "   %d|", card);
      len += sprintf(proc_read_text + len, "      %2d|", quest_irq_line[card]);
      len += sprintf(proc_read_text + len, " %s|", (imask & 0x4)?"enabled":"disabled"); 
      len += sprintf(proc_read_text + len, " %s|", (imask & 0x1)?"enabled":"disabled"); 
      len += sprintf(proc_read_text + len, "    0x%02x|", pci_revision[card]); 
      len += sprintf(proc_read_text + len, "   0x%08x|", pci_memaddr[card]);
      len += sprintf(proc_read_text + len, "  %s|", (ocr & 0x4)?"yes":" no");
      len += sprintf(proc_read_text + len, "  %s", (ocr & 0x2)?"yes":" no");
      len += sprintf(proc_read_text + len, "%9d C", estat & 0xff);
    }

    for(card = 0; card < maxcards; card++)
    {
      len += sprintf(proc_read_text + len, "\nCard %d:\n", card);
      len += sprintf(proc_read_text + len, "=======\n");
      len += sprintf(proc_read_text + len, "Channel|present|LDOWN|#REQ slots avail.|#fragments sent\n");
      len += sprintf(proc_read_text + len, "-------|-------|-----|-----------------|---------------\n");

      osr = quest[card]->opstat;
      fstat = quest[card]->fstat;
      for(channel  = 0; channel < MAXQUESTCHANNELS; channel++)
      {
	len += sprintf(proc_read_text + len, "      %1d|", channel);
	value = osr & (1 << (10 + channel * 7));
	len += sprintf(proc_read_text + len, "    %s|", value?" No":"Yes");
	value = osr & (1 << (4 + channel * 7));
	len += sprintf(proc_read_text + len, "  %s|", value?"Yes":" No");  
	value = (fstat >> (8 * channel)) & 0xff;
	len += sprintf(proc_read_text + len, "%17d|", value);
	len += sprintf(proc_read_text + len, "%15d\n", served[(card << 2) + channel]);     
      }
    }

    len += sprintf(proc_read_text + len, " \n");
    len += sprintf(proc_read_text + len, "The command 'echo <action> > /proc/quest', executed as root,\n");
    len += sprintf(proc_read_text + len, "allows you to interact with the driver. Possible actions are:\n");
    len += sprintf(proc_read_text + len, "debug   -> enable debugging\n");
    len += sprintf(proc_read_text + len, "nodebug -> disable debugging\n");
  }
  kdebug(("quest(quest_read_procmem): number of characters in text buffer = %d\n", len));

  if (count < (len - offset))
    nchars = count;
  else
    nchars = len - offset;
  kdebug(("quest(quest_read_procmem): min nchars         = %d\n", nchars));
  
  if (nchars > 0)
  {
    for (loop = 0; loop < nchars; loop++)
      buf[loop + (offset & (PAGE_SIZE - 1))] = proc_read_text[offset + loop];
    *start = buf + (offset & (PAGE_SIZE - 1));
  }
  else
  {
    nchars = 0;
    *eof = 1;
  }
 
  kdebug(("quest(quest_read_procmem): returning *start   = 0x%08x\n", (u_int)*start));
  kdebug(("quest(quest_read_procmem): returning nchars   = %d\n", nchars));
  return(nchars);
}


/*******************/
int init_module(void)
/*******************/
{
  int tfsize, result;
  u_int channel, card, loop;
  struct page *page_ptr;
     
  SET_MODULE_OWNER(&fops);

  result = init_quests();
  if (result)
  {
    kdebug(("quest(init_module): init_quest() failed\n"));
    return(result);
  }

  result = register_chrdev(quest_major, "quest", &fops); 
  if (result < 1)
  {
    kdebug(("quest(init_module): registering QUEST driver failed.\n"));
    return(-QUEST_EIO);
  }
  quest_major = result;

  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kdebug(("quest(init_module): error from kmalloc\n"));
    return(-EFAULT);
  }
  
  quest_file = create_proc_entry("quest", 0644, NULL);
  if (quest_file == NULL)
  {
    kdebug(("quest(init_module): error from call to create_proc_entry\n"));
    return (-ENOMEM);
  }

  strcpy(quest_proc_data.name, "quest");
  strcpy(quest_proc_data.value, "quest");
  quest_file->data = &quest_proc_data;
  quest_file->read_proc = quest_read_procmem;
  quest_file->write_proc = quest_write_procmem;
  quest_file->owner = THIS_MODULE;    
  
  // Allocate contiguous memory for the FIFOs and store the base addresses in a global structure
  infifodatasize = MAXQUESTROLS * MAXQUESTINFIFO * sizeof(u_int) * 2;  //infifo
  fifoptrsize = MAXQUESTROLS * sizeof(u_int);                          //infifo counter
  kdebug(("quest(init_module): 0x%08x bytes needed for the IN FIFOs\n", infifodatasize));
  kdebug(("quest(init_module): 0x%08x bytes needed for the FIFO counters\n", fifoptrsize));
  
  tsize = infifodatasize + fifoptrsize;
  kdebug(("quest(init_module): 0x%08x bytes needed for all FIFOs\n", tsize));

  tfsize = (tsize + 4095) & 0xfffff000;  // round up to next 4K boundary
  tfsize = tfsize >> 12;                 // compute number of pages
  kdebug(("quest(init_module): %d pages needed for the S/W FIFOs\n", tfsize));
 
  order = 0;
  while(tfsize)
  {
    order++;
    tfsize = tfsize >> 1;                 // compute order
  } 
  kdebug(("quest(init_module): order = %d\n", order));
  
  swfifobase = __get_free_pages(GFP_ATOMIC, order);
  if (!swfifobase)
  {
    kdebug(("rquest(init_module): error from __get_free_pages\n"));
    return(-QUEST_EFAULT);
  }
  kdebug(("quest(init_module): swfifobase = 0x%08x\n", swfifobase));
   
  // Reserve all pages to make them remapable
  // This code fragment has been taken from the PCI (SHASLink) driver of M. Mueller
  page_ptr = virt_to_page(swfifobase);

  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    set_bit(PG_reserved, &page_ptr->flags);

  swfifobasephys = virt_to_bus((void *) swfifobase);
  kdebug(("quest(init_module): swfifobasephys = 0x%08x\n", swfifobasephys));

  // Assign base addresses to the FIFO arrays
  infifo  = (u_int *)swfifobase;
  infifon = (u_int *)(swfifobase + infifodatasize);
  kdebug(("quest(init_module): infifo   is at 0x%08x\n", (u_int)infifo));
  kdebug(("quest(init_module): infifon  is at 0x%08x\n", (u_int)infifon));

  //Initialize the FIFOs
  for(channel = 0; channel < MAXQUESTROLS; channel++)
  {
    infifor[channel] = 0;
    infifon[channel] = 0;
  }

  //Allocate the buffers for the REQ blocks
  //The max size of such a block is: 4 channels * (1 size word + 126 data words) = 504 words = 2032 bytes
  //It is therefore safe if we get one page (4k) per buffer
  for(card = 0; card < maxcards; card++)
  {
    req_dma_active[card] = 0;
    reqbuf[card] = (u_int)__get_free_page(GFP_ATOMIC);
    if (!reqbuf[card])
    {
      kdebug(("quest(init_module): error from __get_free_pages for reqbuf\n"));
      return(-QUEST_EFAULT);
    }
    reqadd[card] = virt_to_bus((void *) reqbuf[card]);
    kdebug(("quest(init_module): reqbuf[%d] = 0x%08x (PCI = 0x%08x)\n", card, reqbuf[card], reqadd[card]));
  }
  
  opid = 0;
  kdebug(("quest(init_module): driver loaded; major device number = %d\n", quest_major));
  return(0);
}


/***********************/
void cleanup_module(void)
/***********************/
{
  u_int card, loop;
  struct page *page_ptr;
      
  opid = 0;
  cleanup_quests();

  // unreserve all pages used for the S/W FIFOs
  page_ptr = virt_to_page(swfifobase);

  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    clear_bit(PG_reserved, &page_ptr->flags);

  // free the area 
  free_pages(swfifobase, order);
  
  // Return the buffers for the REQ blocks
  for(card = 0; card < maxcards; card++)
    free_page(reqbuf[card]);
  
  if (unregister_chrdev(quest_major, "quest") != 0) 
  {
    kdebug(("quest(cleanup_module): cleanup_module failed\n"));
  }
  
  remove_proc_entry("quest", NULL);
  kfree(proc_read_text);

  kdebug(("quest(cleanup_module): driver removed\n"));
}


/*************************************************************************/
static void quest_irq_handler (int irq, void *dev_id, struct pt_regs *regs)
/*************************************************************************/
{
  u_int card;

  // Note: the QUEST card(s) may have to share an interrupt line with other PCI devices.
  // It is therefore important to exit quickly if we are not concerned with an interrupt.
  // This is done by looking at REQBLK_DONE bit in the OPSTAT register. 
  
  for(card = 0; card < maxcards; card++)
  {
    if ((quest_irq_line[card] == (u_int)irq) && quest[card]->opstat & 0x1)
    {
      kdebug(("quest(quest_irq_handler): Interrupt received\n"));
      //Acknowledge (and clear) the inetrrupt
      quest[card]->intctl |= 0x5;
      req_dma_active[card] = 0;
      refill_req_fifo_dma(card); 
    }
  }  
}


/**************************/
static int init_quests(void)
/**************************/
{
  struct pci_dev *quest_dev[MAXQUESTCARDS];   // "soft" QUEST structures
  u_char pci_bus[MAXQUESTCARDS], pci_device_fn[MAXQUESTCARDS];
  u_int ok, result, card, loop;
  
  // Find all QUEST modules
  maxcards = 0;
  for(card = 0; card < MAXQUESTCARDS; card++)
  {
    quest_dev[card] = 0;
    ok = 1;
    for(loop = 0; loop < (card + 1); loop++)
    {
      kdebug(("quest(init_quests): card = %d  MAXQUESTCARDS = %d\n", card, MAXQUESTCARDS));
      quest_dev[card] = pci_find_device(PCI_VENDOR_ID_CERN, QUEST_PCI_DEVICE_ID, quest_dev[card]);  //Find N-th device
      if (quest_dev[card] == NULL)
      {
        kdebug(("quest(init_quests): QUEST %d not found\n", card + 1));
        ok = 0;
        break;
      }
      else
      {
        kdebug(("quest(init_quests): QUEST %d found. quest_dev[%d]=0x%08x\n", loop + 1, card, quest_dev[card]));
      }
    }
    if (ok)
      maxcards++;
  }
  
  if (!maxcards)   // No QUESTs found
    return(-QUEST_ENOSYS);
    
  kdebug(("quest(init_quests): %d QUESTs found\n", maxcards));
  
  // Initialize the available QUEST modules
  for(card = 0; card < maxcards; card++)
  {
    pci_bus[card]       = quest_dev[card]->bus->number;
    pci_device_fn[card] = quest_dev[card]->devfn;
    pci_memaddr[card]   = quest_dev[card]->resource[0].start;

    // get revision directly from the QUEST
    pci_read_config_byte(quest_dev[card], PCI_REVISION_ID, &pci_revision[card]);

    kdebug(("quest(init_quests): QUEST found: \n"));
    kdebug(("quest(init_quests): bus = %x device = %x revision = %x address = 0x%08x\n", pci_bus[card], pci_device_fn[card], pci_revision[card], pci_memaddr[card]));

    if (pci_revision[card] < MINREV)
    {
      kdebug(("quest(init_quests): Illegal QUEST Revision\n"));
      return(-QUEST_ILLREV);
    }

    if ((pci_memaddr[card] & PCI_BASE_ADDRESS_SPACE) == PCI_BASE_ADDRESS_SPACE_IO)
    {
      kdebug(("quest(init_quests): The device uses io addresses\n"));
      kdebug(("quest(init_quests): But this driver is made for memory mapped access\n"));
      return(-QUEST_EIO);
    }

    quest[card] = (T_quest_regs *)ioremap(pci_memaddr[card] & PCI_BASE_ADDRESS_MEM_MASK, 4 * 1024);

    pci_read_config_dword(quest_dev[card], PCI_INTERRUPT_LINE, &quest_irq_line[card]);
    kdebug(("quest(init_quests): interrupt pin = %d, interrupt line = %d \n", (quest_irq_line[card] & 0x0000ff00)>>8, quest_irq_line[card] & 0x000000ff));

    quest_irq_line[card] &= 0x000000FF;

    result = request_irq(quest_irq_line[card], quest_irq_handler, (SA_INTERRUPT | SA_SHIRQ), "quest", quest_irq_handler); // Rubini p.275
    if (result)
    {
      kdebug(("quest(init_quests): request_irq failed on IRQ = %d\n", quest_irq_line[card]));
      return(-QUEST_REQIRQ);
    }
    kdebug(("quest(init_quests): request_irq OK\n"));
  }
  
  return(0);
}	


/*****************************/
static int cleanup_quests(void)
/*****************************/
{
  u_int card;

  for(card = 0; card < maxcards; card++)
  {
    quest[card]->intctl |= 0x5; // clear any outstanding interrupts
    quest[card]->intmask = 0;   // mask the interrupts
    free_irq(quest_irq_line[card], quest_irq_handler);
    kdebug(("quest(cleanup_quest): interrupt line %d released\n", quest_irq_line[card]));

    iounmap((void *)quest[card]);
  }

  return(0);
}


/***********************************************************/
int quest_mmap(struct file *file, struct vm_area_struct *vma)
/***********************************************************/
{
  unsigned long offset, size;

  kdebug(("quest(quest_mmap): function called\n"));
  
  offset = vma->vm_pgoff << PAGE_SHIFT;  
  size = vma->vm_end - vma->vm_start;

  kdebug(("quest(quest_mmap): offset = 0x%08x\n",(u_int)offset));
  kdebug(("quest(quest_mmap): size   = 0x%08x\n",(u_int)size));

  if (offset & ~PAGE_MASK)
  {
    kdebug(("quest(quest_mmap): offset not aligned: %ld\n", offset));
    return -ENXIO;
  }

  // we only support shared mappings. "Copy on write" mappings are
  // rejected here. A shared mapping that is writeable must have the
  // shared flag set.
  if ((vma->vm_flags & VM_WRITE) && !(vma->vm_flags & VM_SHARED))
  {
    kdebug(("quest(quest_mmap): writeable mappings must be shared, rejecting\n"));
    return(-EINVAL);
  }

  vma->vm_flags |= VM_RESERVED;
  
  // we do not want to have this area swapped out, lock it
  vma->vm_flags |= VM_LOCKED;

  // we create a mapping between the physical pages and the virtual
  // addresses of the application with remap_page_range.
  // enter pages into mapping of application
  kdebug(("quest(quest_mmap): Parameters of remap_page_range()\n"));
  kdebug(("quest(quest_mmap): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("quest(quest_mmap): Physical address = 0x%08x\n",(u_int)offset));
  kdebug(("quest(quest_mmap): Size             = 0x%08x\n",(u_int)size));
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,20)
  if (remap_page_range(vma, vma->vm_start, offset, size, vma->vm_page_prot))
#else
  if (remap_page_range(vma->vm_start, offset, size, vma->vm_page_prot))
#endif
  {
    kdebug(("quest(quest_mmap): remap page range failed\n"));
    return -ENXIO;
  }
  
  vma->vm_ops = &quest_vm_ops;  
  
  kdebug(("quest(quest_mmap): function done\n"));
  return(0);
}


/*******************************************/
void quest_vclose(struct vm_area_struct *vma)
/*******************************************/
{  
  kdebug(("quest(quest_vclose): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("quest(quest_vclose): mmap released\n"));
}


//------------------
// Service functions
//------------------


/*************************************************************/
static int in_fifo_pop(int channel, u_int *data1, u_int *data2)
/*************************************************************/
{
  int index;
  
  if (infifon[channel] == 0)
  {
    kdebug(("quest(in_fifo_pop): The IN FIFO is empty\n"));
    return(-1);
  }
  
  index = QUEST_IN_INDEX(channel, infifor[channel]);
  *data1 = infifo[index];
  *data2 = infifo[index + 1];
  kdebug(("quest(in_fifo_pop): index=%d data1=0x%08x  data2=0x%08x\n", index, *data1, *data2));

  infifor[channel]++;

  if (infifor[channel] > (MAXQUESTINFIFO - 1))
    infifor[channel] = 0;

  infifon[channel]--;

  return(0);
}


/***************************************/
static void refill_req_fifo_dma(int card)
/***************************************/
{
  int channel;
  int reqlen = 2; // 4 32-bit words for the entry count -> 2 64-bit words
  int rol = 4 * card;
  u_int *reqptr = (u_int *) reqbuf[card];
  u_int fstat = quest[card]->fstat;
  kdebug(("quest(refill_req_fifo_dma): fstat = 0x%08x\n", fstat));

  if (req_dma_active[card]) 
  {
    kdebug(("quest(refill_req_fifo_dma): Still busy....\n"));
    return;
  }
    
  for (channel = 0; channel < MAXQUESTCHANNELS; channel++)
  {
    int loop;
    int numfree = fstat & 0xff; 
    int numreq = infifon[rol];
    int numcopy = (numfree < numreq) ? numfree : numreq;
    kdebug(("quest(refill_req_fifo_dma): rol = %d\n", rol));
    kdebug(("quest(refill_req_fifo_dma): numfree = %d, numreq = %d, numcopy = %d\n", numfree, numreq, numcopy));
    
    *reqptr++ = numcopy << 1; // number of requests x 2
    
    for (loop = 0; loop < numcopy; loop++)
    {
      u_int addr, size;
      in_fifo_pop(rol, &addr, &size);
      *reqptr++ = addr; // address of the data block
      *reqptr++ = size; // block size and control bits
    }
    served[rol] += numcopy;
    reqlen += numcopy;
    fstat >>= 8;
    rol++;
  }
  
  // Post the request
  if (reqlen > 2) // at least one request
  {
    req_dma_active[card] = 1;
    quest[card]->blklen = reqlen; // reqlen is in 64 bit words
    quest[card]->reqadd = reqadd[card];
  }  
}


