/************************************************************************/
/*									*/
/* File: solar_driver.c							*/
/*									*/
/* driver for the SOLAR S-Link interface				*/
/*									*/
/* 2. Apr. 03  MAJO  created						*/
/*									*/
/************ C 2003 - The software with that certain something *********/

/************************************************************************/
/*NOTES:								*/
/*- This driver should work on kernels from 2.4 onwards			*/
/************************************************************************/

// Module Versioning a la Rubini p.316
#include <linux/config.h>

#if defined(CONFIG_MODVERSIONS) && !defined(MODVERSIONS)
  #define MODVERSIONS
#endif

#if defined(MODVERSIONS)
  #include <linux/modversions.h>
#endif

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/mm.h>
#include <linux/vmalloc.h>
#include <linux/mman.h>
#include <linux/delay.h>
#include <linux/string.h>
#include <asm/io.h>
#include <asm/uaccess.h>
#include "ROSsolar/solar_driver.h"

#ifdef MODULE
  MODULE_PARM (debug, "i");
  MODULE_PARM_DESC(debug, "1 = enable debugging   0 = disable debugging");
  MODULE_DESCRIPTION("S-Link SOLAR");
  MODULE_AUTHOR("Markus Joos, CERN/EP");
  #ifdef MODULE_LICENSE
    MODULE_LICENSE("Private: Contact markus.joos@cern.ch");
  #endif
#endif


/*********/
/*Globals*/
/*********/
static int debug = 0;
static int solar_major = 0; // use dynamic allocation
static volatile T_solar_regs *solar[MAXCARDS];
static u_int solar_irq_line[MAXCARDS];
static u_int maxcards, pci_memaddr[MAXCARDS];
static u_char pci_revision[MAXCARDS];
static u_int served[MAXCARDS];
static u_int *infifo, *infifon;
static u_int infifor[MAXCARDS];
static u_int order, swfifobasephys, swfifobase, infifodatasize, fifoptrsize, tsize = 0;
static struct proc_dir_entry *solar_file;
struct solar_proc_data_t solar_proc_data;
static char *proc_read_text;

/************/
/*Prototypes*/
/************/
static void solar_irq_handler (int irq, void *dev_id, struct pt_regs *regs);
void solar_vclose(struct vm_area_struct *vma);


static struct file_operations fops = 
{
  owner:   THIS_MODULE,
  mmap:    solar_mmap,
  ioctl:   solar_ioctl,
  open:    solar_open,    
  release: solar_release
};

// memory handler functions
static struct vm_operations_struct solar_vm_ops = 
{
  close:  solar_vclose,   // mmap-close
};


/*****************************/
/* Standard driver functions */
/*****************************/


/**********************************************************/
static int solar_open(struct inode *ino, struct file *filep)
/**********************************************************/
{
  MOD_INC_USE_COUNT;
  return(0);
}


/*************************************************************/
static int solar_release(struct inode *ino, struct file *filep)
/*************************************************************/
{
  MOD_DEC_USE_COUNT;
  return(0);
}

/*************************************************************************************************/
static int solar_ioctl(struct inode *inode, struct file *file, u_int cmd, unsigned long arg)
/*************************************************************************************************/
{
  switch (cmd)
  {    
    case OPEN:
    {
      SOLAR_open_t params;
      
      params.size = tsize;
      params.physbase = swfifobasephys;
      params.insize = infifodatasize;
      kdebug(("solar(ioctl,OPEN): params.size     = 0x%08x\n", params.size));
      kdebug(("solar(ioctl,OPEN): params.physbase = 0x%08x\n", params.physbase));
      kdebug(("solar(ioctl,OPEN): params.insize   = 0x%08x\n", params.insize));
  
      if (copy_to_user((void *)arg, &params, sizeof(SOLAR_open_t)) != 0)
      {
	kdebug(("solar(ioctl, OPEN): error from copy_to_user\n"));
	return(-SOLAR_EFAULT);
      }      
      return(0);
      break;
    }

    case CONFIG:
    { 
      SOLAR_config_t params;
      u_int card, data;
      
      if (copy_from_user(&params, (void *)arg, sizeof(SOLAR_config_t)) !=0 )
      {
	kdebug(("solar(ioctl,CONFIG): error from copy_from_user\n"));
	return(-SOLAR_EFAULT);
      }
      kdebug(("solar(ioctl,CONFIG): \n"));        
      kdebug(("solar(ioctl,CONFIG): params.bswap  = %d\n", params.bswap));
      kdebug(("solar(ioctl,CONFIG): params.wswap  = %d\n", params.wswap));
      kdebug(("solar(ioctl,CONFIG): params.scw    = 0x%08x\n", params.scw));
      kdebug(("solar(ioctl,CONFIG): params.ecw    = 0x%08x\n", params.ecw));
      kdebug(("solar(ioctl,CONFIG): params.udw    = %d\n", params.udw));
      kdebug(("solar(ioctl,CONFIG): params.clksel = %d\n", params.clksel));
      kdebug(("solar(ioctl,CONFIG): params.clkdiv = %d\n", params.clkdiv));

      for(card = 0; card < maxcards; card++)
      {
        data = 0;  
        solar[card]->opctrl = (params.udw << 18) + (params.wswap << 2) + (params.bswap << 1);
        solar[card]->bctrlw = params.scw & 0xfffffffc;
        solar[card]->ectrlw = params.ecw & 0xfffffffc;
        solar[card]->opfeat = (params.clksel << 31) + (params.clkdiv << 29);
        
        //The LFF and LDOWN interrupts are not yet supported
        //If enabled the REQ interrupt has a fixed threshold of 32
        //This is to reduce the IRQ frequency
        solar[card]->intmask = INTERRUPT_THRESHOLD;

        served[card] = 0;
      }
      break;
    }
    
    case RESET:
    {
      u_int data, card;
      
      if (copy_from_user(&card, (void *)arg, sizeof(int)) !=0 )
      {
	kdebug(("solar(ioctl,RESET): error from copy_from_user\n"));
	return(-SOLAR_EFAULT);
      }
     
      kdebug(("solar(ioctl,RESET): Resetting card %d\n", card));
      data = solar[card]->opctrl;
      data |= 0x1;
      solar[card]->opctrl = data;
      // Delay for at least one us
      udelay(2);
      data &= 0xfffffffe;
      solar[card]->opctrl = data;

      //reset the FIFOs
      infifor[card] = 0;
      infifon[card] = 0;

      return(0);
      break;
    }
    
    case LINKRESET:
    {   
      u_int waited = 0,  card;
 
      if (copy_from_user(&card, (void *)arg, sizeof(int)) !=0 )
      {
	kdebug(("solar(ioctl,LINKRESET): error from copy_from_user\n"));
	return(-SOLAR_EFAULT);
      }
      
      kdebug(("solar(ioctl,LINKRESET): Resetting link of card %d\n", card));

      // Set the URESET bit
      solar[card]->opctrl |= 0x00020000;
      
      // Delay for at least two ms
      udelay(2000);

      // Now wait for LDOWN to come up again
      while(solar[card]->opstat & 0x00040000)
      {
        waited++;
        if (waited > 10000)
        {
          kdebug(("solar(ioctl,LINKRESET): card %d does not come up again\n", card));
          // Reset the URESET bit
          solar[card]->opctrl &= 0xfffdffff;
	  return(-SOLAR_STUCK);
        }          
      }

      // Reset the URESET bit
      solar[card]->opctrl &= 0xfffdffff;

      served[card] = 0;
      return(0);
      break;
    }
    
    case LINKSTATUS:
    {              
      unsigned long flags;
      SOLAR_status_t params;
      
      if (copy_from_user(&params, (void *)arg, sizeof(SOLAR_status_t)) !=0 )
      {
	kdebug(("solar(ioctl, LINKSTATUS): error from copy_from_user\n"));
	return(-SOLAR_EFAULT);
      }
      
      if (solar[params.card]->opstat & 0x00040000)
        params.ldown = 1;
      else
        params.ldown = 0;
      
      //We have to make sure that this code does not get interrupted. 
      save_flags(flags);
      cli();       
      params.infifo = infifon[params.card];
      params.reqfifo = REQFIFODEPTH - (solar[params.card]->opstat) & 0x3f; 
      kdebug(("solar(ioctl, LINKSTATUS): params.ldown = %d\n", params.ldown));
      kdebug(("solar(ioctl, LINKSTATUS): params.infifo = %d\n", params.infifo));
      kdebug(("solar(ioctl, LINKSTATUS): params.reqfifo = %d\n", params.reqfifo));
      restore_flags(flags);

      if (copy_to_user((void *)arg, &params, sizeof(SOLAR_status_t)) != 0)
      {
	kdebug(("solar(ioctl, LINKSTATUS): error from copy_to_user\n"));
	return(-SOLAR_EFAULT);
      } 
      
      return(0);
      break;
    }

    case INFO:
    {
      SOLAR_info_t info;
      u_int card;
     
      //Initialise the array to 1 (it is not possible to disable a SOLAR card via a register)
      for(card = 0; card < MAXCARDS; card++)
      {
        info.cards[card] = 0;
        info.enabled[card] = 0;
      }
      
      for(card = 0; card < maxcards; card++)
      {
        info.cards[card] = 1;
        info.enabled[card] = 1;
      }
      
      info.ncards = maxcards;  
      kdebug(("solar(ioctl, INFO): maxcards = %d\n", maxcards));

      if (copy_to_user((void *)arg, &info, sizeof(SOLAR_info_t)) != 0)
      {
	kdebug(("solar(ioctl, INFO): error from copy_to_user\n"));
	return(-SOLAR_EFAULT);
      }      
      return(0);
      break;
    }
    
    case FIFOIN:
    { 
      u_int card;

      if (copy_from_user(&card, (void *)arg, sizeof(int)) !=0)
      {
	kdebug(("solar(ioctl,FIFOIN): error from copy_from_user\n"));
	return(-SOLAR_EFAULT);
      }
      kdebug(("solar(ioctl,FIFOIN): card = %d\n", card));

      // (re)enable the interupt. Once the REQ FIFO is empty the ISR will 
      // fill it with the new requests
      solar[card]->intmask = INTERRUPT_THRESHOLD;

      return(0);
      break;
    }
  }   
  return(0);
}



/****************************************************************************************************/
static int solar_write_procmem(struct file *file, const char *buffer, unsigned long count, void *data)
/****************************************************************************************************/
{
  int res, len;
  u_int card;
  struct solar_proc_data_t *fb_data = (struct solar_proc_data_t *)data;

  kdebug(("solar(solar_write_procmem): solar_write_procmem called\n"));

  if(count > 99)
    len = 99;
  else
    len = count;

  if (copy_from_user(fb_data->value, buffer, len))
  {
    kdebug(("solar(solar_write_procmem): error from copy_from_user\n"));
    return(-SOLAR_EFAULT);
  }

  kdebug(("solar(solar_write_procmem): len = %d\n", len));
  fb_data->value[len - 1] = '\0';
  kdebug(("solar(solar_write_procmem): text passed = %s\n", fb_data->value));

  if (!strcmp(fb_data->value, "debug"))
  {
    debug = 1;
    kdebug(("solar(solar_write_procmem): debugging enabled\n"));
  }

  if (!strcmp(fb_data->value, "nodebug"))
  {
    kdebug(("solar(solar_write_procmem): debugging disabled\n"));
    debug = 0;
  }
  
  return len;
}




/***************************************************************************************************/
static int solar_read_procmem(char *buf, char **start, off_t offset, int count, int *eof, void *data)
/***************************************************************************************************/
{
  u_int card, ocr, osr, imask;
  int loop, nchars = 0;
  static int len = 0;

  kdebug(("solar(solar_read_procmem): Called with buf    = 0x%08x\n", (u_int)buf));
  kdebug(("solar(solar_read_procmem): Called with *start = 0x%08x\n", (u_int)*start));
  kdebug(("solar(solar_read_procmem): Called with offset = %d\n", (u_int)offset));
  kdebug(("solar(solar_read_procmem): Called with count  = %d\n", count));

  if (offset == 0)
  {
    kdebug(("exp(exp_read_procmem): Creating text....\n"));
    len = 0;
    len += sprintf(proc_read_text + len, "\n\n\nSOLAR driver for release %s (based on CVS tag %s)\n", RELEASE_NAME, CVSTAG);

    len += sprintf(proc_read_text + len, "==================================================\n");
    len += sprintf(proc_read_text + len, "Card|IRQ line| REQ IRQ|revision|PCI MEM addr.|wswap|bswap|LDOWN|REQ available|REQs posted|\n");
    len += sprintf(proc_read_text + len, "----|--------|--------|--------|-------------|-----|-----|-----|-------------|-----------|\n");
    for(card = 0; card < maxcards; card++)
    {
      ocr = solar[card]->opctrl;
      osr = solar[card]->opstat;
      imask = solar[card]->intmask;
      len += sprintf(proc_read_text + len, "   %d|", card);
      len += sprintf(proc_read_text + len, "      %2d|", solar_irq_line[card]);
      len += sprintf(proc_read_text + len, "      %2d|", (imask & 0xf)); 
      len += sprintf(proc_read_text + len, "    0x%02x|", pci_revision[card]); 
      len += sprintf(proc_read_text + len, "   0x%08x|", pci_memaddr[card]);
      len += sprintf(proc_read_text + len, "  %s|", (ocr & 0x4)?"yes":" no");
      len += sprintf(proc_read_text + len, "  %s|", (ocr & 0x2)?"yes":" no");
      len += sprintf(proc_read_text + len, "  %s|", (osr & 0x00040000)?"yes":" no");
      len += sprintf(proc_read_text + len, "           %2d|", (osr & 0x3f));
      len += sprintf(proc_read_text + len, "%11d|\n", served[card]);
    }

    len += sprintf(proc_read_text + len, " \n");
    len += sprintf(proc_read_text + len, "The command 'echo <action> > /proc/solar', executed as root,\n");
    len += sprintf(proc_read_text + len, "allows you to interact with the driver. Possible actions are:\n");
    len += sprintf(proc_read_text + len, "debug   -> enable debugging\n");
    len += sprintf(proc_read_text + len, "nodebug -> disable debugging\n");
  }
  kdebug(("solar(solar_read_procmem): number of characters in text buffer = %d\n", len));

  if (count < (len - offset))
    nchars = count;
  else
    nchars = len - offset;
  kdebug(("solar(solar_read_procmem): min nchars         = %d\n", nchars));
  
  if (nchars > 0)
  {
    for (loop = 0; loop < nchars; loop++)
      buf[loop + (offset & (PAGE_SIZE - 1))] = proc_read_text[offset + loop];
    *start = buf + (offset & (PAGE_SIZE - 1));
  }
  else
  {
    nchars = 0;
    *eof = 1;
  }
 
  kdebug(("solar(solar_read_procmem): returning *start   = 0x%08x\n", (u_int)*start));
  kdebug(("solar(solar_read_procmem): returning nchars   = %d\n", nchars));
  return(nchars);
}


/*******************/
int init_module(void)
/*******************/
{
  int card, tfsize, result;
  u_int loop;
  struct page *page_ptr;
     
  SET_MODULE_OWNER(&fops);

  result = init_solars();
  if (result)
  {
    kdebug(("solar(init_module): init_solar() failed\n"));
    return(result);
  }

  result = register_chrdev(solar_major, "solar", &fops); 
  if (result < 1)
  {
    kdebug(("solar(init_module): registering SOLAR driver failed.\n"));
    return(-SOLAR_EIO);
  }
  solar_major = result;

  proc_read_text = (char *)kmalloc(MAX_PROC_TEXT_SIZE, GFP_KERNEL);
  if (proc_read_text == NULL)
  {
    kdebug(("solar(init_module): error from kmalloc\n"));
    return(-EFAULT);
  }

  solar_file = create_proc_entry("solar", 0644, NULL);
  if (solar_file == NULL)
  {
    kdebug(("solar(init_module): error from call to create_proc_entry\n"));
    return (-ENOMEM);
  }

  strcpy(solar_proc_data.name, "solar");
  strcpy(solar_proc_data.value, "solar");
  solar_file->data = &solar_proc_data;
  solar_file->read_proc = solar_read_procmem;
  solar_file->write_proc = solar_write_procmem;
  solar_file->owner = THIS_MODULE;    
  
  // Allocate contiguous memory for the FIFOs and store the base addresses in a global structure
  infifodatasize = MAXCARDS * MAXINFIFO * sizeof(u_int) * 2;  //infifo
  fifoptrsize = MAXCARDS * sizeof(u_int);                     //infifo(w/r/n)
  kdebug(("solar(init_module): 0x%08x bytes needed for the IN FIFO\n", infifodatasize));
  kdebug(("solar(init_module): 0x%08x bytes needed for the FIFO pointers\n", fifoptrsize));
  
  tsize = infifodatasize + fifoptrsize;
  kdebug(("solar(init_module): 0x%08x bytes needed for all FIFOs\n", tsize));

  tfsize = (tsize + 4095) & 0xfffff000;  // round up to next 4K boundary
  tfsize = tfsize >> 12;                 // compute number of pages
  kdebug(("solar(init_module): %d pages needed for the S/W FIFOs\n", tfsize));
 
  order = 0;
  while(tfsize)
  {
    order++;
    tfsize = tfsize >> 1;                 // compute order
  } 
  kdebug(("solar(init_module): order = %d\n", order));
  
  swfifobase = __get_free_pages(GFP_ATOMIC, order);
  if (!swfifobase)
  {
    kdebug(("rsolar(init_module): error from __get_free_pages\n"));
    return(-SOLAR_EFAULT);
  }
  kdebug(("solar(init_module): swfifobase = 0x%08x\n", swfifobase));
   
  // Reserve all pages to make them remapable
  // This code fragment has been taken from the PCI (SHASLink) driver of M. Mueller
  page_ptr = virt_to_page(swfifobase);

  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    set_bit(PG_reserved, &page_ptr->flags);

  swfifobasephys = virt_to_bus((void *) swfifobase);
  kdebug(("solar(init_module): swfifobasephys = 0x%08x\n", swfifobasephys));

  // Assign base addresses to the FIFO arrays
  infifo  = (u_int *)swfifobase;
  infifon = (u_int *)(swfifobase + infifodatasize);
  kdebug(("solar(init_module): infifo   is at 0x%08x\n", (u_int)infifo));
  kdebug(("solar(init_module): infifon  is at 0x%08x\n", (u_int)infifon));

  //Initialize the FIFOs
  for(card = 0; card < MAXCARDS; card++)
  {
    infifor[card] = 0;
    infifon[card] = 0;
  }

  kdebug(("solar(init_module): driver loaded; major device number = %d\n", solar_major));
  return(0);
}


/***********************/
void cleanup_module(void)
/***********************/
{
  u_int loop;
  struct page *page_ptr;
      
  cleanup_solars();

  // unreserve all pages used for the S/W FIFOs
  page_ptr = virt_to_page(swfifobase);

  for (loop = (1 << order); loop > 0; loop--, page_ptr++)
    clear_bit(PG_reserved, &page_ptr->flags);

  // free the area 
  free_pages(swfifobase, order);

  if (unregister_chrdev(solar_major, "solar") != 0) 
  {
    kdebug(("solar(cleanup_module): cleanup_module failed\n"));
  }
  
  remove_proc_entry("solar", NULL);
  kfree(proc_read_text);

  kdebug(("solar(cleanup_module): driver removed\n"));
}


/*************************************************************************/
static void solar_irq_handler (int irq, void *dev_id, struct pt_regs *regs)
/*************************************************************************/
{
  u_int card, for_us;

  // Note: the SOLAR card(s) may have to share an interrupt line with other PCI devices.
  // It is therefore important to exit quickly if we are not concerned with an interrupt.
  // For now this is done by looking at the OPSTAT register. There may be a dedicated
  // interrupt status bit in the future.
  
  for_us = 0;
  for(card = 0; card < maxcards; card++)
    for_us += solar[card]->opstat & 0x3f;

  if (for_us)
  {
    kdebug(("solar(solar_irq_handler): Interrupt received\n"));
    refill_req_fifo();
  }
}


/**************************/
static int init_solars(void)
/**************************/
{
  struct pci_dev *solar_dev[MAXCARDS];   // "soft" SOLAR structures
  u_char pci_bus[MAXCARDS], pci_device_fn[MAXCARDS];
  u_int ok, result, card, loop;
  
  // Find all SOLAR modules
  maxcards = 0;
  for(card = 0; card < MAXCARDS; card++)
  {
    solar_dev[card] = 0;
    ok = 1;
    for(loop = 0; loop < (card + 1); loop++)
    {
      kdebug(("solar(init_solars): card = %d  MAXCARDS = %d\n", card, MAXCARDS));
      solar_dev[card] = pci_find_device(PCI_VENDOR_ID_CERN, 0x17, solar_dev[card]);  //Find N-th device
      if (solar_dev[card] == NULL)
      {
        kdebug(("solar(init_solars): SOLAR %d not found\n", card + 1));
        ok = 0;
        break;
      }
      else
      {
        kdebug(("solar(init_solars): SOLAR %d found. solar_dev[%d]=0x%08x\n", loop + 1, card, solar_dev[card]));
      }
    }
    if (ok)
      maxcards++;
  }
  
  if (!maxcards)   // No SOLARs found
    return(-SOLAR_ENOSYS);
    
  kdebug(("solar(init_solars): %d SOLARs found\n", maxcards));
  
  // Initialize the available SOLAR modules
  for(card = 0; card < maxcards; card++)
  {
    pci_bus[card] = solar_dev[card]->bus->number;
    pci_device_fn[card] = solar_dev[card]->devfn;

    pci_memaddr[card] = solar_dev[card]->resource[0].start;

    // get revision directly from the Solar
    pci_read_config_byte(solar_dev[card], PCI_REVISION_ID, &pci_revision[card]);

    kdebug(("solar(init_solars): Solar found: \n"));
    kdebug(("solar(init_solars):  bus = %x device = %x revision = %x address = 0x%08x\n", pci_bus[card], pci_device_fn[card], pci_revision[card], pci_memaddr[card]));

    if (pci_revision[card] < MINREV)
    {
      kdebug(("solar(init_solars): Illegal Solar Revision\n"));
      return(-SOLAR_ILLREV);
    }

    if ((pci_memaddr[card] & PCI_BASE_ADDRESS_SPACE) == PCI_BASE_ADDRESS_SPACE_IO)
    {
      kdebug(("solar(init_solars): The device uses io addresses\n"));
      kdebug(("solar(init_solars): But this driver is made for memory mapped access\n"));
      return(-SOLAR_EIO);
    }

    solar[card] = (T_solar_regs *)ioremap(pci_memaddr[card] & PCI_BASE_ADDRESS_MEM_MASK, 4 * 1024);

    pci_read_config_dword(solar_dev[card], PCI_INTERRUPT_LINE, &solar_irq_line[card]);
    kdebug(("solar(init_solars): interrupt pin = %d, interrupt line = %d \n", (solar_irq_line[card] & 0x0000ff00)>>8, solar_irq_line[card] & 0x000000ff));

    solar_irq_line[card] &= 0x000000FF;

    result = request_irq(solar_irq_line[card], solar_irq_handler, (SA_INTERRUPT | SA_SHIRQ), "solar", solar_irq_handler); // Rubini p.275
    if (result)
    {
      kdebug(("solar(init_solars): request_irq failed on IRQ = %d\n", solar_irq_line[card]));
      return(-SOLAR_REQIRQ);
    }
    kdebug(("solar(init_solars): request_irq OK\n"));
  }
  
  return(0);
}	


/*****************************/
static int cleanup_solars(void)
/*****************************/
{
  u_int card;

  for(card = 0; card < maxcards; card++)
  {
    free_irq(solar_irq_line[card], solar_irq_handler);
    kdebug(("solar(cleanup_solar): interrupt line %d released\n", solar_irq_line[card]));

    iounmap((void *)solar[card]);
  }

  return(0);
}


/***********************************************************/
int solar_mmap(struct file *file, struct vm_area_struct *vma)
/***********************************************************/
{
  unsigned long offset, size;

  kdebug(("solar(solar_mmap): function called\n"));
  
  offset = vma->vm_pgoff << PAGE_SHIFT;  
  size = vma->vm_end - vma->vm_start;

  kdebug(("solar(solar_mmap): offset = 0x%08x\n",(u_int)offset));
  kdebug(("solar(solar_mmap): size   = 0x%08x\n",(u_int)size));

  if (offset & ~PAGE_MASK)
  {
    kdebug(("solar(solar_mmap): offset not aligned: %ld\n", offset));
    return -ENXIO;
  }

  // we only support shared mappings. "Copy on write" mappings are
  // rejected here. A shared mapping that is writeable must have the
  // shared flag set.
  if ((vma->vm_flags & VM_WRITE) && !(vma->vm_flags & VM_SHARED))
  {
    kdebug(("solar(solar_mmap): writeable mappings must be shared, rejecting\n"));
    return(-EINVAL);
  }

  vma->vm_flags |= VM_RESERVED;
  
  // we do not want to have this area swapped out, lock it
  vma->vm_flags |= VM_LOCKED;

  // we create a mapping between the physical pages and the virtual
  // addresses of the application with remap_page_range.
  // enter pages into mapping of application
  kdebug(("solar(solar_mmap): Parameters of remap_page_range()\n"));
  kdebug(("solar(solar_mmap): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("solar(solar_mmap): Physical address = 0x%08x\n",(u_int)offset));
  kdebug(("solar(solar_mmap): Size             = 0x%08x\n",(u_int)size));
#if LINUX_VERSION_CODE >= KERNEL_VERSION(2,4,20)
  if (remap_page_range(vma, vma->vm_start, offset, size, vma->vm_page_prot))
#else
  if (remap_page_range(vma->vm_start, offset, size, vma->vm_page_prot))
#endif
  {
    kdebug(("solar(solar_mmap): remap page range failed\n"));
    return -ENXIO;
  }
  
  vma->vm_ops = &solar_vm_ops;  
  MOD_INC_USE_COUNT;
  
  kdebug(("solar(solar_mmap): function done\n"));
  return(0);
}


/*******************************************/
void solar_vclose(struct vm_area_struct *vma)
/*******************************************/
{  
  kdebug(("solar(solar_vclose): Virtual address  = 0x%08x\n",(u_int)vma->vm_start));
  kdebug(("solar(solar_vclose): mmap released\n"));
  MOD_DEC_USE_COUNT;
}


//------------------
// Service functions
//------------------


/**********************************************************/
static int in_fifo_pop(int card, u_int *data1, u_int *data2)
/**********************************************************/
{
  int index;
  
  if (infifon[card] == 0)
  {
    kdebug(("solar(in_fifo_pop): The IN FIFO is empty\n"));
    return(-1);
  }
  
  index = IN_INDEX(card, infifor[card]);
  *data1 = infifo[index];
  *data2 = infifo[index + 1];
  kdebug(("solar(in_fifo_pop): index=%d data1=0x%08x  data2=0x%08x\n", index, *data1, *data2));

  infifor[card]++;

  if (infifor[card] > (MAXINFIFO - 1))
    infifor[card] = 0;

  infifon[card]--;

  return(0);
}


/*******************************/
static void refill_req_fifo(void)
/*******************************/
{
  u_int data1, data2, loop, card, numfree, numreq, numcopy;

  // We got here because one of the cards generated an interrupt.
  // As other cards may also have space in ther REQ FIFOs we process all of them
  // to reduce the interrupt frequency.

  for(card = 0; card < maxcards; card++)
  {       
    // Is there space in the REQ FIFO?
    numfree = (solar[card]->opstat) & 0x3f;

    // Have we got new requests?
    numreq = infifon[card];
    kdebug(("solar(refill_req_fifo): numfree=%d  numreq=%d\n", numfree, numreq));

    if (numfree && !numreq)
    {
      //We have to disable the interrupt as we can not clear it by filling the REQ FIFO
      kdebug(("solar(refill_req_fifo): Disabling interrupt\n"));
      solar[card]->intmask = 0;
    }
    else
    {
      if (numfree < numreq)
        numcopy = numfree;
      else
        numcopy = numreq;
      kdebug(("solar(refill_req_fifo): numcopy=%d \n", numcopy));

      for (loop = 0; loop < numcopy; loop++)
      {
        // As we have checked the number of entries in the IN FIFO we do not have to worry about errors
        in_fifo_pop(card, &data1, &data2);
        solar[card]->reqfifo1 = data1;
        solar[card]->reqfifo2 = data2;
        served[card]++;
        kdebug(("solar(refill_req_fifo): data1=0x%08x  data2=0x%08x\n", data1, data2));
      }
    }
  }
}


