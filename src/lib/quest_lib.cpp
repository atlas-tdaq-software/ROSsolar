/************************************************************************/
/*									*/
/*  This is the QUEST library 	   				        */
/*  Its supports the QUEST F/W on the FILAR PCI card    	        */
/*									*/
/*   10. Feb. 04  MAJO  created						*/
/*									*/
/*******C 2004 - The software with that certain something****************/


/*******************************************************************************/
/* NOTE:								       */
/* Card numbering:							       */
/* The API numbers cards from 0..MAXQUESTCARDS-1 to be compatible with arrays  */
/*******************************************************************************/


#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
//#include <asm/page.h>
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "ROSsolar/quest.h"
#include "ROSsolar/quest_lib.h"

//Globals
static int fd, is_open = 0;
static u_int *infifo, infifow[MAXQUESTROLS], *infifon;
static u_long fifobasevirt;
static QUEST_open_t fifos;  
static QUEST_info_t hwstat;  


/********************************/
QUEST_ErrorCode_t QUEST_Open(void)
/********************************/
{  
  u_int loop, card, ret;
  
  if (is_open)
  {
    is_open++;             //keep track of multiple open calls
    return(RCC_ERROR_RETURN(0, QUEST_SUCCESS));
  }

  //open the error package
  ret = rcc_error_init(P_ID_QUEST, QUEST_err_get);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_Open: Failed to open error package");
    return(RCC_ERROR_RETURN(0, QUEST_ERROR_FAIL)); 
  }
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Open: error package opened"); 
  
  //Initialize an array
  for(loop = 0; loop < MAXQUESTROLS; loop++)
    infifow[loop] = 0;
  
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Open: Opening /dev/quest");
  if ((fd = open("/dev/quest", O_RDWR)) < 0)
  {
    perror("open");
    return(RCC_ERROR_RETURN(0, QUEST_FILE)); 
  }
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Open: /dev/quest is open");
  
  ret = ioctl(fd, QUEST_OPEN, &fifos);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_Open: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Open: S/W FIFOS start at " << fifos.physbase);
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Open: S/W FIFOS size = " << fifos.size);
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Open: S/W FIFOS insize = " << fifos.insize);

  // Map the S/W FIFOs into the user virtual address space
  fifobasevirt = (uintptr_t)mmap(0, fifos.size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, fifos.physbase);
  if ((int)fifobasevirt == 0 || (int)fifobasevirt == -1)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_Open: Error from mmap, errno = " << errno);
    return(RCC_ERROR_RETURN(0, QUEST_MMAP)); 
  }
  
  // Define pointers to the relevant FIFO resources
  // This code has to be kept in sync with the respective section in the driver
  infifo  = (u_int *)fifobasevirt;
  infifon = (u_int *)(fifobasevirt + fifos.insize);
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Open: infifo   is at " << (uintptr_t)infifo);
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Open: infifon  is at " << (uintptr_t)infifon);

  is_open = 1;
    
  //Get information about the number of cards installed  
  ret = QUEST_Info(&hwstat);  
  if (ret)
    return(RCC_ERROR_RETURN(0, ret)); 
   
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Open: hwstat.nacrds = " << hwstat.ncards);
  for(card = 0; card < MAXQUESTCARDS; card++)
  {    
    DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Open: hwstat.cards[" << card << "] = " << hwstat.cards[card]);
  }
  
  return(QUEST_SUCCESS);
}


/*********************************/
QUEST_ErrorCode_t QUEST_Close(void)
/*********************************/
{
  int ret, dummy;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, QUEST_NOTOPEN));
  
  if (is_open > 1)
    is_open--;
  else
  {  
    DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Close: calling munmap");    
    ret = munmap((void *)fifobasevirt, fifos.size);
    if (ret)
    {
      DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_Close: Error from munmap, errno = " << errno);
      return(RCC_ERROR_RETURN(0, QUEST_MUNMAP)); 
    }
    
    ret = ioctl(fd, QUEST_CLOSE, &dummy);
    if (ret)
    {
      DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_Closen: Error from ioctl, errno = " << errno);
      return(RCC_ERROR_RETURN(0, errno));
    }
   
    close(fd);
    is_open = 0;
  }
  
  return(QUEST_SUCCESS);
}


/**********************************************/
QUEST_ErrorCode_t QUEST_Info(QUEST_info_t *info)
/**********************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, QUEST_NOTOPEN));
  
  ret = ioctl(fd, QUEST_INFO, info);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_Info: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(QUEST_SUCCESS);
}


/**************************************************/
QUEST_ErrorCode_t QUEST_Init(QUEST_config_t *config)
/**************************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, QUEST_NOTOPEN));
  
  ret = ioctl(fd, QUEST_CONFIG, config);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_Init: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(QUEST_SUCCESS);
}


/***************************************/
QUEST_ErrorCode_t QUEST_Reset(u_int card)
/***************************************/
{
  u_int rol, loop, ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, QUEST_NOTOPEN));
  
  DEBUG_TEXT(DFDB_ROSQUEST, 20, "QUEST_Reset: called");

  if (card > (hwstat.ncards - 1) || hwstat.cards[card] == 0)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QUEST_Reset: card " << card << " does not exist");
    return(RCC_ERROR_RETURN(0, QUEST_NOHW));
  }
  
  ret = ioctl(fd, QUEST_RESET, &card);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5, "QUEST_Reset: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  for (loop = 0; loop < MAXQUESTCHANNELS; loop++)
  {
    rol = (card << 2) | loop;
    infifow[rol] = 0;
    DEBUG_TEXT(DFDB_ROSFILAR, 20, "QUEST_Reset: resetting S/W FIFO of ROL " << rol);
  }
  
  return(QUEST_SUCCESS);
}


/**********************************************/
QUEST_ErrorCode_t QUEST_LinkReset(u_int channel)
/**********************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, QUEST_NOTOPEN));
  
  if ((channel >> 2) > (hwstat.ncards - 1) || hwstat.channel[channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_LinkReset: channel " << channel << " does not exist");
    return(RCC_ERROR_RETURN(0, QUEST_NOHW));
  }
  
  ret = ioctl(fd, QUEST_LINKRESET, &channel);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_LinkReset: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(QUEST_SUCCESS);
}


/***********************************************************************/
QUEST_ErrorCode_t QUEST_LinkStatus(u_int channel, QUEST_status_t *status)
/***********************************************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, QUEST_NOTOPEN));
 
  status->channel = channel;
  if ((channel >> 2) > (hwstat.ncards - 1) || hwstat.channel[channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_LinkReset: channel " << channel << " does not exist");
    return(RCC_ERROR_RETURN(0, QUEST_NOHW));
  }
  
  ret = ioctl(fd, QUEST_LINKSTATUS, status);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_LinkReset: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(QUEST_SUCCESS);
}


/*************************************************/
QUEST_ErrorCode_t QUEST_PagesIn(QUEST_in_t *fifoin)
/*************************************************/
{
  u_int loop, ret, ok, data;

  if (!is_open) 
    return(RCC_ERROR_RETURN(0, QUEST_NOTOPEN));

  if ((fifoin->channel >> 2) > (hwstat.ncards - 1) || hwstat.channel[fifoin->channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_PagesIn: channel " << fifoin->channel << " does not exist");
    return(RCC_ERROR_RETURN(0, QUEST_NOHW));
  }

  ok = 1;               
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_PagesIn: filling IN FIFO with " << fifoin->nvalid << " entries");

  for (loop = 0; loop < fifoin->nvalid; loop++)
  {    
    //Just in case...
    fifoin->scw[loop] &= 0x1;
    fifoin->ecw[loop] &= 0x1;
    fifoin->size[loop] &= 0xfffff;

    data = (fifoin->scw[loop] << 31) + (fifoin->ecw[loop] << 30) + (fifoin->size[loop]);
    ret = quest_in_fifo_push(fifoin->channel, fifoin->pciaddr[loop], data);
    if (ret)
    {
      DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_PagesIn: error from quest_in_fifo_push (FIFO full)");
      ok = 0;
      break;   //FIFO is full. Stop filling
    }
  }
  
  if (ok)
    return(QUEST_SUCCESS);
  else
    return(RCC_ERROR_RETURN(0, QUEST_FIFOFULL));
}


/******************************************/
QUEST_ErrorCode_t QUEST_Flush(u_int channel)
/******************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, QUEST_NOTOPEN));

  if ((channel >> 2) > (hwstat.ncards - 1) || hwstat.channel[channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_Flush: channel " << channel << " does not exist");
    return(RCC_ERROR_RETURN(0, QUEST_NOHW));
  }
  //Inform the driver about the new entries. 
  //This is needed to get the QUEST driver going if the FIFOs are empty.
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_Flush: Request block DMA initiated");
  ret = ioctl(fd, QUEST_FIFOIN, &channel);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_Flush: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }

  return(QUEST_SUCCESS);
}


/*********************************************************/
QUEST_ErrorCode_t QUEST_InFree(u_int channel, u_int *nfree)
/*********************************************************/
{  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, QUEST_NOTOPEN));

  if ((channel >> 2) > (hwstat.ncards - 1) || hwstat.channel[channel] == 0)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"QUEST_InFree: channel " << channel << " does not exist");
    return(RCC_ERROR_RETURN(0, QUEST_NOHW));
  }
  *nfree = MAXQUESTINFIFO - infifon[channel];   
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"QUEST_InFree: nfree (channel " << channel << ") = " << *nfree);
  return(QUEST_SUCCESS);
}


/*****************************************************************/
unsigned int QUEST_err_get(err_pack err, err_str pid, err_str code)
/*****************************************************************/
{ 
  strcpy(pid, P_ID_QUEST_STR);

  switch (RCC_ERROR_MINOR(err))
  {  
    case QUEST_SUCCESS:       strcpy(code, QUEST_SUCCESS_STR);     break;
    case QUEST_FILE:          strcpy(code, QUEST_FILE_STR);        break;
    case QUEST_NOTOPEN:       strcpy(code, QUEST_NOTOPEN_STR);     break;
    case QUEST_EFAULT:        strcpy(code, QUEST_EFAULT_STR);      break;
    case QUEST_EIO:           strcpy(code, QUEST_EIO_STR);         break;
    case QUEST_ENOSYS:        strcpy(code, QUEST_ENOSYS_STR);      break;
    case QUEST_ILLREV:        strcpy(code, QUEST_ILLREV_STR);      break;
    case QUEST_IOREMAP:       strcpy(code, QUEST_IOREMAP_STR);     break;
    case QUEST_REQIRQ:        strcpy(code, QUEST_REQIRQ_STR);      break;
    case QUEST_KMALLOC:       strcpy(code, QUEST_KMALLOC_STR);     break;
    case QUEST_FIFOFULL:      strcpy(code, QUEST_FIFOFULL_STR);    break;
    case QUEST_MMAP:          strcpy(code, QUEST_MMAP_STR);        break;
    case QUEST_MUNMAP:        strcpy(code, QUEST_MUNMAP_STR);      break;
    case QUEST_NOHW:          strcpy(code, QUEST_NOHW_STR);        break;
    case QUEST_STUCK:         strcpy(code, QUEST_STUCK_STR);       break;
    case QUEST_USED :         strcpy(code, QUEST_USED_STR);        break;
    default:                  strcpy(code, QUEST_NO_CODE_STR);     return(RCC_ERROR_RETURN(0, QUEST_NO_CODE)); break;
  }
  return(RCC_ERROR_RETURN(0, QUEST_SUCCESS));
}


/************************************************************/
int quest_in_fifo_push(u_int channel, u_int data, u_int data2)
/************************************************************/
{
  int index;
  
  if (infifon[channel] == MAXQUESTINFIFO)
  {
    DEBUG_TEXT(DFDB_ROSQUEST, 5 ,"quest_in_fifo_push: The IN FIFO is full");
    return(-1);
  }
  
  index = QUEST_IN_INDEX(channel, infifow[channel]);
  infifo[index] = data;
  infifo[index + 1] = data2;
  DEBUG_TEXT(DFDB_ROSQUEST, 20 ,"quest_in_fifo_push: index = " << index << "  data = " << data << " data2 " << data2);  
    
  infifow[channel]++;
  if (infifow[channel] == MAXQUESTINFIFO)
    infifow[channel] = 0;
  
  infifon[channel]++;
  return(0);
}







