/************************************************************************/
/*									*/
/*  This is the SOLAR library 	   				        */
/*  Its supports the SOLAR F/W on the S32PCI64 PCI card    	        */
/*									*/
/*   2. Apr. 03  MAJO  created						*/
/*									*/
/*******C 2003 - The software with that certain something****************/


/**************************************************************************/
/* NOTE:								  */
/* Card numbering:							  */
/* The API numbers cards from 0..MAXCARDS-1 to be compatible with arrays  */
/**************************************************************************/


#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
//#include <asm/page.h>
#include "rcc_error/rcc_error.h"
#include "DFDebug/DFDebug.h"
#include "ROSsolar/solar.h"
#include "ROSsolar/solar_lib.h"

//Globals
static int fd, is_open = 0;
static u_int *infifo, infifow[MAXCARDS], *infifon;
static u_int fifobasevirt;
static SOLAR_open_t fifos;  
static SOLAR_info_t hwstat;  


/********************************/
SOLAR_ErrorCode_t SOLAR_Open(void)
/********************************/
{  
  u_int card, ret;
  
  if (is_open)
  {
    is_open++;             //keep track of multiple open calls
    return(RCC_ERROR_RETURN(0, SOLAR_SUCCESS));
  }

  //open the error package
  ret = rcc_error_init(P_ID_SOLAR, SOLAR_err_get);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_Open: Failed to open error package");
    return(RCC_ERROR_RETURN(0, SOLAR_ERROR_FAIL)); 
  }
  DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Open: error package opened"); 

  DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Open: Opening /dev/solar");
  if ((fd = open("/dev/solar", O_RDWR)) < 0)
  {
    perror("open");
    return(RCC_ERROR_RETURN(0, SOLAR_FILE)); 
  }
  DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Open: /dev/solar is open");
  
  ret = ioctl(fd, OPEN, &fifos);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_Open: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Open: S/W FIFOS start at " << fifos.physbase);
  DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Open: S/W FIFOS size = " << fifos.size);

  // Map the S/W FIFOs into the user virtual address space
  fifobasevirt = (uintptr_t)mmap(0, fifos.size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, (int)fifos.physbase);
  if ((int)fifobasevirt == 0 || (int)fifobasevirt == -1)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_Open: Error from mmap, errno = " << errno);
    return(RCC_ERROR_RETURN(0, SOLAR_MMAP)); 
  }
  
  // Define pointers to the relevant FIFO resources
  // This code has to be kept in sync with the respective section in the driver
  infifo  = (u_int *)fifobasevirt;
  infifon = (u_int *)(fifobasevirt + fifos.insize);
  DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Open: infifo   is at " << (uintptr_t)infifo);
  DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Open: infifon  is at " << (uintptr_t)infifon);

  is_open = 1;
    
  //Get information about the number of cards installed  
  ret = SOLAR_Info(&hwstat);  
  if (ret)
    return(RCC_ERROR_RETURN(0, ret)); 
   
  DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Open: hwstat.nacrds = " << hwstat.ncards);
  for(card = 0; card < MAXCARDS; card++)
  {    
    DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Open: hwstat.cards[" << card << "] = " << hwstat.cards[card]);
  }
  
  return(SOLAR_SUCCESS);
}


/*********************************/
SOLAR_ErrorCode_t SOLAR_Close(void)
/*********************************/
{
  int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, SOLAR_NOTOPEN));
  
  if (is_open > 1)
    is_open--;
  else
  {  
    DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Close: calling munmap");    
    ret = munmap((void *)fifobasevirt, fifos.size);
    if (ret)
    {
      DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_Close: Error from munmap, errno = " << errno);
      return(RCC_ERROR_RETURN(0, SOLAR_MUNMAP)); 
    }
  
    close(fd);
    is_open = 0;
  }
  
  return(SOLAR_SUCCESS);
}


/**********************************************/
SOLAR_ErrorCode_t SOLAR_Info(SOLAR_info_t *info)
/**********************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, SOLAR_NOTOPEN));
  
  ret = ioctl(fd, INFO, info);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_Info: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(SOLAR_SUCCESS);
}


/**************************************************/
SOLAR_ErrorCode_t SOLAR_Init(SOLAR_config_t *config)
/**************************************************/
{
  u_int ret, card;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, SOLAR_NOTOPEN));
  
  for(card = 0; card < MAXCARDS; card++)
  {    
    DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Init: config->enable[" << card << "] = " << config->enable[card]);
    DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_Init: hwstat.cards[" << card << "] = " << hwstat.cards[card]);

    if ((config->enable[card] == 1) && (hwstat.cards[card] == 0))
    {
      DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_Init: card " << card << " does not exist");
      return(RCC_ERROR_RETURN(0, SOLAR_NOHW));
    }
  }
    
  ret = ioctl(fd, CONFIG, config);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_Init: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(SOLAR_SUCCESS);
}


/***************************************/
SOLAR_ErrorCode_t SOLAR_Reset(u_int card)
/***************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, SOLAR_NOTOPEN));
  
  if (card > (hwstat.ncards - 1) || hwstat.cards[card] == 0)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_Reset: card " << card << " does not exist");
    return(RCC_ERROR_RETURN(0, SOLAR_NOHW));
  }
  
  ret = ioctl(fd, RESET, &card);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_Reset: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(SOLAR_SUCCESS);
}


/*******************************************/
SOLAR_ErrorCode_t SOLAR_LinkReset(u_int card)
/*******************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, SOLAR_NOTOPEN));
  
  if (card > (hwstat.ncards - 1) || hwstat.cards[card] == 0)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_LinkReset: card " << card << " does not exist");
    return(RCC_ERROR_RETURN(0, SOLAR_NOHW));
  }
  
  ret = ioctl(fd, LINKRESET, &card);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_LinkReset: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(SOLAR_SUCCESS);
}


/********************************************************************/
SOLAR_ErrorCode_t SOLAR_LinkStatus(u_int card, SOLAR_status_t *status)
/********************************************************************/
{
  u_int ret;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, SOLAR_NOTOPEN));
 
  status->card = card;
  if (status->card > (hwstat.ncards - 1) || hwstat.cards[status->card] == 0)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_LinkReset: card " << card << " does not exist");
    return(RCC_ERROR_RETURN(0, SOLAR_NOHW));
  }
  
  ret = ioctl(fd, LINKSTATUS, status);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_LinkReset: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  return(SOLAR_SUCCESS);
}



/*************************************************/
SOLAR_ErrorCode_t SOLAR_PagesIn(SOLAR_in_t *fifoin)
/*************************************************/
{
  u_int loop, ret, ok, data;
  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, SOLAR_NOTOPEN));

  if (fifoin->card > (hwstat.ncards - 1) || hwstat.cards[fifoin->card] == 0)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_PagesIn: card " << fifoin->card << " does not exist");
    return(RCC_ERROR_RETURN(0, SOLAR_NOHW));
  }
           
  ok = 1;               
  DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"SOLAR_PagesIn: filling IN FIFO");
  for (loop = 0; loop < fifoin->nvalid; loop++)
  {    
    //Just in case...
    fifoin->scw[loop] &= 0x1;
    fifoin->ecw[loop] &= 0x1;
    fifoin->size[loop] &= 0xfffff;

    data = (fifoin->scw[loop] << 31) + (fifoin->ecw[loop] << 30) + (fifoin->size[loop]);
    ret = in_fifo_push(fifoin->card, fifoin->pciaddr[loop], data);
    if (ret)
    {
      DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_PagesIn: error from in_fifo_push (FIFO full)");
      ok = 0;
      break;   //FIFO is full. Stop filling
    }
  }
  
  //Inform the driver about the new entries. 
  //This is needed to get the SOLAR driver going if the FIFOs are empty.
  ret = ioctl(fd, FIFOIN, &fifoin->card);
  if (ret)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_PagesIn: Error from ioctl, errno = " << errno);
    return(RCC_ERROR_RETURN(0, errno));
  }
  
  if (ok)
    return(SOLAR_SUCCESS);
  else
    return(RCC_ERROR_RETURN(0, SOLAR_FIFOFULL));
}


/******************************************************/
SOLAR_ErrorCode_t SOLAR_InFree(u_int card, u_int *nfree)
/******************************************************/
{  
  if (!is_open) 
    return(RCC_ERROR_RETURN(0, SOLAR_NOTOPEN));

  if (card > (hwstat.ncards - 1) || hwstat.cards[card] == 0)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"SOLAR_InFree: card " << card << " does not exist");
    return(RCC_ERROR_RETURN(0, SOLAR_NOHW));
  }
  
  *nfree = MAXINFIFO - infifon[card];   
  return(SOLAR_SUCCESS);
}


/*****************************************************************/
unsigned int SOLAR_err_get(err_pack err, err_str pid, err_str code)
/*****************************************************************/
{ 
  strcpy(pid, P_ID_SOLAR_STR);

  switch (RCC_ERROR_MINOR(err))
  {  
    case SOLAR_SUCCESS:       strcpy(code, SOLAR_SUCCESS_STR);     break;
    case SOLAR_FILE:          strcpy(code, SOLAR_FILE_STR);        break;
    case SOLAR_NOTOPEN:       strcpy(code, SOLAR_NOTOPEN_STR);     break;
    case SOLAR_EFAULT:        strcpy(code, SOLAR_EFAULT_STR);      break;
    case SOLAR_EIO:           strcpy(code, SOLAR_EIO_STR);         break;
    case SOLAR_ENOSYS:        strcpy(code, SOLAR_ENOSYS_STR);      break;
    case SOLAR_ILLREV:        strcpy(code, SOLAR_ILLREV_STR);      break;
    case SOLAR_IOREMAP:       strcpy(code, SOLAR_IOREMAP_STR);     break;
    case SOLAR_REQIRQ:        strcpy(code, SOLAR_REQIRQ_STR);      break;
    case SOLAR_KMALLOC:       strcpy(code, SOLAR_KMALLOC_STR);     break;
    case SOLAR_FIFOFULL:      strcpy(code, SOLAR_FIFOFULL_STR);    break;
    case SOLAR_MMAP:          strcpy(code, SOLAR_MMAP_STR);        break;
    case SOLAR_MUNMAP:        strcpy(code, SOLAR_MUNMAP_STR);      break;
    case SOLAR_NOHW:          strcpy(code, SOLAR_NOHW_STR);        break;
    case SOLAR_STUCK:         strcpy(code, SOLAR_STUCK_STR);       break;
    default:                  strcpy(code, SOLAR_NO_CODE_STR);     return(RCC_ERROR_RETURN(0, SOLAR_NO_CODE)); break;
  }
  return(RCC_ERROR_RETURN(0, SOLAR_SUCCESS));
}


/***************************************************/
int in_fifo_push(u_int card, u_int data, u_int data2)
/***************************************************/
{
  int index;
  
  if (infifon[card] == MAXINFIFO)
  {
    DEBUG_TEXT(DFDB_ROSSOLAR, 5 ,"in_fifo_push: The IN FIFO is full");
    return(-1);
  }

  index = IN_INDEX(card, infifow[card]);
  infifo[index] = data;
  infifo[index + 1] = data2;
  DEBUG_TEXT(DFDB_ROSSOLAR, 20 ,"in_fifo_push: index = " << index << "  data = " << data << " data2 " << data2);  
    
  infifow[card]++;
  if (infifow[card] == MAXINFIFO)
    infifow[card] = 0;
  
  infifon[card]++;
  return(0);
}







