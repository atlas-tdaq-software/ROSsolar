/****************************************************************/
/*                                                              */
/* File: dolar_slink_src.cpp                                    */
/*                                                              */
/* This program allows to access the resources of a DOLAR       */
/* card in a user friendly way and includes some test routines  */
/*                                                              */
/*  Author: Stefan Haas, CERN EP-ATE                            */
/*          based on solar_slink_src from Markus Joos           */
/*                                                              */
/*  18-Jun-04 STH created                                       */
/*  15-Nov-05 MJ  support for multiple DOLAR cards added        */
/*  10-Jun-13 MJ  support for 12 channel DOLAR added            */
/*                                                              */
/****************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "io_rcc/io_rcc.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include "ROSEventFragment/RODFragment.h"
#include "ROSEventFragment/ROBFragment.h"
#include "cmdl/cmdargs.h"
#include "tmgr/tmresult.h"
#include "ROSsolar/dolar.h"

using namespace ROS;
using namespace daq::tmgr;

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

// Constants
// Standard header and trailer sizes
#define HEADER_SIZE_WRDS     (sizeof(RODFragment::RODHeader) / 4)
#define TRAILER_SIZE_WRDS    (sizeof(RODFragment::RODTrailer) / 4)
#define STATUS_SIZE          1
#define MAX_BLOCKLETS        237     // Max. number of sub-fragments
#define MAX_LEN              8191    // Max. amount of data words per sub-fragment
#define MIN_PACKET_SIZE_WRDS (HEADER_SIZE_WRDS + TRAILER_SIZE_WRDS + 4)  // Header + trailer words + at least 4 words  
#define MAX_PACKET_SIZE_WRDS ((MAX_BLOCKLETS * MAX_LEN) - HEADER_SIZE_WRDS - TRAILER_SIZE_WRDS)  

#define MAX_CARDS            6       
#define MAX_ROLS             24  // Must be <= 32 (because of ChannelMask)


typedef struct stats_s 
{
  tstamp     ts;
  u_int64_t  words[MAX_ROLS];
  u_int64_t  frags[MAX_ROLS];
} stats_t;

// Prototypes
int dolar_map(void);
int dolar_set_type(void);
int dolar_unmap(void);
int cardreset(void);
int linkreset(void);
int slidas_start(int freq, int blocks);
int slidas_stop(void);
int update_stats(void);
int read_stats(stats_t *stats);
int zero_stats(stats_t *stats);
int display_stats(int mode);
int slidas_setup_rod_pattern(int card, int channel, int length, u_int lvl1_id);
int slidas_set_lvl1id(int card, int channel, u_int lvl1_id);

// Global variables
static int flg_verbose    = FALSE;
static u_int ecr_num      = 0;
static u_int module_id    = 0;
static u_int detector_id  = 0xa0;
static u_int l1_type      = 0x7;
static u_int det_type     = 0xa; 
static u_int size         = 256;
static u_int wait         = 0;
static u_int freq         = 0;
static u_int npackets     = 0;
static u_int run_time     = 0;
static u_int ChannelMask  = 0x1;    // Select the channels to be used
static u_int CardMask     = 0x1;    // Select the cards to be used
static u_int card_type    = 1;
static u_int max_channels = 4;
static u_int numb_flag    = 0;
static u_int freq_flag    = 0;
static u_int max_cards    = MAX_CARDS;
static u_int shandle[MAX_CARDS];
static u_long sreg[MAX_CARDS];
static stats_t start_stats, total_stats, ref_stats;
static T_dolar_regs *dolar[MAX_CARDS];


/******************************/
void sigquit_handler(int signum)
/******************************/
{
  signum = signum;  // remove compiler warning
  std::cout << "ctrl-\\ received" << std::endl;
  display_stats(0);
}


/*****************************/
void sigint_handler(int signum)
/*****************************/
{
  int code;
  
  std::cout << "Signal " << signum  << " received. Exiting...." << std::endl;
    
  slidas_stop();
  display_stats(1);

  // Reset the card and the links to make sure that no fragments are left in the buffers
  // Hoever this should not normally be required
  cardreset(); 
  if (linkreset()) 
    exit(TmUnresolved);
  std::cout << "Dolar has been reset" << std::endl;

  if ((code = dolar_unmap())) 
  {
    std::cout << "DOLAR_unmap() FAILED with error " << std::hex << code << std::endl;
    rcc_error_print(stdout, code);
    exit(TmUnresolved);
  }

  if ((code = ts_close(TS_DUMMY))) 
  {
    rcc_error_print(stdout, code);
    exit(TmUnresolved);
  }
  
  std::cout << "... Done." << std::endl;
  exit(TmPass);
}


/**********************/
int dolar_set_type(void)
/**********************/
{
  u_int tmp_handle, eret;
     
  if ((eret = IO_Open()) != IO_RCC_SUCCESS) 
  {
    rcc_error_print(stdout, eret);
    exit(TmFail);
  }
  
  eret = IO_PCIDeviceLink(DOLAR_VID, DOLAR_DID, 1, &tmp_handle);   //The first DOLAR/DOZOLAR found decides what type of card will be used. Mixing DOLAR and DOZOLAR cards in the same PC is not possible
  if (eret == IO_RCC_SUCCESS)
  {
    card_type    = 1; 
    max_cards    = 6;
    max_channels = 4;
  }
  else
  {
    eret = IO_PCIDeviceLink(DOLAR12_VID, DOLAR12_DID, 1, &tmp_handle); 
    if (eret == IO_RCC_SUCCESS)
    {
      card_type    = 2; 
      max_cards    = 2;
      max_channels = 12;
    }
    else 
      exit(TmFail);
  }

  if ((eret = IO_PCIDeviceUnlink(tmp_handle)) != IO_RCC_SUCCESS) 
    rcc_error_print(stdout, eret);
  
  return(0);
}


/*****************/
int dolar_map(void)
/*****************/
{
  u_int eret, pciaddr, offset, fw_ver, card, fw;
  
  for (card = 0; card < max_cards; card++)
  {
    if (CardMask & (1 << card))
    {
      if (flg_verbose) 
        std::cout << "Trying to map card " << card << "...." << std::endl;
			
      if (card_type == 1)	
        eret = IO_PCIDeviceLink(DOLAR_VID, DOLAR_DID, card + 1, &shandle[card]); 
      else	
        eret = IO_PCIDeviceLink(DOLAR12_VID, DOLAR12_DID, card + 1, &shandle[card]); 

      if (eret != IO_RCC_SUCCESS) 
      {
	rcc_error_print(stdout, eret);
	exit(TmFail);
      }  

      if ((eret = IO_PCIConfigReadUInt(shandle[card], 0x8, &fw_ver)) != IO_RCC_SUCCESS) 
      {
	rcc_error_print(stdout, eret);
	exit(TmFail);
      }
      
      fw_ver &= 0xff;
      
      if (flg_verbose) 
        std::cout << "Running on a DOLAR card with firmware revision " << fw_ver << std::endl;

      if (card_type == 1)
	fw = DOLAR_FIRMWARE_VERSION;
      else
	fw = DOLAR12_FIRMWARE_VERSION;
	
      if (fw_ver < fw) 
      {
	std::cout << "This version only runs on cards with firmware version " << fw << "!" << std::endl;
	exit(TmFail);
      }

      if ((eret = IO_PCIConfigReadUInt(shandle[card], 0x10, &pciaddr)) != IO_RCC_SUCCESS) 
      {
	rcc_error_print(stdout, eret);
	exit(TmFail);
      } 
 
      pciaddr &= 0xfffffff0;
      offset = pciaddr & 0xfff;
      pciaddr &= 0xfffff000;

      if ((eret = IO_PCIMemMap(pciaddr, 0x1000, &sreg[card])) != IO_RCC_SUCCESS) 
      {
	rcc_error_print(stdout, eret);
	exit(TmFail);
      } 

      dolar[card] = (T_dolar_regs *)(sreg[card] + offset); 
    }
  }
  return(0);
}


/*******************/
int dolar_unmap(void)
/*******************/
{
  u_int eret, card;

  for (card = 0; card < max_cards; card++)
  {
    if (CardMask & (1 << card))
    {
      if (flg_verbose) 
        std::cout << "Trying to un-map card " << card << "...." << std::endl;
      if ((eret = IO_PCIMemUnmap(sreg[card], 0x1000)))
	rcc_error_print(stdout, eret);
      if ((eret = IO_PCIDeviceUnlink(shandle[card])) != IO_RCC_SUCCESS) 
	rcc_error_print(stdout, eret);
    }
  }    

  if ((eret = IO_Close()))
    rcc_error_print(stdout, eret);

  return(0);
}


/*****************/
int cardreset(void)
/*****************/
{
  u_int card, data;

  for (card = 0; card < max_cards; card++)
  {
    if (CardMask & (1 << card))
    {
      if (flg_verbose) 
        std::cout << "Resetting DOLAR card ..." << std::endl;
      data = dolar[card]->opctrl1;  
      data |= 0x80000000;
      dolar[card]->opctrl1 = data;
      usleep(100000); // wait 100 ms
      
      if(card_type == 2)
      {
	data = dolar[card]->opctrl2;  
	data |= 0x80000000;
	dolar[card]->opctrl2 = data;
	usleep(100000); // wait 100 ms

	data = dolar[card]->opctrl3;  
	data |= 0x80000000;
	dolar[card]->opctrl3 = data;
	usleep(100000); // wait 100 ms
      }
    }
  }
  return(0);
}


/*****************/
int linkreset(void)
/*****************/
{
  u_int card, status, ureset_mask = 0, chan, i = 0;

  if (card_type == 1)
  {
    for (card = 0; card < max_cards; card++)
    {
      if (CardMask & (1 << card))
      {
	if (flg_verbose) 
          std::cout << "Resetting S-LINK channels on card " << card << std::endl;

	// set up the reset bit mask
	for (chan = 0; chan < max_channels; chan++) 
	{
	  if (ChannelMask & (1 << (chan + max_channels * card))) 
	    ureset_mask |= 1 << (8 * chan);   
	}

	// clear the URESET bits to make sure that there is a falling edge on URESET_N
	dolar[card]->opctrl1 &= ~ureset_mask;

	// set the URESET bits
	dolar[card]->opctrl1 |= ureset_mask;

	if (flg_verbose) 
          std::cout << "Waiting for links to come up ..." << std::endl;

	// now wait for LDOWN to come up again
	while (((status = dolar[card]->opstat1) & ureset_mask) && i++ < 100)
	  ts_delay(10000); // wait for 10 ms to give the link time to come up

	// clear the URESET bits
	dolar[card]->opctrl1 &= ~ureset_mask;
	if (status & ureset_mask) 
	{
	  std::cout << "ERROR: Link(s) did not come up after reset (card = " << card << " , OPSTAT = " << std::hex << status << std::dec << ")" << std::endl;
	  return(-1);
	}
      }
    } 
  }
  else
  {
    for (card = 0; card < max_cards; card++)
    {
      if (CardMask & (1 << card))
      {
	if (flg_verbose) 
          std::cout << "Resetting S-LINK channels on card " << card << std::endl;

	// Channels 1 - 4
	for (chan = 0; chan < 4; chan++) 
	{
	  if (ChannelMask & (1 << (chan + max_channels * card))) 
	    ureset_mask |= 1 << (8 * chan);   
	}
	dolar[card]->opctrl1 &= ~ureset_mask;
	dolar[card]->opctrl1 |= ureset_mask;
	if (flg_verbose) 
          std::cout << "Waiting for links 1-4 to come up ..." << std::endl;
	while (((status = dolar[card]->opstat1) & ureset_mask) && i++ < 100)
	  ts_delay(10000); // wait for 10 ms to give the link time to come up
	dolar[card]->opctrl1 &= ~ureset_mask;
	if (status & ureset_mask) 
	{
	  std::cout << "ERROR: Link(s) 1-4 did not come up after reset (card = " << card << " , OPSTAT = " << std::hex << status << std::dec << ")" << std::endl;
	  return(-1);
	}
	
	// Channels 5 - 8
	ureset_mask = 0;
	for (chan = 0; chan < 4; chan++) 
	{
	  if (ChannelMask & (1 << (chan + 4 + max_channels * card))) 
	    ureset_mask |= 1 << (8 * chan);   
	}
	dolar[card]->opctrl2 &= ~ureset_mask;
	dolar[card]->opctrl2 |= ureset_mask;
	if (flg_verbose) 
          std::cout << "Waiting for links 5-8 to come up ..." << std::endl;
	while (((status = dolar[card]->opstat2) & ureset_mask) && i++ < 100)
	  ts_delay(10000); // wait for 10 ms to give the link time to come up
	dolar[card]->opctrl2 &= ~ureset_mask;
	if (status & ureset_mask) 
	{
	  std::cout << "ERROR: Link(s) 5-8 did not come up after reset (card = " << card << " , OPSTAT = " << std::hex << status << std::dec << ")" << std::endl;
	  return(-1);
	}
	
	// Channels 9 - 12
	ureset_mask = 0;
	for (chan = 0; chan < 4; chan++) 
	{
	  if (ChannelMask & (1 << (chan + 8 + max_channels * card))) 
	    ureset_mask |= 1 << (8 * chan);   
	}
	dolar[card]->opctrl3 &= ~ureset_mask;
	dolar[card]->opctrl3 |= ureset_mask;
	if (flg_verbose) 
          std::cout << "Waiting for links 9-12 to come up ..." << std::endl;
	while (((status = dolar[card]->opstat3) & ureset_mask) && i++ < 100)
	  ts_delay(10000); // wait for 10 ms to give the link time to come up
	dolar[card]->opctrl3 &= ~ureset_mask;
	if (status & ureset_mask) 
	{
	  std::cout << "ERROR: Link(s) 9-12 did not come up after reset (card = " << card << " , OPSTAT = " << std::hex << status << std::dec << ")" << std::endl;
	  return(-1);
	}
      }
    } 
  }    
       
  return(0);
}


/****************************************************************************/
int slidas_setup_rod_pattern(int card, int channel, int length, u_int lvl1_id)
/****************************************************************************/
{
  int n_inst, rem_len; 

  if (length > MAX_BLOCKLETS * MAX_LEN) 
  {
    std::cout << "Cannot send more than " << MAX_BLOCKLETS * MAX_LEN << " words." << std::endl;
    exit(TmUnresolved);
  } 

  u_int source_id = (module_id & 0xffff) | ((detector_id & 0xff) << 16);

  dolar[card]->slidas[channel].ctrl = RESET;                    // reset channel
  dolar[card]->slidas[channel].ctrl = PROG_MODE;	        // programmation mode assert
  while ((dolar[card]->slidas[channel].sts & 0x1) == 0)         // waiting for the slidas
    usleep(1000);
  dolar[card]->slidas[channel].mem = 0;                         // start writing at address 0

  // program the translation table
  dolar[card]->slidas[channel].trans = 0xb0f00000;              // index 0: start-of-fragment
  dolar[card]->slidas[channel].trans = 0xee1234ee;              // index 1: ROD header marker
  dolar[card]->slidas[channel].trans = 0x00000009;              // index 2: header length
  dolar[card]->slidas[channel].trans = 0x03010000;              // index 3: format version number
  dolar[card]->slidas[channel].trans = 0x00000000;              // index 4: 0
  dolar[card]->slidas[channel].trans = 0x00000001;              // index 5: 1
  dolar[card]->slidas[channel].trans = length-13;               // index 6: data size
  dolar[card]->slidas[channel].trans = 0xe0f00000;              // index 7: end-of-fragment
  dolar[card]->slidas[channel].trans = 0x00000003;              // index 8: 3
  dolar[card]->slidas[channel].trans = source_id;               // index 9: source ID
  dolar[card]->slidas[channel].trans = l1_type;                 // index 10: trigger type, default = 7 
  dolar[card]->slidas[channel].trans = det_type;                // index 11: detector event type
  dolar[card]->slidas[channel].trans = lvl1_id - 1;             // index 12: level 1 ID
  dolar[card]->slidas[channel].trans = 3 * lvl1_id - 3;         // index 13: BCID = 3 * L1ID

  // program the instruction memory
  dolar[card]->slidas[channel].prog = LOAD_INIT(1, 12);         // 0:  load counter 1 with starting L1ID  
  dolar[card]->slidas[channel].prog = LOAD_INIT(2, 13);         // 1:  load counter 2 with starting BCID  
  dolar[card]->slidas[channel].prog = CONTROL(0, 1);            // 2:  start-of-fragment
  dolar[card]->slidas[channel].prog = CONST(1, 1);              // 3:  ROD header marker
  dolar[card]->slidas[channel].prog = CONST(2, 1);              // 4:  header length
  dolar[card]->slidas[channel].prog = CONST(3, 1);              // 5:  format version number
  dolar[card]->slidas[channel].prog = CONST(9, 1);              // 6:  source identifier
  dolar[card]->slidas[channel].prog = CONST(5, 1);              // 7:  run number = 1
  dolar[card]->slidas[channel].prog = COUNTER(1, 5, 1);         // 8:  level 1 ID = counter 1, increment 1
  dolar[card]->slidas[channel].prog = COUNTER(2, 8, 1);         // 9:  bunch crossing ID = counter 2, increment 3
  dolar[card]->slidas[channel].prog = CONST(10, 1);             // 10: trigger type
  dolar[card]->slidas[channel].prog = CONST(11, 1);             // 11: detector event type
  dolar[card]->slidas[channel].prog = LOAD(4, 4);               // 12:  data = counter 4, load with 0

  n_inst = 19; 
  rem_len = length - 14; 
  while (rem_len > MAX_LEN) 
  {
   dolar[card]->slidas[channel].prog = COUNTER(4, 5, MAX_LEN);
   rem_len -= MAX_LEN;
   n_inst++;
  }
  dolar[card]->slidas[channel].prog = COUNTER(4, 5, rem_len);

  dolar[card]->slidas[channel].prog = CONST(4, 1);              // 14: status element = 0
  dolar[card]->slidas[channel].prog = CONST(5, 1);              // 15: number of status elements = 1
  dolar[card]->slidas[channel].prog = CONST(6, 1);              // 16: number of data elements = length - 13
  dolar[card]->slidas[channel].prog = CONST(5, 1);              // 17: status block position = 1 (after data)
  dolar[card]->slidas[channel].prog = CONTROL(7, 1);            // 18: end-of-fragment
  dolar[card]->slidas[channel].prog = END;                      // 19: end of fragment instruction
  dolar[card]->slidas[channel].mem |= MEM_REGISTER(n_inst, 2);
  dolar[card]->slidas[channel].ctrl = RESET;                    // clear program mode and reset
  return(0);
}


/*********************************************************/
int slidas_set_lvl1id(int card, int channel, u_int lvl1_id)
/*********************************************************/
{
  dolar[card]->slidas[channel].ctrl = PROG_MODE;	  // programmation mode assert
  while ((dolar[card]->slidas[channel].sts & 0x1) == 0)   // waiting for the slidas
    usleep(1000);
  dolar[card]->slidas[channel].mem = 12 << 16;            // start writing in the translation table at address 12

  // program the translation table
  dolar[card]->slidas[channel].trans = lvl1_id - 1;       // index 12: level 1 ID
  dolar[card]->slidas[channel].mem = MEM_REGISTER(19, 2); // program memory contains 20 instructions, loop to instruction 2
  dolar[card]->slidas[channel].ctrl = RESET;              // clear program mode and reset
  return(0);
}


/************************************/
int slidas_start(int freq, int blocks)
/************************************/
{
  u_int card, chan;

  for (card = 0; card < max_cards; card++)
  {
    for (chan = 0; chan < max_channels; chan++) 
    {
      if (ChannelMask & (1 << (chan + max_channels * card))) 
      {
	usleep(1000);
	if (blocks > 0) 
	{ // frame-by-frame mode
	  dolar[card]->slidas[chan].freq = blocks;
	  dolar[card]->slidas[chan].ctrl = FRAME_MODE; // | RESET;
	  if (flg_verbose) 
	    std::cout << "Channel " << chan + 1 << " sending " << dolar[card]->slidas[chan].freq << " blocks" << std::endl;
	  dolar[card]->slidas[chan].ctrl = STEP_FORWARD | FRAME_MODE; // one frame forward
	} 
	else 
	{
	  if (freq > 0)
	    dolar[card]->slidas[chan].freq = (66667 / freq) - 1;
	  else 
	    dolar[card]->slidas[chan].freq = 29;
	  if (flg_verbose) 
	    std::cout << "Channel " << chan + 1 << " started, maximum fragment rate is " << 66666.667 / dolar[card]->slidas[chan].freq << " kHz" << std::endl;
	  dolar[card]->slidas[chan].ctrl = RUN_MODE; // switch to run mode
	}
      }
    }
  }
  return(0);
}


/*******************/
int slidas_stop(void)
/*******************/
{
  u_int card, chan;
  
  for (card = 0; card < max_cards; card++)
  {
    for (chan = 0; chan < max_channels; chan++) 
    {
      if (ChannelMask & (1 << (chan + max_channels * card))) 
      {
	dolar[card]->slidas[chan].ctrl = STOP_MODE; // switch to stop mode
	if (flg_verbose) 
	  std::cout << "Channel " << chan + 1 << " stopped" << std::endl;
      }
    }
  }
  return(0);
}


/****************************/
int read_stats(stats_t *stats)
/****************************/
{
  u_int chan, card, code; 

  if ((code = ts_clock(&(stats->ts)))) 
  {
    rcc_error_print(stdout, code);
    exit(TmUnresolved);
  }

  for (card = 0; card < max_cards; card++)
  {
    for (chan = 0; chan < max_channels; chan++) 
    {
      if (ChannelMask & (1 << (chan + max_channels * card))) 
      {
	stats->words[chan + 4 * card] = dolar[card]->slidas[chan].wrdcnt;
	stats->frags[chan + 4 * card] = dolar[card]->slidas[chan].blkcnt & BLK_CNT_MASK;
      }
    }
  }
  return(0);
}


/****************************/
int zero_stats(stats_t *stats)
/****************************/
{
  u_int chan, card, code; 

  if ((code = ts_clock(&(stats->ts)))) 
  {
    rcc_error_print(stdout, code);
    exit(TmUnresolved);
  }

  for (card = 0; card < max_cards; card++)
  {
    for (chan = 0; chan < max_channels; chan++) 
    {
      if (ChannelMask & (1 << (chan + max_channels * card))) 
      {
	stats->words[chan + 4 * card] = 0;
	stats->frags[chan + 4 * card] = 0;
      }
    }
  }
  return(0);
}


/********************/
int update_stats(void)
/********************/
{
  u_int rol;
  stats_t curr_stats;

  read_stats(&curr_stats);
  total_stats.ts = curr_stats.ts;
  
  for (rol = 0; rol < MAX_ROLS; rol++) 
  {
    if (ChannelMask & (1 << rol)) 
    {
      total_stats.words[rol] += curr_stats.words[rol] - start_stats.words[rol];
      total_stats.frags[rol] += curr_stats.frags[rol] - start_stats.frags[rol];
      if (curr_stats.words[rol] < start_stats.words[rol]) 
      {
	total_stats.words[rol] += 0x80000000;
	total_stats.words[rol] += 0x80000000;
      }
      if (curr_stats.frags[rol] < start_stats.frags[rol]) 
	total_stats.frags[rol] += 0x1000000;
    }
  }

  start_stats = curr_stats;
  return(0);
}


/*************************/
int display_stats(int mode)
/*************************/
{
  u_int rol;
  double word_rate, frag_rate, elapsed;
  u_int64_t numpackets;

  update_stats();
  elapsed = ts_duration(ref_stats.ts, total_stats.ts);
  std::cout << "Elapsed time: " << elapsed << " seconds"  << std::endl;

  for (rol = 0; rol < MAX_ROLS; rol++)
  {
    if (ChannelMask & (1 << rol)) 
    {
      word_rate = 1E-6 * (total_stats.words[rol] - ref_stats.words[rol]) / elapsed;
      frag_rate = 1E-3 * (total_stats.frags[rol] - ref_stats.frags[rol]) / elapsed;
      numpackets = total_stats.frags[rol] - ref_stats.frags[rol];
      std::cout << "ROL " << rol + 1 << " packets sent = " << numpackets << ", data rate = " << word_rate << " Mword/s, fragment rate = " << frag_rate << " kHz" << std::endl;
    }
  }  
  ref_stats = total_stats;
  
  if (mode == 1)
  {
    std::cout << "Totals:" << std::endl;
    for (rol = 0; rol < MAX_ROLS; rol++)
      if (ChannelMask & (1 << rol)) 
	std::cout << "ROL " << rol + 1 << " packets sent = " << total_stats.frags[rol] << std::endl;
  }
  
  return(0);
}


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-c x: Channel list (see below)                         ->Default: channel 1" << std::endl;
  std::cout << "-s x: Packet size (in words)                           ->Default: " << size << std::endl;
  std::cout << "-f x: First L1id for ROD emulation                     ->Default: 0" << std::endl;
  std::cout << "-w x: Wait before sending (in seconds)                 ->Default: " << wait << std::endl;
  std::cout << "-n x: Number of fragments (not compatible with -F)     ->Default: infinite loop" << std::endl;
  std::cout << "-e x: Number of fragments between ECRs, use with -n    ->Default: 0 (no ECR)" << std::endl;
  std::cout << "-F x: Max. fragment generation frequency (kHz)         ->Default: 0 (run as fast as possible)" << std::endl;
  std::cout << "-t x: Time to run for when using -F option (sec)       ->Default: 0x" << std::hex << run_time << std::dec << std::endl;
  std::cout << "-O x: Module id (Hex format)                           ->Default: 0x" << std::hex << module_id << std::dec << std::endl;
  std::cout << "-D x: Detector id (Hex format)                         ->Default: 0x" << std::hex << detector_id << std::dec << std::endl;
  std::cout << "-L x: L1 Trigger type (Hex format)                     ->Default: 0x" << std::hex << l1_type << std::dec << std::endl;
  std::cout << "-T x: Detector event type (Hex format)                 ->Default: 0x" << std::hex << det_type << std::dec << std::endl;
  std::cout << "-C  : Send another block of fragments without reinitializing the card" << std::endl;
  std::cout << "-R  : Do not send a card & link reset                  ->Default: FALSE" << std::endl;
  std::cout << "-r  : Reset the card and the selected S-LINK channels and exit" << std::endl;
  std::cout << "-v  : Verbose output" << std::endl;
  std::cout << std::endl;
  std::cout << "Definition of the channel list parameter:" << std::endl;
  std::cout << "The selection if the active channels is made via a string consisting of '1' and '0' characters" << std::endl;
  std::cout << "The position of a character defines the number of the channel" << std::endl;
  std::cout << "How the channels map onto the card depends on the type of DOLAR" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "For 4-channel DOLAR cards (-N 4)" << std::endl;
  std::cout << "The first DOLAR has channels 1 to 4, the second one channels 5 to 8 and so on" << std::endl;
  std::cout << "Leading '0' characters can be ommitted" << std::endl;
  std::cout << "Examples:" << std::endl;
  std::cout << "Enable channels 1 and 3 of the first DOLAR:  -c 101" << std::endl;
  std::cout << "Enable channels 1 and 3 of the second DOLAR: -c 1010000" << std::endl;
  std::cout << "Enable channel 2 of the first three DOLARs:  -c 1000100010" << std::endl;
  std::cout << "" << std::endl;
  std::cout << "For 12-channel DOLAR cards (-N 12)" << std::endl;
  std::cout << "The first DOLAR has channels 1 to 12, the second one channels 13 to 24" << std::endl;
  std::cout << "Leading '0' characters can be ommitted" << std::endl;
  std::cout << "Examples:" << std::endl;
  std::cout << "Enable channels 1 and 3 of the first DOLAR:  -c 101" << std::endl;
  std::cout << "Enable channels 1 and 3 of the second DOLAR: -c 101000000000000" << std::endl;
  std::cout << "Enable channel 2 of the first and second DOLAR:  -c 10000000000010" << std::endl;
  std::cout << std::endl;
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  char c;
  u_int rol, card, loop, chan, code, sending;
  int flg_continue = FALSE, flg_size = FALSE, flg_resetonly = FALSE, flg_noreset = FALSE, flg_lvl1id = FALSE;
  u_int i, lvl1_id = 0, tot_frag = 0, rem_frag, send_frags;
  struct sigaction saint, sa;

  while ((c = getopt(argc, argv, "hc:CO:D:L:T:f:F:rs:t:w:n:vRe:")) != -1)
    switch (c) 
    {
      case 'h':
	std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
	usage();
	exit(TmUnresolved);
	break;

      case 'c':
	ChannelMask = 0;
	for (i = 0; i < strlen(optarg); i++) 
	  if (optarg[strlen(optarg) - 1 - i] == '1')
          {
	    if (flg_verbose) 
	      std::cout << "Channel " << i + 1 << " selected" << std::endl;
	    ChannelMask |= (1 << i);
	  }
	break;

      case 's':
	flg_size = TRUE;
	size = atoi(optarg);
	if (size < MIN_PACKET_SIZE_WRDS || size > MAX_PACKET_SIZE_WRDS) 
	{
	  std::cout << "Wrong packet size: " << size << std::endl << "(acceptable range: " << MIN_PACKET_SIZE_WRDS << " - " << MAX_PACKET_SIZE_WRDS << " words)" << std::endl;
	  exit(TmUnresolved);
	}
	break;

      case 'f': 
	flg_lvl1id = TRUE;
        sscanf(optarg, "%u", &lvl1_id);
	break;

      case 'F': 
	freq = atoi(optarg);
	if (freq <= 0) 
	{
	  std::cout << "fragment rate must be > 0" << std::endl;
	  exit(TmUnresolved);
	} 
	freq_flag = 1;
	break;

      case 't': 
	run_time = atoi(optarg);
	if (run_time <= 0) 
	{
	  std::cout << "time must be > 0" << std::endl;
	  exit(TmUnresolved);
	} 
	break;
	
      case 'n':
	tot_frag = atoi(optarg);
	numb_flag = 1;
	break;

      case 'C':
	if (flg_verbose) 
	  std::cout << "Continuation run, the card will not be reintialized, L1ID will be continous" << std::endl;
	flg_continue = 1;
	break;
      
      case 'O': sscanf(optarg,"%x", &module_id);   break;
      case 'D': sscanf(optarg,"%x", &detector_id); break;
      case 'L': sscanf(optarg,"%x", &l1_type);     break;
      case 'T': sscanf(optarg,"%x", &det_type);    break;
      case 'e': sscanf(optarg,"%u", &ecr_num);     break;
      case 'w': wait = atoi(optarg);               break;
      case 'v': flg_verbose = TRUE;                break;
      case 'r': flg_resetonly = TRUE;              break;
      case 'R': flg_noreset = TRUE;                break;
      
      default:
	std::cout << "Invalid option " << c << std::endl;
	std::cout << "Usage: " << argv[0] << "[options]" << std::endl;
	usage();
	exit (TmUnresolved);
    }

  // Check compatibility of options
  if (numb_flag && freq_flag) 
  { 
    std::cout << "Options -F and -n are not compatible" << std::endl;
    exit (TmUnresolved);
  }

  if (flg_continue) 
  {
    if (flg_size) 
    {
      std::cout << "Options -C and -s are not compatible" << std::endl;
      exit(TmUnresolved);
    }
    if (flg_lvl1id) 
    {
      std::cout << "Options -C and -f are not compatible" << std::endl;
      exit(TmUnresolved);
    }
    if (tot_frag < 1) 
    {
      std::cout << "Option -C can only be used with -n" << std::endl;
      exit(TmUnresolved);
    }
  }

  if (ecr_num > 0 && freq > 0) 
  {
    std::cout << "Options -e and -F are not compatible" << std::endl;
    exit (TmUnresolved);
  }

  if (ecr_num > 0 && ecr_num >= tot_frag) 
  {
    std::cout << "Total number of fragments (" << tot_frag << ") has to be larger than the number of fragments between ECRs (" << ecr_num<< ")" << std::endl;
    exit (TmUnresolved);
  }

  if (flg_verbose && (ecr_num > 0)) 
    std::cout << "ECR mode selected: sending " << tot_frag << " fragments, with an ECR every " << ecr_num << " fragments" << std::endl;

  if(!ChannelMask)
  {
    std::cout << "Seems you have not selected any channels" << std::endl;
    exit (TmUnresolved);
  }
  
  code = dolar_set_type();
  
  //build the card mask
  CardMask = 0;
  for (loop = 0; loop < max_cards; loop++)
  {
    if (card_type == 1)
    {
      if (ChannelMask & (0xf << loop * max_channels))
        CardMask |= (1 << loop);
    }
    else
    {
      if (ChannelMask & (0xfff << loop * max_channels))
        CardMask |= (1 << loop);
    }
  }
  if (flg_verbose) 
  {
    std::cout << "ChannelMask = 0x" << std::hex << ChannelMask << std::dec << std::endl;
    std::cout << "CardMask    = 0x" << std::hex << CardMask << std::dec << std::endl;
  }
  
  // wait some time to be sure that slink_dst has started
  sleep(wait);

  code = dolar_map();
  
  // Check if the settings are compatible with a continuation run
  if (flg_continue) 
  {
    for (card = 0; card < max_cards; card++)
    {
      for (chan = 0; chan < max_channels; chan++) 
      {
	if (ChannelMask & (1 << (chan + max_channels * card))) 
	{
	  if ((dolar[card]->slidas[chan].ctrl & FRAME_MODE) != FRAME_MODE) 
	  {
 	    std::cout << "Option -C option can only be used if the previous run was made with option -n and if all the packets were sent." << std::endl;
 	    exit(TmUnresolved);
          }
	}
      }
    }
  }

  // Install signal handler for SIGQUIT
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit (TmUnresolved);
  }

  // Install signal handler for SIGINT
  sigemptyset(&saint.sa_mask);
  saint.sa_flags   = 0;
  saint.sa_handler = sigint_handler;

  if (sigaction(SIGINT, &saint, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TmUnresolved);
  }

  if (sigaction(SIGTERM, &saint, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TmUnresolved);
  }

  if (!flg_continue && !flg_noreset) 
  {
    cardreset();
    if (linkreset()) exit(TmFail);
    if (flg_resetonly) exit(TmPass);
  }
  
  std::cout << std::endl <<"Press ctrl-\\ to output statistics" << std::endl;
  std::cout << "Press ctrl-c to quit" << std::endl << std::endl;

  if ((code = ts_open(1, TS_DUMMY))) 
  {
    rcc_error_print(stdout, code);
    exit(TmUnresolved);
  }

  if (!flg_continue) 
  {  // configure data generators
    for (card = 0; card < max_cards; card++)
    {
      for (chan = 0; chan < max_channels; chan++) 
      {
	if (ChannelMask & (1 << (chan + max_channels * card))) 
	  slidas_setup_rod_pattern(card, chan, size, lvl1_id);
      }
    }
  }

  // read the start values
  read_stats(&start_stats);
  update_stats();
  zero_stats(&ref_stats);
  if (freq_flag) 
  { 
    struct timespec ts_req, ts_rem;
    slidas_start(freq, 0); // start all selected channels
    
    if (run_time)
    {
      ts_req.tv_sec = run_time;
      ts_req.tv_nsec = 0;
      while (nanosleep(&ts_req, &ts_rem)) 
	ts_req = ts_rem;
    }
    else
      while(1);      
    sigint_handler(0);
    exit(TmUnresolved);
  }

  rem_frag = tot_frag;
  npackets = tot_frag;
  if (ecr_num > 0) 
    npackets = ecr_num;
  send_frags = 0;

  while (true) 
  {
    u_int remaining = 0;
    u_long sleep_time;
    slidas_start(0, npackets); // start all selected channels

    // check if all the packets have been sent
    sending = 1;
    send_frags += npackets;
    while (sending || npackets == 0) 
    {
      update_stats();
      sending = 0;
      for (rol = 0; rol < MAX_ROLS; rol++)
      {
  	if (ChannelMask & (1 << rol)) 
	{
	  remaining = send_frags - total_stats.frags[rol];
          if (flg_verbose) 
            std::cout << "rol = " << rol << ", remaining = " << remaining << ", send_frags = " << send_frags << ", total_stats.frags = " << total_stats.frags[rol] << std::endl;
	  if (remaining > 0 || !tot_frag) 
	    sending = 1;
	}
      }
      
      if (sending == 0) 
      {
        if (flg_verbose) 
          std::cout << "sending == 0. Exiting" << std::endl;
        break;
      }
      
      sleep_time = (remaining / 40000) * (size + 2);          // time to send the remaining fragments at full rate in ms
      sleep_time = (sleep_time > 10000) ? 10000 : sleep_time; // read statistics at least every 10 seconds
      if (sleep_time == 0) 
        sleep_time++;
      struct timespec ts_sleep;
      ts_sleep.tv_sec = sleep_time / 1000;
      ts_sleep.tv_nsec = 1000000 * (sleep_time % 1000);
      nanosleep(&ts_sleep, NULL);                             // sleep to reduce the CPU load
    }
    
    if (ecr_num == 0) 
    {
      if (flg_verbose) 
      std::cout << "ecr_num == 0. Exiting" << std::endl;
      break; // finish when not in ECR mode
    }
    
    rem_frag -= npackets;
    if (rem_frag == 0) 
    {
      if (flg_verbose) 
      std::cout << "rem_frag == 0. Exiting" << std::endl;
      break; // finish when not in ECR mode
    }

    npackets = (rem_frag > ecr_num) ? ecr_num : rem_frag;
    lvl1_id &= 0xFF000000;
    lvl1_id += 0x01000000;

    for (card = 0; card < max_cards; card++)
    {
      for (chan = 0; chan < max_channels; chan++) 
      {
        if (ChannelMask & (1 << (chan + max_channels * card))) 
	  slidas_set_lvl1id(card, chan, lvl1_id);
      }
    }
    
    read_stats(&start_stats);
  }

  display_stats(1);

  if (flg_verbose) 
    std::cout << "All done. Exiting" << std::endl;

  if ((code = dolar_unmap())) 
  {
    std::cout << "main: DOLAR_unmap() FAILED with error " << std::hex << code << std::endl;
    rcc_error_print(stdout, code);
    exit(TmFail);
  }

  if ((code = ts_close(TS_DUMMY))) 
  {
    rcc_error_print(stdout, code);
    exit(TmUnresolved);
  }

  exit(TmPass);
}
