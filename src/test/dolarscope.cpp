/****************************************************************/
/*                                                              */
/* File: dolarscope.cpp                                         */
/*                                                              */
/* This program allows to access the resources of a DOLAR       */
/* card in a user friendly way and includes some test routines  */
/*                                                              */
/*  Author: Stefan Haas, CERN EP-ATE                            */
/*          based on solarscope from Markus Joos                */
/*                                                              */
/*  11-Jul-03 STH created                                       */
/*                                                              */
/****************************************************************/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include "ROSsolar/dolar.h"
#include "rcc_error/rcc_error.h"
#include "io_rcc/io_rcc.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"


/*Macros*/
#ifdef DEBUG
  #define debug(x) printf x
#else
  #define debug(x)
#endif

#define CHANNEL_OK(chan)\
{\
  if (card_type == 1 && (channel < 0 || channel > 3)) \
  {\
    printf("ERROR: This Dolar only supports channels from 0 to 3\n");\
    return(-1);\
  }\
  if (card_type == 2 && (channel < 0 || channel > 11))\
  {\
    printf("ERROR: This Dolar only supports channels from 0 to 11\n");\
    return(-1);\
  }\
}\


/*prototypes*/
int dolar_map(int occ);
int dolar_unmap(void);
int set_opctrl(void);
int set_pciconf(void);
int set_slidasctrl(int channel);
int set_slidasfreq(int channel);
int set_slidasmem(int channel);
int set_slidasprog(int channel);
int set_slidastrans(int channel);
int cardreset(void);
int linkreset(void);
int mainhelp(void);
int display_slidas_config(int data);
int display_slidas_status(int data);
int display_slidas_mem(int data);
int setlat();
int dumpconf(void);
int setreg(void);
int slidas_config(void);
int slidas_runstop(void);
int calc_rate(void);
int fill_memories(int channel, char* prog_file_name);
int trans_table_contains(u_int trans_table[],int nb_elem, u_int value);
int get_trans_table_index(u_int table[], int* entries, u_int value);
int setup_rod_pattern(int channel, int length, u_int lvl1_id);
int setup_alt_pattern(int channel, int length, u_int data);
int setup_rnd_pattern(int channel, int length);
int setup_walking_pattern(int channel, int length, int mode);
int select_channels(void);
int stability_test(void);
void SigQuitHandler(int /*signum*/);


/*globals*/
static u_int cont, shandle, offset, card_type, max_channels;
static u_long sreg;
static u_int pcidefault[16] = 
{
  0x001810dc, 0x00800000, 0x02800001, 0x0000ff00, 0xfffffc00, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x000001ff
};
static int ChannelMask;          // Select the channels to be used
static T_dolar_regs *dolar;


/*********************************/
void SigQuitHandler(int /*signum*/)
/*********************************/
{
  cont=0;
}


/********************/
int dolar_map(int occ)
/********************/
{
  u_int eret, pciaddr;

  eret = IO_Open();
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }

  card_type = 0;

  eret = IO_PCIDeviceLink(0x10dc, 0x0018, occ, &shandle);
  if (eret == IO_RCC_SUCCESS)
  {
    card_type = 1; // We have found a 4-channel DOLAR
    ChannelMask = 0xf;
    max_channels = 4;
  }
  else
  {
    eret = IO_PCIDeviceLink(DOLAR12_VID, DOLAR12_DID, occ, &shandle);      
    if (eret == IO_RCC_SUCCESS)
    {
      card_type = 2; // We have found a 12-channel DOLAR
      ChannelMask = 0xfff;
      max_channels = 12;
    }
    else
    {
      printf("Failed to fined a DOLAR\n");
      rcc_error_print(stdout, eret);
      exit(-1);
    }
  } 

  eret = IO_PCIConfigReadUInt(shandle, 0x10, &pciaddr);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 

  pciaddr &= 0xfffffff0;  //Remove status bits
  printf("pciaddr = 0x%08x\n", pciaddr);

  offset = pciaddr & 0xfff;
  pciaddr &= 0xfffff000;
  printf("pciaddr = 0x%08x\n", pciaddr);
  printf("offset = 0x%08x\n", offset);
  eret = IO_PCIMemMap(pciaddr, 0x1000, &sreg);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 

  dolar = (T_dolar_regs *)(sreg + offset); 
  return(0);
}


/*******************/
int dolar_unmap(void)
/*******************/
{
  u_int eret;

  eret = IO_PCIMemUnmap(sreg, 0x1000);
  if (eret)
    rcc_error_print(stdout, eret);

  eret = IO_Close();
  if (eret)
    rcc_error_print(stdout, eret);
  return(0);
}


/**********/
int setlat()
/**********/
{
  u_int eret, latency, data;

  eret = IO_PCIConfigReadUInt(shandle, 0xC, &data);
  if (eret)
  {
    printf(" failed to read register\n");
    return(-1);
  }
  latency = (data >> 8) & 0xff;
  printf("Enter new value: ");
  latency = gethexd(latency);

  data &= 0xffff00ff;
  data |= (latency << 8);
  
  eret = IO_PCIConfigWriteUInt(shandle, 0xC, data);
  if (eret)
  {
    printf(" failed to write register\n");
    return(-1);
  }
  return(0);
}


/****************/
int dumpconf(void)
/****************/
{
  u_int loop, eret, data;

  printf("PCI configuration registers:\n\n");
  printf("Offset  |  content     |  Power-up default\n");
  for(loop = 0; loop < 0x40; loop += 4)
  {
    printf("   0x%02x |", loop);
    eret = IO_PCIConfigReadUInt(shandle, loop, &data);      
    if (eret)
      printf(" failed to read register\n");
    else
    {
      printf("   0x%08x |", data);
      if (data == pcidefault[loop >> 2])
        printf(" Yes\n");
      else
        printf(" No (0x%08x)\n", pcidefault[loop >> 2]);
    }
  }
  return(0);
}


/*********************************/
int display_slidas_config(int data)
/*********************************/
{
  switch ((data >> 14) & 3) 
  {
  case 0:
    printf("Stop        ");
    break;
  case 1:
    printf("Run         ");
    break;
  case 2:
    printf("Single-block");
    break;
  case 3:
    printf("Single-step ");
    break;
  }
  printf("   ");
  return(0);
}


/*********************************/
int display_slidas_status(int data)
/*********************************/
{
  // prog_mod_ok
  printf("Programming mode:                         %s\n", (data & 1) ? "on" : "off");
  // running
  printf("Frames to be sent:                        %s\n", (data & 2) ? "Set" : "Not set");
  // overflow
  printf("Overflow:                                 %s\n", (data & 4) ? "Set" : "Not set");
  return(0);
}


/******************************/
int display_slidas_mem(int data)
/******************************/
{
  int value;
  value = ((data >> PROG_ADDR_MAX_OFFSET) & PROG_ADDR_MAX_FIELD);
  printf("Address of the last instruction : %u\n",value);
	
  return(0);
}


/**********************/
int stability_test(void)
/**********************/
{
  u_int autotest = 0, tmask = 7, chan, theend = 0, d1, d2, d3, d4, d5, d6, second = 0, minute = 0;
  
  printf("Enter test mask");
  tmask = getdecd(tmask);
  
  if (tmask == 8)
  {
    autotest = 1;
    tmask = 1;
    printf("test mask is 1\n");
  }
  
  if (card_type == 1)
    return(1);

  d1 = dolar->opctrl1;
  d2 = dolar->opctrl2;
  d3 = dolar->opctrl3;
  d4 = dolar->opstat1;
  d5 = dolar->opstat2;
  d6 = dolar->opstat3;

  cont = 1;
  printf("=====================================\n");
  printf("Test running. Press <ctrl+\\> to stop\n");
  printf("=====================================\n");
  while(cont)
  {
    if (tmask & 0x1)
    {
      if (dolar->opctrl1 != d1) theend = 1;
      if (dolar->opctrl2 != d2) theend = 2;
      if (dolar->opctrl3 != d3) theend = 3;
      if (dolar->opstat1 != d4) theend = 4;
      if (dolar->opstat2 != d5) theend = 5;
      if (dolar->opstat3 != d6) theend = 6;
    }
    if (tmask & 0x2)
      if (dolar->cardtemp == 0xffffffff) theend = 7;
    
    if (tmask & 0x4)
    {
      for (chan = 0; chan < max_channels; chan++)
      {
	if (dolar->slidas[chan].ctrl   != 0) theend = 8;
	if (dolar->slidas[chan].sts    != 0) theend = 9;
	if (dolar->slidas[chan].wrdcnt != 0) theend = 10;
	if (dolar->slidas[chan].blkcnt != 0) theend = 11;
	if (dolar->slidas[chan].freq   != 0) theend = 12;
	if (dolar->slidas[chan].mem    != 0) theend = 13;
	if (dolar->slidas[chan].prog   != 0) theend = 14;
	if (dolar->slidas[chan].trans  != 0) theend = 15;
      }
    }
    
    if (theend)
    {
      printf("Connection to register lost. Theend = %d\n", theend);
      fflush(stdout);
      return(2);
    }
        
    printf(".");
    second++;
    if (second == 60)
    {
      minute++;
      second = 0;
      printf("%d", minute);
    }

    if (minute == 60 && autotest)
    {
      minute = 0;
      tmask++;
      if (tmask == 8)
        tmask = 1;
      printf("test mask is %d\n", tmask);
    }
    
    fflush(stdout);
    sleep(1);
  }
  printf("Terminated by user\n");
  return(0);
}


/***************/
int dumpmem(void)
/***************/
{
  u_int data, chan;
  float ctemp;

  printf("\n==============================================================================\n");
  
  if (card_type == 2)
  {
    printf("Raw dump:\n");
  
    printf("Operation Control register 1 = 0x%08x\n", dolar->opctrl1);
    printf("Operation Control register 2 = 0x%08x\n", dolar->opctrl2);
    printf("Operation Control register 3 = 0x%08x\n", dolar->opctrl3);
    printf("Operation Status register 1  = 0x%08x\n", dolar->opstat1);
    printf("Operation Status register 2  = 0x%08x\n", dolar->opstat2);
    printf("Operation Status register 3  = 0x%08x\n", dolar->opstat3);
    printf("Card temperature  = 0x%08x\n", dolar->cardtemp);

    for (chan = 0; chan < max_channels; chan++)
    {
      printf("Channel |       ctrl |        sts |     wrdcnt |     blkcnt |       freq |        mem |       prog |      trans\n");
      printf("     %02d |", chan);
      printf(" 0x%08x |", dolar->slidas[chan].ctrl);
      printf(" 0x%08x |", dolar->slidas[chan].sts);
      printf(" 0x%08x |", dolar->slidas[chan].wrdcnt);
      printf(" 0x%08x |", dolar->slidas[chan].blkcnt);
      printf(" 0x%08x |", dolar->slidas[chan].freq);
      printf(" 0x%08x |", dolar->slidas[chan].mem);
      printf(" 0x%08x |", dolar->slidas[chan].prog);
      printf(" 0x%08x\n", dolar->slidas[chan].trans);
    }

    printf("Decoded  dump:\n");
  }
  
  data = dolar->opctrl1;   
  printf("\nOperation Control register (0x%08x)", data);
  printf("\n                   Channel 1      Channel 2      Channel 3      Channel 4");
  printf("\nLSC Link reset:    ");
  for (chan = 0; chan < 4; chan++) 
    printf("%-15s", (data & (1 << (8 * chan))) ? "on" : "off");
  printf("\nS-LINK test mode:  ");
  for (chan = 0; chan < 4; chan++) 
    printf("%-15s", (data & (2 << (8 * chan))) ? "on" : "off");
  printf("\n\nReset interface:   %s\n", (data & 0x80000000) ? "on" : "off");

  data = dolar->opstat1;  
  printf("\nOperation Status register (0x%08x)", data);
  printf("\n                   Channel 1      Channel 2      Channel 3      Channel 4");
  printf("\nLink Status:       ");
  for (chan = 0; chan < 4; chan++) 
    printf("%-15s", (data & (1 << (8 * chan))) ? "Link down" : "Link up");
  printf("\nFull Flag:         ");
  for (chan = 0; chan < 4; chan++) 
    printf("%-15s", (data & (2 << (8 * chan))) ? "Set" : "Not set");
  printf("\nReturn Lines:      ");
  for (chan = 0; chan < 4; chan++) 
    printf("%-15d", (data >> (3 + 8 * chan)) & 0xF);
  printf("\nF/O Transceiver:   ");
  for (chan = 0; chan < 4; chan++) 
    printf("%-15s", (data & (0x80 << (8 * chan))) ? "Not installed" : "Installed");


  if (card_type == 2)
  {
    data = dolar->opctrl2;   
    printf("\nOperation Control register (0x%08x)", data);
    printf("\n                   Channel 5      Channel 6      Channel 7      Channel 8");
    printf("\nLSC Link reset:    ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15s", (data & (1 << (8 * chan))) ? "on" : "off");
    printf("\nS-LINK test mode:  ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15s", (data & (2 << (8 * chan))) ? "on" : "off");
    printf("\n\nReset interface:   %s\n", (data & 0x80000000) ? "on" : "off");

    data = dolar->opstat2; 
    printf("\nOperation Status register (0x%08x)", data);
    printf("\n                   Channel 5      Channel 6      Channel 7      Channel 8");
    printf("\nLink Status:       ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15s", (data & (1 << (8 * chan))) ? "Link down" : "Link up");
    printf("\nFull Flag:         ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15s", (data & (2 << (8 * chan))) ? "Set" : "Not set");
    printf("\nReturn Lines:      ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15d", (data >> (3 + 8 * chan)) & 0xF);
    printf("\nF/O Transceiver:   ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15s", (data & (0x80 << (8 * chan))) ? "Not installed" : "Installed");

    data = dolar->opctrl3;   
    printf("\nOperation Control register (0x%08x)", data);
    printf("\n                   Channel 9     Channel 10     Channel 11     Channel 12");
    printf("\nLSC Link reset:    ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15s", (data & (1 << (8 * chan))) ? "on" : "off");
    printf("\nS-LINK test mode:  ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15s", (data & (2 << (8 * chan))) ? "on" : "off");
    printf("\n\nReset interface:   %s\n", (data & 0x80000000) ? "on" : "off");

    data = dolar->opstat3; 
    printf("\nOperation Status register (0x%08x)", data);
    printf("\n                   Channel 9     Channel 10     Channel 11     Channel 12");
    printf("\nLink Status:       ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15s", (data & (1 << (8 * chan))) ? "Link down" : "Link up");
    printf("\nFull Flag:         ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15s", (data & (2 << (8 * chan))) ? "Set" : "Not set");
    printf("\nReturn Lines:      ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15d", (data >> (3 + 8 * chan)) & 0xF);
    printf("\nF/O Transceiver:   ");
    for (chan = 0; chan < 4; chan++) 
      printf("%-15s", (data & (0x80 << (8 * chan))) ? "Not installed" : "Installed");
  }
  

  // Temperature Sensor
  ctemp = dolar->cardtemp * 0.49 - 271;
  printf("\n\nCard temperature:  %5.1f deg. C\n", ctemp);
  printf("\nTemperature raw data = 0x%08x\n", dolar->cardtemp);

  // Slidas 
  for (chan = 0; chan < max_channels; chan++)
  {
    printf("Channel %2d\n", chan);
    printf("==========\n");
    printf("Control register:  0x%08x\n", dolar->slidas[chan].ctrl & 0x1ffff);
    printf("Programming mode:  %s\n", (dolar->slidas[chan].ctrl & (1 << 13)) ? "on" : "off");
    printf("Operation mode:    ");
    display_slidas_config(dolar->slidas[chan].ctrl & 0x1ffff);
    printf("\n");
    printf("Status register:   0x%08x\n", dolar->slidas[chan].sts & 0x7);
    printf("Programming mode:  %s\n", (dolar->slidas[chan].sts & 1) ? "set" : "not set");
    printf("Frames to be sent: %s\n", (dolar->slidas[chan].sts & 2) ? "set" : "not set");
    printf("Overflow:          %s\n", (dolar->slidas[chan].sts & 4) ? "Set" : "Not set");
    printf("Frequency divider: %u\n", dolar->slidas[chan].freq);
    printf("Frequency [Hz]:    %u\n", 66666667 / (dolar->slidas[chan].freq + 1));
    printf("Memory register:   0x%08x\n", dolar->slidas[chan].mem & 0x3fffff);
    printf("Last instr. addr.: %u\n", (dolar->slidas[chan].mem >> PROG_ADDR_MAX_OFFSET) & PROG_ADDR_MAX_FIELD);
    printf("Word counter:      %u\n", dolar->slidas[chan].wrdcnt);
    printf("Block counter:     %u\n", dolar->slidas[chan].blkcnt & 0xFFFFFF);
  }
  printf("==============================================================================\n");
  return(0);
}


/**************/
int setreg(void)
/**************/
{
  static int fun = 1, reg = 1;
  int chan;

  printf("\n=============================================================\n");
  do 
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 SLIDAS  1 registers           2 SLIDAS  2 registers\n");
    printf("   3 SLIDAS  3 registers           4 SLIDAS  4 registers\n");
    printf("   5 SLIDAS  5 registers           6 SLIDAS  6 registers\n");
    printf("   7 SLIDAS  7 registers           8 SLIDAS  8 registers\n");
    printf("   9 SLIDAS  9 registers          10 SLIDAS 10 registers\n");
    printf("  11 SLIDAS 11 registers          12 SLIDAS 12 registers\n");
    printf("  13 Operation Control register   14 PCI configuration register\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    chan = fun - 1; // which channel to use
    
    switch (fun) 
    {
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12:
      printf("\nSelect a register:\n");
      printf("   1 Control Register             2 Frequency register\n");
      printf("   3 Memory Register              4 Programmation memory\n");
      printf("   5 Translation memory\n");
      printf("Your choice ");
      reg = getdecd(reg);
      switch (reg) 
      {
      case 1:
	set_slidasctrl(chan);
	break;
      case 2:
	set_slidasfreq(chan);
	break;
      case 3:
	set_slidasmem(chan);
	break;
      case 4:
	set_slidasprog(chan);
	break;
      case 5:
	set_slidastrans(chan);
	break;
      }
      break;
    case 13:
      set_opctrl();  //MJ: May be more than one register
      break;
    case 14:
      set_pciconf();
      break;
    }
    printf("\n=============================================================\n");
  } while (fun != 0);
  return(0);
}


/******************/
int set_opctrl(void)
/******************/
{
  int data;

  data = dolar->opctrl1;   
  printf("Enter a new value for the Operation Control register 1");
  data = gethexd(data);
  dolar->opctrl1 = data;
  
  if (card_type == 2)
  {
    data = dolar->opctrl2;   
    printf("Enter a new value for the Operation Control register 2");
    data = gethexd(data);
    dolar->opctrl2 = data;

    data = dolar->opctrl3;   
    printf("Enter a new value for the Operation Control register 3");
    data = gethexd(data);
    dolar->opctrl3 = data;
  }
  
  return(0);
}


/*****************************/
int set_slidasctrl(int channel)
/*****************************/
{
  int data;

  CHANNEL_OK(channel)   

  printf("Enter a new value for the SLIDAS %d control register ", channel + 1);
  data = dolar->slidas[channel].ctrl;
  data = gethexd(data);
  dolar->slidas[channel].ctrl = data;
  return(0);
}


/*****************************/
int set_slidasfreq(int channel)
/*****************************/
{
  int freq;

  CHANNEL_OK(channel)   
    
  printf("Enter the new fragment frequency (in Hz) for channel %d: ", channel + 1);
  freq = 66666667 / (dolar->slidas[channel].freq + 1);
  freq = getdecd(freq);
  if (freq == 0) 
  {
    printf("ERROR: 0 is not a valid frequency\n");
    return (1);
  }
    
  dolar->slidas[channel].freq = (66666667 / freq) - 1;
  return(0);
}


/****************************/
int set_slidasmem(int channel)
/****************************/
{
  int data;

  CHANNEL_OK(channel)   

  printf("Enter new value for the SLIDAS %d memory register ", channel + 1);
  data = dolar->slidas[channel].mem;
  data = gethexd(data);
  dolar->slidas[channel].mem = data;
  return(0);
}


/*****************************/
int set_slidasprog(int channel)
/*****************************/
{
  int data = 0x0;

  CHANNEL_OK(channel)   

  printf("Enter new value for the SLIDAS %d program memory word ", channel + 1);
  data = gethexd(data);
  dolar->slidas[channel].prog = data;
  return(0);
}


/******************************/
int set_slidastrans(int channel)
/******************************/
{
  int data = 0x0;

  CHANNEL_OK(channel)   

  printf("Enter new value for the SLIDAS %d translation memory word ", channel + 1);
  data = gethexd(data);
  dolar->slidas[channel].trans = data;
  return(0);
}


/*******************/
int set_pciconf(void)
/*******************/
{
  u_int eret, offset=0, data;

  printf("Enter the offset of the register (4-byte alligned) ");
  offset = gethexd(offset);
  offset &= 0x3c;

  if ((eret = IO_PCIConfigReadUInt(shandle, offset, &data))) 
  {
    printf("ERROR: failed to read register\n");
    return(1);
  }
  printf("Enter new value for this register ");
  data = gethexd(data);
  
  if ((eret = IO_PCIConfigWriteUInt(shandle, offset, data))) 
  {
    printf("ERROR: failed to write register\n");
    return(2);
  }

  return(0);
}


/*****************/
int cardreset(void)
/*****************/
{
  u_int data;

  data = dolar->opctrl1;
  data |= 0x80000000;
  dolar->opctrl1 = data;
  
  if (card_type == 2)
  {
    data = dolar->opctrl2;
    data |= 0x80000000;
    dolar->opctrl2 = data;

    data = dolar->opctrl3;
    data |= 0x80000000;
    dolar->opctrl3 = data;
  }
  
  return(0);
}


/*****************/
int linkreset(void)
/*****************/
{
  static int c_limit, channel;
  u_int chan_mask = 0, status, ureset_mask = 0;
  int chan, i = 0;

  if (card_type == 1)
  {
    channel = 5;
    c_limit = 5;
    printf("Reset which channel (1-4: indvidual channel, 5:all selected channels, 0:cancel) ");
  }
  else
  {
    channel = 13;
    c_limit = 13;
    printf("Reset which channel (1-12: indvidual channel, 13:all selected channels, 0:cancel) ");
  }

  channel = getdecd (channel);
  if (channel > 0 && channel < c_limit) 
    chan_mask = 1 << (channel - 1);
  else if (channel == c_limit) 
    chan_mask = ChannelMask;
  else
  { 
    printf("ERROR: %d is not a legal value\n", channel);
    return (-1);
  }
  
  // set up the reset bit mask
  for (chan = 0; chan < 4; chan++) 
  {
    if (chan_mask & 1) 
      ureset_mask |= 1 << (8 * chan);   
    chan_mask >>= 1;
  }

  // clear the URESET bits to make sure that there is a falling edge on URESET_N 
  dolar->opctrl1 &= ~ureset_mask;
  
  // set the URESET bits 
  dolar->opctrl1 |= ureset_mask;
  printf("Waiting for links to come up ...\n");
  
  // now wait for LDOWN to come up again 
  while (((status = dolar->opstat1) & ureset_mask) && i++ < 100)
    ts_delay(10000); // wait for 10 ms to give the link time to come up 
  
  if (status & ureset_mask) 
    printf("ERROR: Link did not come up after reset (OPSTAT = 0x%08x)\n", status);
  
  // clear the URESET bits 
  dolar->opctrl1 &= ~ureset_mask;
  
  
  if (card_type == 2)
  {
    // Reset channels 5-8
    ureset_mask = 0;
    for (chan = 4; chan < 8; chan++) 
    {
      if (chan_mask & 1) 
	ureset_mask |= 1 << (8 * (chan - 4));   
      chan_mask >>= 1;
    }

    dolar->opctrl2 &= ~ureset_mask;
    dolar->opctrl2 |= ureset_mask;
    printf("Waiting for links 5-8 to come up ...\n");
    while (((status = dolar->opstat2) & ureset_mask) && i++ < 100)
      ts_delay(10000);
    if (status & ureset_mask) 
      printf("ERROR: Link in range 5-8 did not come up after reset (OPSTAT = 0x%08x)\n", status);
    dolar->opctrl2 &= ~ureset_mask;

    // Reset channels 9-12
    ureset_mask = 0;
    for (chan = 8; chan < 12; chan++) 
    {
      if (chan_mask & 1) 
	ureset_mask |= 1 << (8 * (chan - 8));   
      chan_mask >>= 1;
    }

    dolar->opctrl3 &= ~ureset_mask;
    dolar->opctrl3 |= ureset_mask;
    printf("Waiting for links 9-12 to come up ...\n");
    while (((status = dolar->opstat3) & ureset_mask) && i++ < 100)
      ts_delay(10000);
    if (status & ureset_mask) 
      printf("ERROR: Link in range 9-12 did not come up after reset (OPSTAT = 0x%08x)\n", status);
    dolar->opctrl3 &= ~ureset_mask;
  }
  
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\nPlease refer to the SLIDAS specification/user guide:\n");
  printf("<http://hsi.web.cern.ch/HSI/s-link/devices/slidas/user_guide3.html>\n");
  printf("Stefan Haas, stefan.haas@cern.ch\n");
  printf("----\n");
  printf("For the 12 channles version\n");
  printf("Markus Joos, markus.joos@cern.ch\n");
  return(0);
}


/*********************/
int slidas_config(void)
/*********************/
{
  static int c_limit = 5, mode = 1, channel = 5, pattern = 1, length = 256, freq = 250000, blocks = 1;
  static u_int lvl1_id = 0;
  u_int i, chan_mask, value, counter = 0;
  static char prog_fn[80] = "program.dat";

  if (card_type == 1)
  {
    channel = 5;
    c_limit = 5;
    printf("Enter the channel number (1-4: indvidual channel, 5:all selected channels, 0:cancel) ");
  }
  else
  {
    channel = 13;
    c_limit = 13;
    printf("Enter the channel number (1-12: indvidual channel, 13:all selected channels, 0:cancel) ");
  }
  
  channel = getdecd(channel);
  if (channel > 0 && channel < c_limit) 
    chan_mask = 1 << (channel - 1);
  else if (channel == c_limit) 
    chan_mask = ChannelMask; 
  else 
  {
    printf("ERROR: %d is not a legal value\n", channel);
    return (-1);
  }
  
  printf("==============================================================================\n");
  printf("Select the mode:\n");
  printf("  1 - Run                2 - N block             3 - Single word\n");
  printf("Your choice ");
  mode = getdecd(mode);

  switch (mode) 
  {
  case 1:
    value = STOP_MODE | RESET;
    printf("Enter the fragment frequency (in Hz) ");
    freq = getdecd(freq);
    if (freq == 0) 
    {
      printf("ERROR: 0 is not a legal frequency");
      return (-1);
    }
    counter = (66666667 / freq) - 1;
    break;
    
  case 2:
    value = FRAME_MODE | RESET;
    printf("How many blocks ");
    blocks = getdecd(blocks);
    if (blocks == 0) 
    {
      printf("ERROR: 0 is not a legal number of blocks");
      return (-1);
    }
    break;
    
  case 3:
    value = STEP_MODE | RESET;
    break;
    
  default:
    printf("ERROR: wrong mode!\n");
    return(-1);
  }

  printf("==============================================================================\n");
  printf("Select the pattern:\n");
  printf("  1 = Walking one               2 = Walking zero\n");
  printf("  3 = FFFF/0000                 4 = AAAA/5555\n");
  printf("  5 = Random data               6 = ROD fragment (v3.0)\n");
  printf("  7 = User defined\n");
  printf("Your choice ");
  pattern = getdecd(pattern);

  switch (pattern) 
  {
  case 1:
  case 2:
  case 3:
  case 4:
  case 5:
    printf("Enter the fragment length (1-8191 words) ");
    length = getdecd(length);
    if (length < 1 || length > 8191) 
    {
      printf("ERROR: wrong length!\n");
      return(-1);
    }
    break;
    
  case 6:
  case 7:
    printf("Enter the fragment length (14-8191 words) ");
    length = getdecd(length);
    if (length < 14 || length > 8191) 
    {
      printf("ERROR: wrong length!\n");
      return(-1);
    }
    
    printf("Enter the level 1 ID ");
    lvl1_id = getdecd(lvl1_id);
    break;
    
  case 8:
    printf("Enter the program file name ");
    getstrd(prog_fn, prog_fn);
    break;
    
  default:
    printf("RROR: wrong selection!\n");
    return(-1);
  }
    
  for (i = 0; i < max_channels; i++) 
  {
    if (chan_mask & 1) 
    {
      dolar->slidas[i].ctrl = STOP_MODE;              // stop channel
      usleep(100000);                                 // allow some time to finish transmission
      dolar->slidas[i].ctrl = RESET;                  // reset channel
      usleep(1000);
      dolar->slidas[i].ctrl = PROG_MODE;	      // assert programming mode
      while ((dolar->slidas[i].sts & 0x1) == 0)       // waiting for the slidas to be ready
	usleep(1000);
      dolar->slidas[i].mem = 0;                       // start writing at address 0
      switch (pattern) 
      {
      case 1:
	setup_walking_pattern(i, length, 1);
	break;
      case 2:
	setup_walking_pattern(i, length, 0);
	break;
      case 3:
	setup_alt_pattern(i, length, 0xffffffff);
	break;
      case 4:
	setup_alt_pattern(i, length, 0xaaaaaaaa);
	break;
      case 5:
	setup_rnd_pattern(i, length);
	break;
      case 6:
	setup_rod_pattern(i, length, lvl1_id);
	break;
      case 7:
	fill_memories(i, prog_fn);
	break;
      }
      if (mode == 1) dolar->slidas[i].freq = counter;
      if (mode == 2) dolar->slidas[i].freq = blocks;
      dolar->slidas[i].ctrl = value;
    }
    chan_mask >>= 1;
  }
  
  if (pattern == 6 && mode == 2) lvl1_id += blocks;

  printf("==============================================================================\n");
  
  return(0);
}


/***********************************************************/
int setup_rod_pattern(int channel, int length, u_int lvl1_id)
/***********************************************************/
{
  // program the translation table
  dolar->slidas[channel].trans = 0xb0f00000;              // index 0: start-of-fragment
  dolar->slidas[channel].trans = 0xee1234ee;              // index 1: ROD header marker
  dolar->slidas[channel].trans = 0x00000009;              // index 2: header length
  dolar->slidas[channel].trans = 0x03010000;              // index 3: format version number
  dolar->slidas[channel].trans = 0x00000000;              // index 4: 0
  dolar->slidas[channel].trans = 0x00000001;              // index 5: 1
  dolar->slidas[channel].trans = length - 13;             // index 6: data size
  dolar->slidas[channel].trans = 0xe0f00000;              // index 7: end-of-fragment
  dolar->slidas[channel].trans = 0x00000003;              // index 8: 3
  dolar->slidas[channel].trans = lvl1_id - 1;             // index 9: level 1 ID
  dolar->slidas[channel].trans = 3 * lvl1_id - 3;         // index 10: BCID = 3 * L1ID
  dolar->slidas[channel].trans = 0x00a00000;              // index 11: source identifier
  dolar->slidas[channel].trans = 0x00000007;              // index 12: level 1 trigger type
  dolar->slidas[channel].trans = 0x0000000a;              // index 13: detector event type

  // program the instruction memory
  dolar->slidas[channel].prog = LOAD_INIT(1, 9);          // 0:  load counter 1 with starting L1ID  
  dolar->slidas[channel].prog = LOAD_INIT(2, 10);         // 1:  load counter 1 with starting L1ID  
  dolar->slidas[channel].prog = CONTROL(0, 1);            // 2:  start-of-fragment
  dolar->slidas[channel].prog = CONST(1, 1);              // 3:  ROD header marker
  dolar->slidas[channel].prog = CONST(2, 1);              // 4:  header length
  dolar->slidas[channel].prog = CONST(3, 1);              // 5:  format version number
  dolar->slidas[channel].prog = CONST(11, 1);             // 6:  source identifier = 0xa00000
  dolar->slidas[channel].prog = CONST(5, 1);              // 7:  run number = 1
  dolar->slidas[channel].prog = COUNTER(1, 5, 1);         // 8:  level 1 ID = counter 1, increment 1
  dolar->slidas[channel].prog = COUNTER(2, 8, 1);         // 9:  bunch crossing ID = counter 2, increment 3
  dolar->slidas[channel].prog = CONST(12, 1);             // 10  trigger type = 7
  dolar->slidas[channel].prog = CONST(13, 1);             // 11: detector event type = 0xa
  dolar->slidas[channel].prog = LOAD(4, 4);               // 12:  data = counter 4, load with 0
  dolar->slidas[channel].prog = COUNTER(4, 5, length-14); // 13:  data = counter 4, increment by 1
  dolar->slidas[channel].prog = CONST(4, 1);              // 14: status element = 0
  dolar->slidas[channel].prog = CONST(5, 1);              // 15: number of status elements = 1
  dolar->slidas[channel].prog = CONST(6, 1);              // 16: number of data elements = length - 13
  dolar->slidas[channel].prog = CONST(5, 1);              // 17: status block position = 1 (after data)
  dolar->slidas[channel].prog = CONTROL(7, 1);            // 18: end-of-fragment
  dolar->slidas[channel].prog = END;                      // 19: end of fragment instruction

  dolar->slidas[channel].mem |= MEM_REGISTER(19, 2);      // program memory contains 20 instructions
                                                          // loop to instruction 2
  return(0);
}


/********************************************************/
int setup_alt_pattern(int channel, int length, u_int data)
/********************************************************/
{
  // program the translation table
  dolar->slidas[channel].trans = 0xb0f00000;           // index 0: start-of-fragment
  dolar->slidas[channel].trans = data;                 // index 1: data value
  dolar->slidas[channel].trans = ~data;                // index 2: inverted data
  dolar->slidas[channel].trans = 0xe0f00000;           // index 3: end-of-fragment

  // program the instruction memory
  dolar->slidas[channel].prog = CONTROL(0, 1);         // 0:  start-of-fragment
  dolar->slidas[channel].prog = ALT(1, 2, length);     // 1:  alternating data pattern
  dolar->slidas[channel].prog = CONTROL(3, 1);         // 2: end-of-fragment
  dolar->slidas[channel].prog = END;                   // 3: end of fragment instruction

  dolar->slidas[channel].mem |= MEM_REGISTER(3,0);     // program memory contains 4 instructions

  return(0);
}


/********************************************/
int setup_rnd_pattern(int channel, int length)
/********************************************/
{
  // program the translation table
  dolar->slidas[channel].trans = 0xb0f00000;            // index 0: start-of-fragment
  dolar->slidas[channel].trans = 0xe0f00000;            // index 1: end-of-fragment

  // program the instruction memory
  dolar->slidas[channel].prog = CONTROL(0, 1);          // 0:  start-of-fragment
  dolar->slidas[channel].prog = RANDOM(length);         // 1:  alternating data pattern
  dolar->slidas[channel].prog = CONTROL(1, 1);          // 2: end-of-fragment
  dolar->slidas[channel].prog = END;                    // 3: end of fragment instruction

  dolar->slidas[channel].mem |= MEM_REGISTER(3,0);      // program memory contains 4 instructions

  return(0);
}


/**********************************************************/
int setup_walking_pattern(int channel, int length, int mode)
/**********************************************************/
{
  // program the translation table
  dolar->slidas[channel].trans = 0xb0f00000;           // index 0: start-of-fragment
  dolar->slidas[channel].trans = (mode) ? 1 : ~1;      // index 0: start-of-fragment
  dolar->slidas[channel].trans = 0xe0f00000;           // index 3: end-of-fragment

  // program the instruction memory
  dolar->slidas[channel].prog = CONTROL(0, 1);         // start-of-fragment
  dolar->slidas[channel].prog = SHIFT(1, 1, length);   // walking bit pattern
  dolar->slidas[channel].prog = CONTROL(2, 1);         // end-of-fragment
  dolar->slidas[channel].prog = END;                   // end of fragment instruction

  dolar->slidas[channel].mem |= MEM_REGISTER(3, 0);    // address of last instruction

  return(0);
}


/**************************************************/
int fill_memories(int channel, char* prog_file_name)
/**************************************************/
{
  FILE *program_file;
  u_int prog_table [PROG_NUM_OF_WORDS], trans_table [TRANS_NUM_OF_WORDS], instr_args[3];
  char line[80];
  long long args[3];
  int noi = 0, nb_args, trans_table_index = 0, index = 0, index_alt = 0, i, init_address = 0;
	
  // Reading the instructions from the program file and writing its in the memory 	
  if ((program_file = fopen(prog_file_name, "r")) == NULL) 
  {
    printf("Error when opening %s\n", prog_file_name);
    return(-1);
  }

  while (!feof(program_file)) 
  {
    if (noi >= PROG_NUM_OF_WORDS) 
    {
      printf("ERROR: to many (%d) instructions\n", noi + 1);
      return(-1);
    }

    // Read the instruction opcode
    nb_args = fscanf(program_file, "%s %lli %lli %lli\n", line, args, args + 1, args + 2);
    nb_args--;
    //for (i = 0; i < 3; i++) instr_args[i] = args[i] & 0xffffffff;
    //    printf("command %s, %i args: %x %x %i\n", line, nb_args, instr_args[0], instr_args[1], instr_args[2]);
	  
    if (strcmp((char*)line, "END") == 0) 
    {
      if (nb_args != 0) 
      {
	printf("ERROR: wrong number of args\n");
	return(-1);
      }
      prog_table[noi] = END;
      //	printf("Instruction %i : END, 0x%x\n", noi, prog_table[noi]);
      noi++;
    }

    if (strcmp((char*)line, "CONTROL") == 0) 
    {
      if (nb_args != 2) 
      {
	printf("ERROR: wrong number of arguments, CONTROL <value> <repeat>\n");
	return(-1);
      }
      if ((index = get_trans_table_index(trans_table, &trans_table_index, instr_args[0])) == -1) 
      {
	printf("ERROR: translation table full!\n");
	return(-1);
      }
      prog_table[noi] = CONTROL(index, instr_args[1]);
      //	    printf("Instruction %i : CONTROL ,valeur hex :  %x\n",noi,prog_table[noi]);
      noi++;
    }

    if (strcmp((char*)line, "RANDOM") == 0) 
    {
      if (nb_args != 1) 
      {
	printf("ERROR: wrong number of arguments for instruction RANDOM <repeat>\n"); 
	return(-1);
      }
      prog_table[noi] = RANDOM(instr_args[0]);
      //	printf("Instruction %i : RANDOM , valeur hex :  %x\n",noi,prog_table[noi]);
      noi++;
    }

    if (strcmp((char*)line, "COUNTER") == 0) 
    {
      if (nb_args != 3) 
      {
	printf("ERROR: wrong number of arguments for instruction COUNTER <counter no> <incr> <repeat>\n"); 
	return(-1);
      }
      if ((index = get_trans_table_index(trans_table, &trans_table_index, instr_args[1])) == -1) 
      {
	printf("ERROR: translation table full!\n");
	return(-1);
      }
      prog_table[noi] = COUNTER(instr_args[0], index, instr_args[2]);
      //	  printf("Instruction %i : COUNTER , valeur hex :  %x\n",noi,prog_table[noi]);
      noi++;
    }

    if (strcmp((char*)line, "SHIFT") == 0) 
    {
      if (nb_args != 3) 
      {
	printf("ERROR: wrong number of arguments for instruction COUNTER <counter no> <incr> <repeat>\n"); 
	return(-1);
      }
      if ((index = get_trans_table_index(trans_table, &trans_table_index, instr_args[1])) == -1) 
      {
	printf("ERROR: translation table full!\n");
	return(-1);
      }
      prog_table[noi] = SHIFT(instr_args[0], index, instr_args[2]);
      noi++;
    }

    if (strcmp((char*)line, "LOAD") == 0) 
    {
      if (nb_args != 2) 
      {
	printf("ERROR: wrong number of arguments for instruction LOAD <counter no> <value>\n"); 
	return(-1);
      }
      if ((index = get_trans_table_index(trans_table, &trans_table_index, instr_args[1])) == -1) 
      {
	printf("ERROR: translation table full!\n");
	return(-1);
      }
      prog_table[noi] = LOAD(instr_args[0], index);
      //	  printf("Instruction %i : LOAD , valeur hex :  %x\n",noi,prog_table[noi]);
      noi++;
    }

    if (strcmp((char*)line, "LOAD_INIT") == 0) 
    {
      if (nb_args != 2) 
      {
	printf("ERROR: wrong number of arguments for instruction LOAD_INIT <counter no> <value>\n"); 
	return(-1);
      }
      // instruction for correct initial value:
      //instr_args[1] = (instr_args[1] > 0) ? instr_args[1] - 1 : 0xFFFFFFFF; // fix starting value
      if ((index = get_trans_table_index(trans_table, &trans_table_index, instr_args[1])) == -1) 
      {
	printf("ERROR: translation table full!\n");
	return(-1);
      }
      prog_table[noi] = LOAD_INIT(instr_args[0], index);
      //	  printf("Instruction %i : LOAD_INIT , valeur hex :  %x\n",noi,prog_table[noi]);
      noi++;
    }

    if (strcmp((char*)line, "BEGIN") == 0) 
    {
      if (nb_args != 0) 
      {
	printf("ERROR: wrong number of arguments for instruction BEGIN \n"); 
	return(-1);
      }
      init_address = noi;
    }

    if (strcmp((char*)line, "CONST") == 0) 
    {
      if (nb_args != 2) 
      {
	printf("ERROR: wrong number of arguments, CONTROL <value> <repeat>\n");
	return(-1);
      }
      if ((index = get_trans_table_index(trans_table, &trans_table_index, instr_args[0])) == -1) 
      {
	printf("ERROR: translation table full!\n");
	return(-1);
      }
      prog_table[noi] = CONST(index, instr_args[1]);
      //	  printf("Instruction %i : CONST , valeur hex :  %x\n",noi,prog_table[noi]);
      noi++;
    }

    if (strcmp((char*)line, "ALT") == 0) 
    {
      if (nb_args != 3) 
      {
	printf("ERROR: wrong number of arguments, ALT <value1> <value2> <repeat>\n");
	return(-1);
      }
      if ((index = get_trans_table_index(trans_table, &trans_table_index, instr_args[0])) == -1) 
      {
	printf("ERROR: translation table full!\n");
	return(-1);
      }
      if ((index_alt = get_trans_table_index(trans_table, &trans_table_index, instr_args[1])) == -1) 
      {
	printf("ERROR: translation table full!\n");
	return(-1);
      }
      prog_table[noi] = ALT(index, index_alt, instr_args[2]);
      //	    printf("Instruction %i : ALT , valeur hex :  %x\n",noi,prog_table[noi]);
      noi++;
    }
  }
  fclose(program_file);

  for (i = 0; i < noi; i++)
    dolar->slidas[channel].prog = prog_table[i];

  dolar->slidas[channel].mem = MEM_REGISTER(noi - 1, init_address); // number of instructions

  for (i = 0; i < trans_table_index; i++)
    dolar->slidas[channel].trans = trans_table[i];

  return(0);
}


/*********************************************************************/
int trans_table_contains(u_int trans_table[], int nb_elem, u_int value)
/*********************************************************************/
{
  int i;

  for (i = 0; i < nb_elem; i++) 
  {	
    if (trans_table[i] == value) 
      return(i);
  }
  
  return(-1);	
}


/*****************************************************************/
int get_trans_table_index(u_int table[], int* entries, u_int value)
/*****************************************************************/
{
  int i;

  for (i = 0; i < *entries; i++)
    if (table[i] == value) 
      break;
      
  if (i < *entries) 
  { // value is in the table
#ifdef DEBUG
    printf("value 0x%x found in table, index %i\n", table[i], i);
#endif
    return(i);
  }
  
  if (*entries < TRANS_NUM_OF_WORDS) 
  { // room for one more entry
    table[i] = value;
    *entries += 1;
#ifdef DEBUG
    printf("added new entry 0x%x, index %i, number of entries %i\n", table[i], i, *entries);
#endif
    return(i);
  }
  printf("ERROR: translation table full!\n");
  return(-1);
}


/**********************/
int slidas_runstop(void)
/**********************/
{
  static int channel = 5, c_limit = 5;
  u_int chan_mask, i;
  
  if (card_type == 1)
  {
    channel = 5;
    c_limit = 5;
    printf("Start/stop which channel (1-4: indvidual channel, 5:all selected channels, 0:cancel) ");
  }
  else
  {
    channel = 13;
    c_limit = 13;
    printf("Start/stop which channel (1-12: indvidual channel, 13:all selected channels, 0:cancel) ");
  }
  
  channel = getdecd (channel);
  if (channel > 0 && channel < c_limit) 
    chan_mask = 1 << (channel - 1);
  else if (channel == c_limit) 
    chan_mask = ChannelMask;
  else
  {
    printf("ERROR: %d is not a legal value\n", channel); 
    return (-1);
  }
 
  for (i = 0; i < max_channels; i++) 
  {
    if (chan_mask & 1) 
    {
      switch ((dolar->slidas[i].ctrl) & 0xC000) 
      {
      case STOP_MODE: // stop mode
	printf("Channel %d started\n", i + 1);
	dolar->slidas[i].ctrl = RUN_MODE; // switch to run mode
	break;
      case RUN_MODE: // run mode
	dolar->slidas[i].ctrl = STOP_MODE; // switch to stop mode
	printf("Channel %d stopped\n", i + 1);
	break;
      case FRAME_MODE: // frame-by-frame mode
	printf("Channel %d sending %d blocks\n", i + 1, dolar->slidas[i].freq);
	dolar->slidas[i].ctrl = STEP_FORWARD | FRAME_MODE; // one frame forward
	break;
      case STEP_MODE: // step-by-step mode
	printf("Channel %d single word sent\n", i + 1);
	dolar->slidas[i].ctrl = STEP_FORWARD | STEP_MODE; // one step forward
	break;
      }
    }
    chan_mask >>= 1;
  }
  return(0);
}


/*****************/
int calc_rate(void)
/*****************/
{
  u_int i, start_w[12], stop_w[12], start_b[12], stop_b[12];
  double w_rate, b_rate;

  for (i = 0; i < max_channels; i++) 
  {
    start_w[i] = dolar->slidas[i].wrdcnt;
    start_b[i] = dolar->slidas[i].blkcnt;
  }
  sleep(2);
  for (i = 0; i < max_channels; i++) 
  {
    stop_w[i] = dolar->slidas[i].wrdcnt;
    stop_b[i] = dolar->slidas[i].blkcnt;
  }
  for (i = 0; i < max_channels; i++) 
  {
    w_rate = 0.5E-6 * (stop_w[i] - start_w[i]);
    b_rate = 0.5E-3 * (stop_b[i] - start_b[i]);
    printf("Channel %d data rate = %5.1f Mword/s   fragment rate = %5.1f kHz\n", i + 1, w_rate, b_rate);
  }
  return(0);
}


/***********************/
int select_channels(void)
/***********************/
{
  u_int chan, mask = 0, enable = 1;

  for (chan = 0; chan < max_channels; chan++) 
  {
    printf("Select channel %d (0:no, 1:yes) ", chan + 1);
    enable = getdecd (enable);
    if (enable == 1) 
      mask |= 1 << chan;
  }
  
  ChannelMask = mask;
  return(0);
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  int stat, ret, fun = 1, occ = 1;
  u_int fw_ver, fw_ver2, *fw_ptr, fw;
  struct sigaction sa;

  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler; 
  stat = sigaction(SIGQUIT, &sa, NULL);
  if (stat < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", stat);
    exit(-1);
  }

  if ((argc == 2) && (sscanf(argv[1], "%d", &occ) == 1)) 
    argc--;
  else 
    occ = 1;
  if (argc != 1)
  {
    printf("This is DOLARSCOPE. \n\n");
    printf("Usage: dolarscope [DOLAR occurrence]\n");
    exit(0);
  }

  if (occ == 0)
  {
    printf("Occurrence must not be 0\n");
    exit(0);
  }
  
  ts_open(1, TS_DUMMY);
  dolar_map(occ);
  
  if ((ret = IO_PCIConfigReadUInt(shandle, 0x8, &fw_ver)) != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  fw_ver &= 0xff;
  
  if (card_type == 1)   
    printf("\nDOLARSCOPE running on a card of type DOLAR with F/W revision %02x\n", fw_ver);
  else
  {
    fw_ptr = (u_int *)dolar;
    fw_ver2 = fw_ptr[215];
    printf("\nDOLARSCOPE running on a card of type DOZOLAR with F/W revision %02x, version %02x\n", fw_ver, fw_ver2);
  }
  
  if (card_type == 1)
    fw = DOLAR_FIRMWARE_VERSION;
  else
    fw = DOLAR12_FIRMWARE_VERSION;
    
  if (fw_ver < fw) 
  {
    printf("\nThis version only runs on cards with firmware revision %02x !\n", fw);
    exit(1);
  }
  
  while (fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("  1 Print help            2 Dump PCI conf. space  3 Dump memory registers\n");
    printf("  4 Write to a register   5 Reset the DOLAR       6 Select channels\n");
    printf("  7 Reset S-LINK          8 Configure SLIDAS      9 Run/stop SLIDAS\n");
    printf(" 10 Calculate data rate  11 Stability test\n");
    printf(" 0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    printf("\n");
    if (fun == 1) mainhelp();
    if (fun == 2) dumpconf();
    if (fun == 3) dumpmem();
    if (fun == 4) setreg();
    if (fun == 5) cardreset();
    if (fun == 6) select_channels();
    if (fun == 7) linkreset();
    if (fun == 8) slidas_config();
    if (fun == 9) slidas_runstop();
    if (fun == 10) calc_rate();
    if (fun == 11) stability_test();
 }

  dolar_unmap();
  ts_close(TS_DUMMY);
  exit(0);
}

