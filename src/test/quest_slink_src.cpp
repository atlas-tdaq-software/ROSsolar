/************************************************************************/
/*									*/
/* File: quest_slink_src.cpp						*/
/*									*/
/* Read event fragments from a file 					*/
/* and send them out on a single S-Link channel 			*/
/*									*/
/*  4. Aug. 09  MAJO  created						*/
/*									*/
/**************** C 2009 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "ROSsolar/quest.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif

#define BUFSIZE  (1024 * 1024)       //bytes

//Globals
u_int dblevel = 0, dbpackage = DFDB_ROSQUEST;
u_long virtbase, pcibase;
int verbose = FALSE;
int occurence = 1;
int readfile = FALSE;
char filename[200] = {0};
int forceID = FALSE;

/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-o x: Occurence (= ROL number 1..N)                -> Default: " << occurence << std::endl;
  std::cout << "-f x: Read data from file. The parameter is the path and the name of the file" << std::endl;
  std::cout << "-I x: Force monotonically increasing L1 ID (starting from zero), ignoring ID's in file -> Default: FALSE" << std::endl;
  std::cout << "-v  : Verbose output                               -> Default: FALSE" << std::endl;
  std::cout << "-d  : Debug level                                  -> Default: 0" << std::endl;
  std::cout << "-D  : Debug package                                -> Default: 14" << std::endl;
  std::cout << std::endl;
}

/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int c, ret, memhandle;
  u_int word_number, nfree, freeref, *data;
  u_int fret, event_id = 0, event_size, edloop;
  QUEST_config_t fconfig;
  QUEST_in_t fin;

  static struct option long_options[] = {"DVS", no_argument, NULL, '1'}; 

  while ((c = getopt_long(argc, argv, "o:f:vId:D:h", long_options, NULL)) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]: "<< std::endl;
      usage();
      exit(-1);
      break;

    case 'f':   
      readfile = TRUE;
      sscanf(optarg,"%s", filename);
      std::cout << "read data from file " << filename << std::endl;
      break;
      
    case 'o': occurence = atoi(optarg);     break;
    case 'v': verbose = TRUE;               break;
    case 'd': dblevel = atoi(optarg);       break;
    case 'D': dbpackage = atoi(optarg);     break;
    case 'I': forceID = TRUE;				break;

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << " [options]: " << std::endl;
      usage();
      exit (-1);
    }
    
  occurence -= 1; //Quest channel numbering starts at 0

  // Initialize the debug macro
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);      
    
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  

  ret = CMEM_BPASegmentAllocate(BUFSIZE, (char*)("QUEST_BUFFER"), &memhandle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  
  ret = CMEM_SegmentVirtualAddress(memhandle, &virtbase);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentPhysicalAddress(memhandle, &pcibase);
  if (ret)
    rcc_error_print(stdout, ret);
 
  printf("Contiguous buffer:\n");
  printf("Physical address = 0x%08lx\n", pcibase);
  printf("Virtual address  = 0x%08lx\n", virtbase);
  data = (u_int *) (virtbase);

  ret = QUEST_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  
  if (verbose)
    std::cout << "Calling QUEST_Reset" << std::endl;
  ret = QUEST_Reset(occurence >> 2); // reset the card the channel is on
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  if (verbose)
    std::cout << "Calling QUEST_LinkReset" << std::endl;
  ret = QUEST_LinkReset(occurence); // reset the link
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  fconfig.bswap = 0;
  fconfig.wswap = 0;
  fconfig.scw = 0xb0f00000;
  fconfig.ecw = 0xe0f00000; 

  ret = QUEST_Init(&fconfig);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  } 

  //Open the input file
  FILE *inf;
  inf = fopen(filename, "r");
  if(inf == 0)
  {
    std::cout << "Can't open input file " << filename << std::endl;
    exit(-1);
  }

  word_number = 0;

  //Just to know the number of free pages in idle state
  ret = QUEST_InFree(occurence, &freeref);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  while(!feof(inf))
  {
    //Read fragment size
    fret = fscanf(inf, "%x", &event_size);
    if (fret != 1)
      break;
    word_number++;

    if (verbose)
      std::cout << "ROD Event size = " << event_size << " words." << std::endl;

    if (event_size > BUFSIZE)
    {
      std::cout << "Event is too big" << std::endl;
      exit(-1);
    }

    //Read the event data
    for (edloop = 0; edloop < event_size; edloop++)
    {
      fscanf(inf, "%x", &data[edloop]);
      if (verbose && edloop < 15)
	std::cout << "Data word " << word_number << " is " << HEX(data[edloop]) << std::endl;
      word_number++;
    }

    if(forceID == TRUE){
    	data[5] = event_id;         //Replace the original event ID
    }

    nfree = 0;
    while (nfree != freeref)
    {  
      ret = QUEST_InFree(occurence, &nfree);
      if (ret)
      {
	rcc_error_print(stdout, ret);
	return(-1);
      }
    }
    
    if (verbose){
        if(forceID == TRUE){
        	std::cout << "Sending L1ID = " << event_id << std::endl;
        }
        else{
        	std::cout << "Sending L1ID = " << data[5] << std::endl;
        }
    }

    fin.nvalid = 1; 
    fin.channel = occurence; 
    fin.size[0] = event_size;
    fin.scw[0] = 1;
    fin.ecw[0] = 1;
    fin.pciaddr[0] = pcibase;   

    ret = QUEST_PagesIn(&fin);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return(-1);
    }

    QUEST_Flush(occurence);
    if(forceID == TRUE){
    	event_id++;
    }
}
  std::cout << "In total the file had " << word_number - 2 << " words and " << event_id << " events." << std::endl;
	
  ret = QUEST_Close();
  if (ret)
    rcc_error_print(stdout, ret);

  ret = CMEM_BPASegmentFree(memhandle);
  if (ret)
    rcc_error_print(stdout, ret);
 
  ret = CMEM_Close();
  if (ret)
    rcc_error_print(stdout, ret);
   
  return(0);
}


