/************************************************************************/
/*									*/
/* File: quest_test.c							*/
/*									*/
/* This is the test program for the ROSquest package 			*/
/*									*/
/*  10. Feb. 04  MAJO  created						*/
/*									*/
/**************** C 2004 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "ROSsolar/quest.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"


//Globals
u_int isactive[MAXQUESTROLS], cont, sent[MAXQUESTROLS] ={0}, delta[MAXQUESTROLS] ={0}, dblevel = 0, dbpackage = DFDB_ROSQUEST;
u_long virtbase, pcibase;
static struct sigaction sa, sa2;

//Prototypes
int mainhelp(void);
int func_menu(void);
int auto_menu(void);
int error_menu(void);
int controlword_menu(void);
int sendevents(void);
int sendrodevents(int ecr_mode);
int setdebug(void);
int stress(void);
void generate_fragment_2(u_long ptr, u_int *size);
void generate_fragment_1(u_long ptr, u_int l1id, u_int size);
void generate_fragment_3(u_long ptr, u_int l1id, u_int *size);
void generate_fragment_4(u_long ptr, u_int l1id, u_int mode, u_int *size);

#define DEBUG 1
#ifdef DEBUG
  #define debug(x) printf x
#else
  #define debug(x)
#endif

#define BUFSIZE  (4 * 1024 * 1024)       /*bytes*/

/*****************************/
void SigQuitHandler(int /*signum*/)
/*****************************/
{
  debug(("SigQuitHandler: ctrl+\\ sent\n"));
  cont = 0;
  alarm(0);
}


/***************************/
void AlarmHandler(int /*signum*/)
/***************************/
{
  int loop;
  
  printf("Number of fragments sent:\n");
  for(loop = 0; loop < MAXQUESTROLS; loop++)
  {
    if (isactive[loop])
    {
      sent[loop] += delta[loop];
      printf("Channel %d: total #=%d    # per second=%d\n", loop, sent[loop], delta[loop]/2);
      delta[loop] = 0;
    }
  }
  
  if(cont)
    alarm(2);
}


/****************/
int setdebug(void)
/****************/
{
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}


/************/
int main(void)
/************/
{
  int fun = 3;
  int ret;
  int memhandle;
  u_int loop, *ptr;
 
  printf("\n\n\nThis is the test program for the QUEST library \n");
  printf("===================================================\n");

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler;
  ret = sigaction(SIGQUIT, &sa, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = AlarmHandler;
  ret = sigaction(SIGALRM, &sa2, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  ret = CMEM_BPASegmentAllocate(BUFSIZE, (char*)("QUEST_BUFFER"), &memhandle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  
  ret = CMEM_SegmentVirtualAddress(memhandle, &virtbase);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentPhysicalAddress(memhandle, &pcibase);
  if (ret)
    rcc_error_print(stdout, ret);
 
  printf("Contiguous buffer:\n");
  printf("Physical address = 0x%08lx\n", pcibase);
  printf("Virtual address  = 0x%08lx\n", virtbase);

  // Initialize the buffer
  ptr = (u_int *) virtbase;
  for(loop = 0; loop < (BUFSIZE >> 2); loop++)
    *ptr++ = loop;

  while (fun != 0)  
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                         2 Test functions\n");
    printf("   3 Automatic tests              4 Send ROD fragments with errors\n");
    printf("   5 Set debug parameters         6 Send ROD fragments with user defined control words\n");
    printf("   7 Stress test Robin interrupts\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) func_menu();
    if (fun == 3) auto_menu();
    if (fun == 4) error_menu();
    if (fun == 5) setdebug();
    if (fun == 6) controlword_menu();
    if (fun == 7) stress();
  }

  ret = CMEM_BPASegmentFree(memhandle);
  if (ret)
    rcc_error_print(stdout, ret);
 
  ret = CMEM_Close();
  if (ret)
    rcc_error_print(stdout, ret);
   
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
  return(0);
}


/*****************/
int auto_menu(void)
/*****************/
{
  int fun = 1;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a test:\n");
    printf("   1 Send packets (no ROD events)\n");
    printf("   2 Send ROD events\n");
    printf("   3 Send ROD events with ECRs\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(2);    
    if (fun == 1) sendevents();
    if (fun == 2) sendrodevents(0);
    if (fun == 3) sendrodevents(1);
  }
  printf("=========================================================================\n\n");
  return(0);
}


/******************/
int sendevents(void)
/******************/
{      
  QUEST_ErrorCode_t ret;
  QUEST_config_t fconfig;
  QUEST_in_t fin[MAXQUESTROLS];
  static u_int nchannels = 1, actchan = 0, size = 256;
  static int nfrag = -1;
  u_int nfree, loop, *ptr;
  int channel;

  for (loop = 0; loop < MAXQUESTROLS; loop++)
    isactive[loop] = 0;
    
  printf("Enter the number of channels: ");
  nchannels = getdecd(nchannels);
  
  ret = QUEST_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  
  actchan = 0;
  for(loop = 0; loop < nchannels; loop++)
  {
    printf("Enter the number of channel %d (0..N): ", loop);
    actchan = getdecd(actchan);
    isactive[actchan] = 1;
    ret = QUEST_Reset(actchan >> 2); // reset the card the channel is on
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return(-1);
    }

    ret = QUEST_LinkReset(actchan); // reset the link
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return(-1);
    }
    actchan++;
  }
    
  printf("Enter the block size in words (max %d) : ", (BUFSIZE - 4) / 4);
  size = getdecd(size);

  printf("Enter the number of fragments to send (-1=continous) : ");
  nfrag = getdecd(nfrag);

  fconfig.bswap = 0;
  fconfig.wswap = 0;
  fconfig.scw = 0xb0f00000;
  fconfig.ecw = 0xe0f00000; 

  ret = QUEST_Init(&fconfig);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  } 

  //Initialize the buffer
  ptr = (u_int *) virtbase;
  for (loop = 0; loop < size; loop++)
    *ptr++ = loop;
  
  for (channel = 0; channel < MAXQUESTROLS; channel++)
  {
    if (isactive[channel])
    {
      int i;
      fin[channel].nvalid = MAXQUESTINFIFO; 
      fin[channel].channel = channel; 
      for (i = 0; i < MAXQUESTINFIFO; i++) 
      {
	fin[channel].pciaddr[i] = pcibase;  //we use same page buffer all the time 
	fin[channel].size[i] = size;
	fin[channel].scw[i] = 1;
	fin[channel].ecw[i] = 1;
      }
      delta[channel] = 0;
    }
  }

  cont = 1;
  alarm(2);
  printf("Press ctrl+\\ to stop this test\n");
        
  while (cont)
  {  
    for (channel = 0; channel < MAXQUESTROLS; channel++)
    {
      if (isactive[channel])
      {  
        nfree = 0;
        while (!nfree) //(nfree < 64)
        {
          ret = QUEST_InFree(channel, &nfree);
          if (ret)
          {
            rcc_error_print(stdout, ret);
            return(-1);
          }
        }
	fin[channel].nvalid = nfree;
        ret = QUEST_PagesIn(&fin[channel]);
        if (ret == 0) 
	{
          delta[channel] += nfree;
	  if (nfrag > 0) 
	  {
	    nfrag -= nfree;
	    if (nfrag < 1) cont = 0;
	  }
	}
        else
        {
          rcc_error_print(stdout, ret);
          return(-1);
        }
	QUEST_Flush(channel);
      }
    }
  }
  alarm(0);
  ret = QUEST_Close();
  if (ret)
    rcc_error_print(stdout, ret);
  return(0);
}


/*****************************/
int sendrodevents(int ecr_mode)
/*****************************/
{      
  #define NEVENTS 1000
  QUEST_ErrorCode_t ret;
  QUEST_config_t fconfig;
  QUEST_in_t fin[MAXQUESTROLS];
  static u_int dpattern = 0, vmode = 0, firstl1id = 0, ecr_gap = 10, nchannels = 1, actchan = 0, alisize, size = 256;
  static int nfrag = -1, cfrags[MAXQUESTROLS];
  u_int l1id[MAXQUESTROLS], nfree, freeref, loop, *ptr[NEVENTS], nextptr;
  int channel;

  for (loop = 0; loop < MAXQUESTROLS; loop++)
    isactive[loop] = 0;
    
  printf("Select the verbosity (0/1): ");
  vmode = getdecd(vmode);

  printf("Enter the number of channels (max. %d): ", MAXQUESTROLS);
  nchannels = getdecd(nchannels);
  
  if(ecr_mode)
  {
    printf("Enter the number of events per ECR: ");
    ecr_gap = getdecd(ecr_gap);
    printf("Enter the first L1Id: ");
    firstl1id = gethexd(firstl1id);
  }
  
  ret = QUEST_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  
  actchan = 0;
  for(loop = 0; loop < nchannels; loop++)
  {
    printf("Enter the number of channel %d (0..N): ", loop);
    actchan = getdecd(actchan);
    isactive[actchan] = 1;
    ret = QUEST_Reset(actchan >> 2); // reset the card the channel is on
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return(-1);
    }

    ret = QUEST_LinkReset(actchan); // reset the link
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return(-1);
    }
    actchan++;
  }
    
  printf("Enter the ROD event size in words (max %d) : ", (BUFSIZE - 4 - NEVENTS * 0x400) / 4 / NEVENTS);
  size = getdecd(size);
  
  printf("Enter the number of fragments to send (-1=continous) : ");
  nfrag = getdecd(nfrag);
  
  printf("Data patterns:\n");
  printf("0 = 1, 2, 3\n");
  printf("1 = 0x00000000 - 0xffffffff\n");
  printf("2 = 0xa5a5a5a5 - 0x5a5a5a5a\n");
  printf("3 = cross talk test (ignores ROD event size)\n");
  printf("Select the data pattern : ");
  dpattern = getdecd(3);
 
  if (dpattern == 3)
    size = 270;

  //Align the buffers to a 1K boundary. Mostly for cosmetic reasons
  alisize = size * 4;
  if (alisize & 0x3ff)
    alisize = (alisize + 0x400) & ~0x3ff;
  
  for(loop = 0; loop < MAXQUESTROLS; loop++)
  {
    cfrags[loop] = nfrag;
    l1id[loop] = firstl1id;
  }
  
  printf("size = %d, alisize = %d\n", size, alisize);

  //Initialize the pointers to the events and the event data
  for(loop = 0; loop < NEVENTS; loop++)
  {
    u_int loop2, *data, word;
    u_int ndwords;    
    
    if (dpattern == 3)
    {
      word = 0;
      ptr[loop] = (u_int *) (virtbase + loop * alisize);
      data = (u_int *) (virtbase + loop * alisize);

      data[word++] = 0xee1234ee; //header marker 
      data[word++] = 0x00000009; //header size 
      data[word++] = 0x03010000; //format version 
      data[word++] = 0x00a00000; //source ID
      data[word++] = 0x00000001; //run number
      data[word++] = firstl1id;  //initial L1ID
      data[word++] = 0x00000000; //BCID 
      data[word++] = 0x00000007; //trigger type
      data[word++] = 0x0000000a; //detector event type
      data[word++] = 0x00000000; //status word 
      
      data[word++] = 0x12345678;
      
      data[word++] = 0x00000001; data[word++] = 0x00000001; data[word++] = 0xfffffffe; data[word++] = 0xfffffffe;  //Data words
      data[word++] = 0x00000001; data[word++] = 0x00000001; data[word++] = 0xfffffffe; data[word++] = 0xfffffffe;  //Data words
      data[word++] = 0x00000002; data[word++] = 0x00000002; data[word++] = 0xfffffffd; data[word++] = 0xfffffffd;  //Data words
      data[word++] = 0x00000002; data[word++] = 0x00000002; data[word++] = 0xfffffffd; data[word++] = 0xfffffffd;  //Data words
      data[word++] = 0x00000004; data[word++] = 0x00000004; data[word++] = 0xfffffffb; data[word++] = 0xfffffffb;  //Data words
      data[word++] = 0x00000004; data[word++] = 0x00000004; data[word++] = 0xfffffffb; data[word++] = 0xfffffffb;  //Data words
      data[word++] = 0x00000008; data[word++] = 0x00000008; data[word++] = 0xfffffff7; data[word++] = 0xfffffff7;  //Data words
      data[word++] = 0x00000008; data[word++] = 0x00000008; data[word++] = 0xfffffff7; data[word++] = 0xfffffff7;  //Data words

      data[word++] = 0x00000010; data[word++] = 0x00000010; data[word++] = 0xffffffef; data[word++] = 0xffffffef;  //Data words
      data[word++] = 0x00000010; data[word++] = 0x00000010; data[word++] = 0xffffffef; data[word++] = 0xffffffef;  //Data words
      data[word++] = 0x00000020; data[word++] = 0x00000020; data[word++] = 0xffffffdf; data[word++] = 0xffffffdf;  //Data words
      data[word++] = 0x00000020; data[word++] = 0x00000020; data[word++] = 0xffffffdf; data[word++] = 0xffffffdf;  //Data words
      data[word++] = 0x00000040; data[word++] = 0x00000040; data[word++] = 0xffffffbf; data[word++] = 0xffffffbf;  //Data words
      data[word++] = 0x00000040; data[word++] = 0x00000040; data[word++] = 0xffffffbf; data[word++] = 0xffffffbf;  //Data words
      data[word++] = 0x00000080; data[word++] = 0x00000080; data[word++] = 0xffffff7f; data[word++] = 0xffffff7f;  //Data words
      data[word++] = 0x00000080; data[word++] = 0x00000080; data[word++] = 0xffffff7f; data[word++] = 0xffffff7f;  //Data words

      data[word++] = 0x00000100; data[word++] = 0x00000100; data[word++] = 0xfffffeff; data[word++] = 0xfffffeff;  //Data words
      data[word++] = 0x00000100; data[word++] = 0x00000100; data[word++] = 0xfffffeff; data[word++] = 0xfffffeff;  //Data words
      data[word++] = 0x00000200; data[word++] = 0x00000200; data[word++] = 0xfffffdff; data[word++] = 0xfffffdff;  //Data words
      data[word++] = 0x00000200; data[word++] = 0x00000200; data[word++] = 0xfffffdff; data[word++] = 0xfffffdff;  //Data words
      data[word++] = 0x00000400; data[word++] = 0x00000400; data[word++] = 0xfffffbff; data[word++] = 0xfffffbff;  //Data words
      data[word++] = 0x00000400; data[word++] = 0x00000400; data[word++] = 0xfffffbff; data[word++] = 0xfffffbff;  //Data words
      data[word++] = 0x00000800; data[word++] = 0x00000800; data[word++] = 0xfffff7ff; data[word++] = 0xfffff7ff;  //Data words
      data[word++] = 0x00000800; data[word++] = 0x00000800; data[word++] = 0xfffff7ff; data[word++] = 0xfffff7ff;  //Data words

      data[word++] = 0x00001000; data[word++] = 0x00001000; data[word++] = 0xffffefff; data[word++] = 0xffffefff;  //Data words
      data[word++] = 0x00001000; data[word++] = 0x00001000; data[word++] = 0xffffefff; data[word++] = 0xffffefff;  //Data words
      data[word++] = 0x00002000; data[word++] = 0x00002000; data[word++] = 0xffffdfff; data[word++] = 0xffffdfff;  //Data words
      data[word++] = 0x00002000; data[word++] = 0x00002000; data[word++] = 0xffffdfff; data[word++] = 0xffffdfff;  //Data words
      data[word++] = 0x00004000; data[word++] = 0x00004000; data[word++] = 0xffffbfff; data[word++] = 0xffffbfff;  //Data words
      data[word++] = 0x00004000; data[word++] = 0x00004000; data[word++] = 0xffffbfff; data[word++] = 0xffffbfff;  //Data words
      data[word++] = 0x00008000; data[word++] = 0x00008000; data[word++] = 0xffff7fff; data[word++] = 0xffff7fff;  //Data words
      data[word++] = 0x00008000; data[word++] = 0x00008000; data[word++] = 0xffff7fff; data[word++] = 0xffff7fff;  //Data words

      data[word++] = 0x00010000; data[word++] = 0x00010000; data[word++] = 0xfffeffff; data[word++] = 0xfffeffff;  //Data words
      data[word++] = 0x00010000; data[word++] = 0x00010000; data[word++] = 0xfffeffff; data[word++] = 0xfffeffff;  //Data words
      data[word++] = 0x00020000; data[word++] = 0x00020000; data[word++] = 0xfffdffff; data[word++] = 0xfffdffff;  //Data words
      data[word++] = 0x00020000; data[word++] = 0x00020000; data[word++] = 0xfffdffff; data[word++] = 0xfffdffff;  //Data words
      data[word++] = 0x00040000; data[word++] = 0x00040000; data[word++] = 0xfffbffff; data[word++] = 0xfffbffff;  //Data words
      data[word++] = 0x00040000; data[word++] = 0x00040000; data[word++] = 0xfffbffff; data[word++] = 0xfffbffff;  //Data words
      data[word++] = 0x00080000; data[word++] = 0x00080000; data[word++] = 0xfff7ffff; data[word++] = 0xfff7ffff;  //Data words
      data[word++] = 0x00080000; data[word++] = 0x00080000; data[word++] = 0xfff7ffff; data[word++] = 0xfff7ffff;  //Data words

      data[word++] = 0x00100000; data[word++] = 0x00100000; data[word++] = 0xffefffff; data[word++] = 0xffefffff;  //Data words
      data[word++] = 0x00100000; data[word++] = 0x00100000; data[word++] = 0xffefffff; data[word++] = 0xffefffff;  //Data words
      data[word++] = 0x00200000; data[word++] = 0x00200000; data[word++] = 0xffdfffff; data[word++] = 0xffdfffff;  //Data words
      data[word++] = 0x00200000; data[word++] = 0x00200000; data[word++] = 0xffdfffff; data[word++] = 0xffdfffff;  //Data words
      data[word++] = 0x00400000; data[word++] = 0x00400000; data[word++] = 0xffbfffff; data[word++] = 0xffbfffff;  //Data words
      data[word++] = 0x00400000; data[word++] = 0x00400000; data[word++] = 0xffbfffff; data[word++] = 0xffbfffff;  //Data words
      data[word++] = 0x00800000; data[word++] = 0x00800000; data[word++] = 0xff7fffff; data[word++] = 0xff7fffff;  //Data words
      data[word++] = 0x00800000; data[word++] = 0x00800000; data[word++] = 0xff7fffff; data[word++] = 0xff7fffff;  //Data words

      data[word++] = 0x01000000; data[word++] = 0x01000000; data[word++] = 0xfeffffff; data[word++] = 0xfeffffff;  //Data words
      data[word++] = 0x01000000; data[word++] = 0x01000000; data[word++] = 0xfeffffff; data[word++] = 0xfeffffff;  //Data words
      data[word++] = 0x02000000; data[word++] = 0x02000000; data[word++] = 0xfdffffff; data[word++] = 0xfdffffff;  //Data words
      data[word++] = 0x02000000; data[word++] = 0x02000000; data[word++] = 0xfdffffff; data[word++] = 0xfdffffff;  //Data words
      data[word++] = 0x04000000; data[word++] = 0x04000000; data[word++] = 0xfbffffff; data[word++] = 0xfbffffff;  //Data words
      data[word++] = 0x04000000; data[word++] = 0x04000000; data[word++] = 0xfbffffff; data[word++] = 0xfbffffff;  //Data words
      data[word++] = 0x08000000; data[word++] = 0x08000000; data[word++] = 0xf7ffffff; data[word++] = 0xf7ffffff;  //Data words
      data[word++] = 0x08000000; data[word++] = 0x08000000; data[word++] = 0xf7ffffff; data[word++] = 0xf7ffffff;  //Data words

      data[word++] = 0x10000000; data[word++] = 0x10000000; data[word++] = 0xefffffff; data[word++] = 0xefffffff;  //Data words
      data[word++] = 0x10000000; data[word++] = 0x10000000; data[word++] = 0xefffffff; data[word++] = 0xefffffff;  //Data words
      data[word++] = 0x20000000; data[word++] = 0x20000000; data[word++] = 0xdfffffff; data[word++] = 0xdfffffff;  //Data words
      data[word++] = 0x20000000; data[word++] = 0x20000000; data[word++] = 0xdfffffff; data[word++] = 0xdfffffff;  //Data words
      data[word++] = 0x40000000; data[word++] = 0x40000000; data[word++] = 0xbfffffff; data[word++] = 0xbfffffff;  //Data words
      data[word++] = 0x40000000; data[word++] = 0x40000000; data[word++] = 0xbfffffff; data[word++] = 0xbfffffff;  //Data words
      data[word++] = 0x80000000; data[word++] = 0x80000000; data[word++] = 0x7fffffff; data[word++] = 0x7fffffff;  //Data words
      data[word++] = 0x80000000; data[word++] = 0x80000000; data[word++] = 0x7fffffff; data[word++] = 0x7fffffff;  //Data words

      data[word++] = 0x00000001;  //Number of status words  
      data[word++] = 257;         //Number of data words
      data[word++] = 0x00000000;  //Status word before data words

      ////cfrags[loop] = nfrag;
      ////l1id[loop] = firstl1id;
    }
    else
    {
      word = 0;
      ndwords = size - 13;
      ptr[loop] = (u_int *) (virtbase + loop * alisize);
      data = (u_int *) (virtbase + loop * alisize);

      data[word++] = 0xee1234ee; //header marker 
      data[word++] = 0x00000009; //header size 
      data[word++] = 0x03010000; //format version 
      data[word++] = 0x00a00000; //source ID
      data[word++] = 0x00000001; //run number
      data[word++] = firstl1id;  //initial L1ID
      data[word++] = 0x00000000; //BCID 
      data[word++] = 0x00000007; //trigger type
      data[word++] = 0x0000000a; //detector event type
      data[word++] = 0x00000000; //status word 

      for (loop2 = 0; loop2 < ndwords; loop2++)  
      {
	if (dpattern == 0) 
	  data[word++] = loop2;  //Data word
	else if (dpattern == 1) 
	{
          if(loop2 & 1)
	    data[word++] = 0;           //Data word
	  else
	    data[word++] = 0xffffffff;  //Data word
	}
	else 
	{
          if(loop2 & 1)
	    data[word++] = 0xa5a5a5a5;  //Data word
	  else
	    data[word++] = 0x5a5a5a5a;  //Data word
	}
      }

      data[word++] = 0x00000001;  //Number of status words  
      data[word++] = ndwords;     //Number of data words
      data[word++] = 0x00000000;  //Status word before data words

      ////cfrags[loop] = nfrag;
      ////l1id[loop] = firstl1id;
    }
  }

  fconfig.bswap = 0;
  fconfig.wswap = 0;
  fconfig.scw = 0xb0f00000;
  fconfig.ecw = 0xe0f00000; 

  ret = QUEST_Init(&fconfig);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  } 
  
  for (channel = 0; channel < MAXQUESTROLS; channel++)
  {
    if (isactive[channel])
    {
      fin[channel].nvalid = 1; 
      fin[channel].channel = channel; 
      fin[channel].size[0] = size;
      fin[channel].scw[0] = 1;
      fin[channel].ecw[0] = 1;
      delta[channel] = 0;
    }
  }

  //Just to know the number of free pages in idle state
  for (channel = 0; channel < MAXQUESTROLS; channel++)
  {
    if (isactive[channel])
    {
      ret = QUEST_InFree(channel, &freeref);
      if (ret)
      {
	rcc_error_print(stdout, ret);
	return(-1);
      }
      break;  //We just need a number form one channel
    }
  }

  cont = 1;
  alarm(2);
  printf("Press ctrl+\\ to stop this test\n");
  
  nextptr = 0; 
  u_int still_to_do;       
  while (cont)
  {  
    still_to_do = 0;
    
    //Wait until all events are out
    u_int all_ready = 0;
    while (all_ready == 0)
    {
      all_ready = 1;
      for (channel = 0; channel < MAXQUESTROLS; channel++)
      {
	if (isactive[channel] && cfrags[channel])
	{  
          ret = QUEST_InFree(channel, &nfree);
          if (ret)
          {
            rcc_error_print(stdout, ret);
            return(-1);
          }

	  //printf("channel = %d, free = %d, freeref = %d\n", channel, nfree, freeref);

	  if (nfree != freeref)
	    all_ready = 0;
	}
      } 
    }

    //Send the next group of events
    for (channel = 0; channel < MAXQUESTROLS; channel++)
    {
      if (isactive[channel] && cfrags[channel])
      {  
	ptr[nextptr][5] = l1id[channel]; //set the L1ID

	if (vmode)
	  printf("Sending L1ID = 0x%08x on channel %d with nextptr = %d and pci address = 0x%lx\n", l1id[channel], channel, nextptr, pcibase + nextptr * alisize);

        fin[channel].pciaddr[0] = pcibase + nextptr * alisize;   

        ret = QUEST_PagesIn(&fin[channel]);
	if (ret)
        {
          rcc_error_print(stdout, ret);
          return(-1);
        }

	l1id[channel]++;  

	if ((ecr_mode & (l1id[channel] & 0xffffff)) == ecr_gap)
	  l1id[channel] = (l1id[channel] & 0xff000000) + 0x01000000;

        delta[channel]++;
	cfrags[channel]--;
	nextptr++;
	if (nextptr == NEVENTS)
	  nextptr = 0;
	QUEST_Flush(channel);
	still_to_do += cfrags[channel];
	if (vmode)
          printf("cfrags[%d] = %d\n", channel, cfrags[channel]);
      }
      
    }
    if (!still_to_do)
      break;
  }
    
  alarm(0);
  ret = QUEST_Close();
  if (ret)
    rcc_error_print(stdout, ret);
  return(0);
}


/*****************/
int func_menu(void)
/*****************/
{
  static u_int bswap = 0, wswap = 0, scw = 0xb0f00000, ecw = 0xe0f00000;
  static u_int channel = 0, card = 0, nelem = 1, size = 0x100;
  int fun = 1;
  u_int loop, nfree;
  QUEST_ErrorCode_t ret;
  QUEST_config_t fconfig;
  QUEST_in_t fin;
  QUEST_status_t fstat;
  QUEST_info_t finfo;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function of the API:\n");
    printf("   1 QUEST_Open               2 QUEST_Close\n");
    printf("   3 QUEST_Init               4 QUEST_Info\n");
    printf("   5 QUEST_PagesIn            6 QUEST_InFree\n");
    printf("   7 QUEST_Reset              8 QUEST_LinkReset\n");
    printf("   9 QUEST_LinkStatus        10 QUEST_Flush\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun); 
     
    if (fun == 1)
    {
      ret = QUEST_Open();
      if (ret)
	rcc_error_print(stdout, ret);
    }
    
    if (fun == 2) 
    {
      ret = QUEST_Close();
      if (ret)
	rcc_error_print(stdout, ret);
    }    

    if (fun == 3) 
    {
      printf("Enable byte swapping (1=yes  0=no): ");
      bswap = getdecd(bswap);
            
      printf("Enable word swapping (1=yes  0=no): ");
      bswap = getdecd(wswap);
     
      printf("Enter the start control word: ");
      scw = gethexd(scw);

      printf("Enter the end control word: ");
      ecw = gethexd(ecw);
             
      fconfig.bswap = bswap;
      fconfig.wswap = wswap;
      fconfig.scw = scw;
      fconfig.ecw = ecw; 
            
      ret = QUEST_Init(&fconfig);
      if (ret)
	rcc_error_print(stdout, ret);
    }
    
    if (fun == 4)
    {
      ret = QUEST_Info(&finfo);  
      if (ret)
	rcc_error_print(stdout, ret);
      else
      {  
        printf("There are %d QUEST cards installed\n", finfo.ncards);
        for(loop = 0; loop < (finfo.ncards * 4); loop++)
          printf("Channel %d is %s\n", loop, finfo.channel[loop]?"present":"absent");
      }
    }
    
    if (fun == 5)
    {
      printf("Enter the number of the channel (0..%d, %d=all): ", MAXQUESTROLS -1, MAXQUESTROLS);
      channel = getdecd((int)channel);
      
      printf("Enter the number of entries to write to the IN FIFO: ");
      nelem = getdecd((int)nelem);
      
      printf("Enter the size of the block in words: ");
      size = getdecd((int)size);
      
      if (nelem > MAXQUESTINFIFO)
      {
        printf("You can not write more than %d entries\n", MAXQUESTINFIFO);
        return(-1);
      }
      
      fin.nvalid = nelem;      
      fin.channel = channel;
      for(loop = 0; loop < nelem; loop++)
      { 
        fin.pciaddr[loop] = pcibase;
        fin.size[loop] = size;
        fin.scw[loop] = 1;
        fin.ecw[loop] = 1;  
      }
                  
      if (channel < (MAXQUESTROLS))
      {
        if ((ret = QUEST_PagesIn(&fin)))
	  rcc_error_print(stdout, ret);
        if ((ret = QUEST_Flush(channel)))
	  rcc_error_print(stdout, ret);
      }
      else
      {
        for(channel = 0; channel < MAXQUESTROLS; channel++)
        {
          if (finfo.channel[channel])
          {
            fin.channel = channel;
	    if ((ret = QUEST_PagesIn(&fin)))
	      rcc_error_print(stdout, ret);
	    if (((channel & 3) == 3) && (ret = QUEST_Flush(channel)))
	      rcc_error_print(stdout, ret);
          }
        }
      }
    }
    
    if (fun == 6)
    {      
      printf("Which channel do you want to probe: ");
      channel = getdecd((int)channel);

      ret = QUEST_InFree(channel, &nfree);   
      if (ret)
	rcc_error_print(stdout, ret);
        
      printf("The IN FIFO of channel %d has %d free slots\n", channel, nfree);
    }
        
    if (fun == 7)
    {
      printf("Which card do you want to reset: ");
      card = getdecd((int)card);
      ret = QUEST_Reset(card);
      if (ret)
	rcc_error_print(stdout, ret);
    }  
    
    if (fun == 8) 
    {
      printf("Which channel do you want to reset: ");
      channel = getdecd((int)channel);
      
      ret = QUEST_LinkReset(channel);   
      if (ret)
	rcc_error_print(stdout, ret);        
      printf("Link has been reset\n");
    }
    
    if (fun == 9) 
    {
      printf("Which channel do you want to access: ");
      channel = getdecd((int)channel);
      
      ret = QUEST_LinkStatus(channel, &fstat);   
      if (ret)
	rcc_error_print(stdout, ret);        
      printf("The link is %s\n", (fstat.ldown)?"down":"up");
      printf("The S/W FIFO contains %d entries\n", fstat.infifo);
      printf("The REQ FIFO contains %d entries\n", fstat.reqfifo);
    }
    
    if (fun == 10) 
    {
      printf("Which channel do you want to access: ");
      channel = getdecd((int)channel);
      ret = QUEST_Flush(channel);
      if (ret)
	rcc_error_print(stdout, ret);        
      printf("Done.\n");
    }
    
  }
  printf("=========================================================================\n\n");
  return(0);
}


/******************/
int error_menu(void)
/******************/
{
  int channel;
  static u_int nchannels = 1, actchan, size;
  u_int incl1id = 1, fun = 11, l1id = 0, nfree, loop;
  QUEST_ErrorCode_t ret;
  QUEST_config_t fconfig;
  QUEST_in_t fin[MAXQUESTROLS];
  
  printf("\n=========================================================================\n");

  ret = QUEST_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  for (loop = 0; loop < MAXQUESTROLS; loop++)
    isactive[loop] = 0;

  printf("Enter the number of channels: ");
  nchannels = getdecd(nchannels);
  
  actchan = 0;
  for(loop = 0; loop < nchannels; loop++)
  {
    printf("Enter the number of channel %d (0..%d): ", loop, MAXQUESTROLS);
    actchan = getdecd(actchan);
    isactive[actchan] = 1;
    ret = QUEST_Reset(actchan >> 2); // reset the card the channel is on
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return(-1);
    }

    ret = QUEST_LinkReset(actchan); // reset the link
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return(-1);
    }
    actchan++;
  }
  
  fconfig.bswap = 0;
  fconfig.wswap = 0;
  fconfig.scw   = 0xb0f00000;
  fconfig.ecw   = 0xe0f00000; 

  ret = QUEST_Init(&fconfig);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  while (1)  
  {
    printf("\n");
    printf("Select a test:\n");
    printf("   1 Fragment without error or oversized fragment\n");
    printf("   2 Out of sequence fragment\n");
    printf("   3 ---->CAN THE QUEST DO THAT?<----No data between SCW and ECW\n");
    printf("   4 Data but no L1ID between SCW and ECW\n");
    printf("   5 L1ID but no complete ROD fragment between SCW and ECW\n");
    printf("   6 Incorrect header marker\n");
    printf("   7 Incorrect event format\n");
    printf("   8 Size mismatch\n");
    printf("   9 Non zero status word\n");
    printf("  10 Duplicated fragment\n");
    printf("  11 Fragment with ECR (not an error actually)\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun); 
       
    if (fun == 0)  
    {
      ret = QUEST_Close();
      if (ret)
      {
        rcc_error_print(stdout, ret);
        return(-1);
      }
  
      printf("=========================================================================\n\n");
      return(0);
    }

    if (fun == 1)  
    {
      printf("Enter the size of the fragment in words (min. 13)\n");
      size = getdecd(20);
      generate_fragment_1(virtbase, l1id, size);
      incl1id = 0;
    }
    
    if (fun == 2)
    {  
      l1id += 10;
      size = 64;
      generate_fragment_1(virtbase, l1id, size);
      incl1id = 0;
    }
    
    if (fun == 3)
    {  
      size = 0;
      incl1id = 0;
    }  
    
    if (fun == 4)  
    {
      generate_fragment_2(virtbase, &size);
      incl1id = 0;
    }  
    
    if (fun == 5)  
    {
      generate_fragment_3(virtbase, l1id, &size);
      incl1id = 0;
    }
    
    if (fun == 6)  
    {
      generate_fragment_4(virtbase, l1id, 0, &size);
      incl1id = 0;
    }
    
    if (fun == 7)  
    {
      generate_fragment_4(virtbase, l1id, 1, &size);
      incl1id = 0;
    }
    
    if (fun == 8)  
    {
      generate_fragment_4(virtbase, l1id, 2, &size);
      incl1id = 0;
    }
    
    if (fun == 9)  
    {
      generate_fragment_4(virtbase, l1id, 3, &size);
      incl1id = 0;
    }
  
    if (fun == 10)  
    {
      generate_fragment_1(virtbase, l1id - 1, 20);
      incl1id = 0;
    }
    
    if (fun == 11)
    {  
      l1id = (l1id & 0xff000000) + 0x01000000;
      printf("Enter the size of the fragment in words (min. 13)\n");
      size = getdecd(20);
      generate_fragment_1(virtbase, l1id, size);
      incl1id = 0;
    }

    for (channel = 0; channel < MAXQUESTROLS; channel++)
    {
     if (isactive[channel])
      {
	nfree = 0;
	while (!nfree) 
	{

          ret = QUEST_InFree(channel, &nfree);
          if (ret)
          {
            rcc_error_print(stdout, ret);
            return(-1);
          }
	}     
	fin[channel].nvalid     = 1; 
	fin[channel].channel    = channel; 
	fin[channel].pciaddr[0] = (u_int)pcibase;  //we use same page buffer all the time, as quest requires 32bit variable (and address has to be 32 bit, truncate here)
	fin[channel].size[0]    = size;
	fin[channel].scw[0]     = 1;
	fin[channel].ecw[0]     = 1; 

        ret = QUEST_PagesIn(&fin[channel]);
        if (ret)
        {
          rcc_error_print(stdout, ret);
          return(-1);
        }

	QUEST_Flush(channel);
      }
    }
    printf("The fragment has been sent. L1ID = %d\n", l1id);
    if (incl1id)
      l1id++;
  }
}


/**************/
int stress(void)
/**************/
{
  int channel;
  static u_int nchannels = 1, actchan, size;
  u_int evmax = 0, errate = 0, l1id = 0, pshift, nfree, loop;
  QUEST_ErrorCode_t ret;
  QUEST_config_t fconfig;
  QUEST_in_t fin[MAXQUESTROLS];
  
  printf("\n=========================================================================\n");

  ret = QUEST_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  for (loop = 0; loop < MAXQUESTROLS; loop++)
    isactive[loop] = 0;


  printf("Enter the number of fragments to send (0=indefinite): ");
  evmax = getdecd(evmax);

  printf("Enter the rate of erratic fragments: ");
  printf("Fragment is bad if ((l1id modulo rate) == (rate - 1)): ");
  errate = getdecd(errate);

  printf("Enter the number of channels: ");
  nchannels = getdecd(nchannels);
  
  actchan = 0;
  for(loop = 0; loop < nchannels; loop++)
  {
    printf("Enter the number of channel %d (0..%d): ", loop, MAXQUESTROLS);
    actchan = getdecd(actchan);
    isactive[actchan] = 1;
    ret = QUEST_Reset(actchan >> 2); // reset the card the channel is on
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return(-1);
    }

    ret = QUEST_LinkReset(actchan); // reset the link
    if (ret)
    {
      rcc_error_print(stdout, ret);
      return(-1);
    }
    actchan++;
  }
  
  fconfig.bswap = 0;
  fconfig.wswap = 0;
  fconfig.scw   = 0xb0f00000;
  fconfig.ecw   = 0xe0f00000; 

  ret = QUEST_Init(&fconfig);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  cont = 1;
  pshift = 0;
  printf("Press ctrl+\\ to stop this test\n");
  while(cont)  
  {
    if((l1id % errate) == (errate - 1))
    {
      printf("Bad fragment: L1ID = %d\n", l1id);
      generate_fragment_4(virtbase + pshift * 0x100, l1id, 1, &size); //bad fragment
    }
    else
    {
      //printf("Good fragment: L1ID = %d\n", l1id);
      size = 20;
      generate_fragment_1(virtbase + pshift * 0x100, l1id, size);     //good fragment
    }
       
    for (channel = 0; channel < MAXQUESTROLS; channel++)
    {
      if (isactive[channel])
      {
	nfree = 0;
	while (!nfree && cont) 
	{
          ret = QUEST_InFree(channel, &nfree);
          if (ret)
          {
            rcc_error_print(stdout, ret);
            return(-1);
          }
	}
	  
	if (!cont)
	  break;
	  
	//printf("Address = 0x%08x\n", pcibase + pshift * 0x100);
	//printf("Size    = %d\n", size);
	//printf("Channel = %d\n", channel);

	fin[channel].nvalid     = 1; 
	fin[channel].channel    = channel; 
	fin[channel].pciaddr[0] = pcibase + pshift * 0x100;  
	fin[channel].size[0]    = size;
	fin[channel].scw[0]     = 1;
	fin[channel].ecw[0]     = 1; 
	
        ret = QUEST_PagesIn(&fin[channel]);
        if (ret)
        {
          rcc_error_print(stdout, ret);
          return(-1);
        }
	QUEST_Flush(channel);	
      }
    }
    pshift++;
    if (pshift == 0x1000)
      pshift = 0;
    
    l1id++;
    if (evmax && evmax == l1id)
      cont = 0;
  }
  return(0);
}


/*********************************************************/
void generate_fragment_1(u_long ptr, u_int l1id, u_int size)
/*********************************************************/
{ 
  u_int *data, word = 0, loop, ndatawords;
  
  if(size < 13)
  {
    printf("Error in function generate_fragment_1. Parameter size is %d\n", size);
    exit(-1);
  }

  data = (u_int *) ptr;
  ndatawords = size - 13;    //header + trailer + status
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id;       //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000077; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  for (loop = 0; loop < ndatawords; loop++)
    data[word++] = loop;     //data word
  data[word++] = 0x00000001; //number of status elements
  data[word++] = ndatawords; //number of data elements
  data[word++] = 0x00000000; //status block position (before data)
}


/**********************************************/
void generate_fragment_2(u_long ptr, u_int *size)
/**********************************************/
{ 
  u_int *data, word = 0;

  data = (u_int *) ptr;
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  *size = word;
}


/**********************************************************/
void generate_fragment_3(u_long ptr, u_int l1id, u_int *size)
/**********************************************************/
{ 
  u_int *data, word = 0;

  data = (u_int *) ptr;
  data[word++] = 0xee1234ee; //header marker
  data[word++] = 0x00000009; //header size
  data[word++] = 0x03010000; //format version
  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id;       //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type
  data[word++] = 0x00000000; //status word
  *size = word;
}


/**********************************************************************/
void generate_fragment_4(u_long ptr, u_int l1id, u_int mode, u_int *size)
/**********************************************************************/
{ 
  u_int *data, word = 0;

  data = (u_int *) ptr;
  if (mode == 0)
    data[word++] = 0xee1111ee; //header marker
  else
    data[word++] = 0xee1234ee; //header marker

  data[word++] = 0x00000009; //header size
  
  if (mode == 1)
    data[word++] = 0x03000000; //format version
  else
    data[word++] = 0x03010000; //format version

  data[word++] = 0x00000011; //source ID
  data[word++] = 0x00000022; //run number 
  data[word++] = l1id;       //L1ID
  data[word++] = 0x00000033; //BCID
  data[word++] = 0x00000044; //trigger type
  data[word++] = 0x00000055; //detector event type

  if (mode == 3)  
    data[word++] = 0x10001000; //status word
  else
    data[word++] = 0x00000000; //status word

  data[word++] = 0x00000000; //data word
  data[word++] = 0x00000001; //data word
  data[word++] = 0x00000002; //data word
  data[word++] = 0x00000003; //data word
  data[word++] = 0x00000004; //data word
  data[word++] = 0x00000005; //data word
  data[word++] = 0x00000006; //data word
  data[word++] = 0x00000007; //data word
  data[word++] = 0x00000008; //data word
  data[word++] = 0x00000009; //data word
  data[word++] = 0x00000001; //number of status elements
  
  if (mode == 2)  
    data[word++] = 0x0000000b; //number of data elements
  else
    data[word++] = 0x0000000a; //number of data elements

  data[word++] = 0x00000000; //status block position (before data)
  *size = word;
}


/************************/
int controlword_menu(void)
/************************/
{
  static u_int nchannels, actchan;
  u_int channel, *dataptr, ecw_data, ecw_flag, scw_data, scw_flag, fun = 1, l1id = 0, nfree, loop;
  QUEST_ErrorCode_t ret;
  QUEST_config_t fconfig;
  QUEST_in_t fin[MAXQUESTROLS];
  u_int cnumbers[MAXQUESTROLS];
  
  printf("\n=========================================================================\n");

  nchannels = 0;
  for (loop = 0; loop < MAXQUESTROLS; loop++)
  {
    isactive[loop] = 0;
    cnumbers[loop] = 0;
  }
  
  ecw_data = 0xe0f00000;
  ecw_flag = 1;
  scw_data = 0xb0f00000;
  scw_flag = 1;

  ret = QUEST_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
	
  while (fun != 0)  
  {
    printf("\n");
    printf("   1 Select channels\n");
    printf("   2 Select control words\n");
    printf("   3 Send event(s)\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun); 
       
    if (fun == 1)  
    {              
      for (loop = 0; loop < MAXQUESTROLS; loop++)
      isactive[loop] = 0;

      printf("Enter the number of channels: ");
      nchannels = getdecd(nchannels);
      actchan = 0;
      for(loop = 0; loop < nchannels; loop++)
      {
	printf("Enter the number of channel %d (0..%d): ", loop, MAXQUESTROLS);
	cnumbers[loop] = getdecd(actchan);
	isactive[actchan] = 1;
	actchan++;
      }
    }
  
    if (fun == 2)  
    {  
      printf("Insert start contol word (1=yes, 0=no): ");
      scw_flag = getdecd(scw_flag);
      
      if(scw_flag)
      {
        printf("Enter start control word: ");
	scw_data = gethexd(scw_data);

      }  
      
      printf("Insert end contol word (1=yes, 0=no): ");
      ecw_flag = getdecd(ecw_flag);
      
      if(ecw_flag)
      {
        printf("Enter end control word: ");
        ecw_data = gethexd(ecw_data);
      }        
    }
    
    if (fun == 3)  
    {      
      if(nchannels == 0)
        printf("You did not yet execute menu item 1\n");
      else
      {
	for(loop = 0; loop < nchannels; loop++)
	{
	  ret = QUEST_Reset(cnumbers[loop] >> 2); // reset the card the channel is on
	  if (ret)
	  {
	    rcc_error_print(stdout, ret);
	    return(-1);
	  }

	  ret = QUEST_LinkReset(cnumbers[loop]); // reset the link
	  if (ret)
	  {
	    rcc_error_print(stdout, ret);
	    return(-1);
	  }
	}

	fconfig.bswap = 0;
	fconfig.wswap = 0;
	fconfig.scw   = scw_data;
	fconfig.ecw   = ecw_data; 

	ret = QUEST_Init(&fconfig);
	if (ret)
	{
	  rcc_error_print(stdout, ret);
	  return(-1);
	}
	
	dataptr = (u_int *) virtbase;
	dataptr[0]  = 0xee1234ee; //header marker
	dataptr[1]  = 0x00000009; //header size
	dataptr[2]  = 0x03010000; //format version
	dataptr[3]  = 0x00000011; //source ID
	dataptr[4]  = 0x00000022; //run number 
	dataptr[5]  = l1id++;     //L1ID
	dataptr[6]  = 0x00000033; //BCID
	dataptr[7]  = 0x00000044; //trigger type
	dataptr[8]  = 0x00000055; //detector event type
	dataptr[9]  = 0x00000000; //status word
	dataptr[10] = 0x00000001; //data word 1
	dataptr[11] = 0x00000002; //data word 2
	dataptr[12] = 0x00000003; //data word 3
	dataptr[13] = 0x00000004; //data word 4
	dataptr[14] = 0x00000005; //data word 5
	dataptr[15] = 0x00000001; //number of status elements
	dataptr[16] = 0x00000005; //number of data elements
	dataptr[17] = 0x00000000; //status block position (before data)	
	
	for (channel = 0; channel < nchannels; channel++)
	{
	  nfree = 0;
	  while (!nfree) 
	  {
            ret = QUEST_InFree(channel, &nfree);
            if (ret)
            {
              rcc_error_print(stdout, ret);
              return(-1);
            }
	  }

	  fin[channel].nvalid     = 1; 
	  fin[channel].channel    = channel; 
	  fin[channel].pciaddr[0] = pcibase;  //we use same page buffer all the time 
	  fin[channel].size[0]    = 18;
	  fin[channel].scw[0]     = scw_flag;
	  fin[channel].ecw[0]     = ecw_flag; 

          ret = QUEST_PagesIn(&fin[channel]);
          if (ret)
          {
            rcc_error_print(stdout, ret);
            return(-1);
          }
	  QUEST_Flush(channel);
        }
      }
    }
  }
  
  ret = QUEST_Close();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  printf("=========================================================================\n\n");
  return(0);
}





