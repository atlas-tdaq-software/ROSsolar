/****************************************************************/
/*                                                              */
/*  file: questscope.c                                          */
/*                                                              */
/* This program allows to access the resources of a FILAR       */
/* card with the QUEST F/W in a user friendly way   		*/
/*                                                              */
/*  Author: Markus Joos, CERN-EP                                */
/*                                                              */
/*  5. Dec. 03  MAJO  created                                   */
/*                                                              */
/****************C 2003 - A nickel program worth a dime**********/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "io_rcc/io_rcc.h"
#include "cmem_rcc/cmem_rcc.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"
#include <iostream>

/*prototypes*/
void SigQuitHandler(int signum);
int dumpmem(void);
int quest_map(int occ);
int setlat();
int quest_unmap(void);
int dumpconf(void);
int setocr(void);
int setpci(void);
int setim(void);    
int setbcw(void);
int setecw(void);    
int setic(void);
int uio_init(void);
int uio_exit(void);
int initbuff(int);
int cardreset(void);
int linkreset(void);
int mainhelp(void);
int questconf(void);
int stream_data_dma(void);
int setreqlen(void);
int setreqadd(void);
    
#define DEBUG 2
/*constants*/
#define BUFSIZE       0x400000       /*bytes*/
#define REQSIZE       0x400          /*bytes*/
#define PREFILL       0xfeedbabe
#define MAX_BURST_LEN 1
#define REQ_FIFO_FREE 48
#define DYNAMIC_THRESHOLD
//#define DEBUG         1

/*types*/
typedef unsigned int u_int;

typedef volatile u_int vu_int;

typedef struct {
  vu_int opctl;          /*0x000*/
  vu_int opstat;         /*0x004*/
  vu_int intmask;        /*0x008*/
  vu_int intctl;         /*0x00c*/
  vu_int blklen;         /*0x010*/
  vu_int reqadd;         /*0x014*/
  vu_int sctl;           /*0x018*/
  vu_int ectl;           /*0x01c*/
  vu_int fstat;          /*0x020*/
  vu_int estat;          /*0x024*/
} T_quest_regs;

/*globals*/
static u_int isactive = 0, cont, shandle, offset, pkts_sent = 0, delta = 0;
static int bhandle, reqhandle;
static u_long paddr, uaddr, sreg, uaddr_req, paddr_req;
static T_quest_regs *quest;
static u_int pcidefault[16]=
{
  0x001710dc, 0x00800000, 0x02800001, 0x0000ff00, 0xfffffc00, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x000001ff
};
static struct sigaction sa, sa2;


/*****************************/
void SigQuitHandler(int /*signum*/)
/*****************************/
{
  cont = 0;
  alarm(0);
}


/***************************/
void AlarmHandler(int /*signum*/)
/***************************/
{
  printf("Number of data blocks sent: ");
  pkts_sent += delta;
  printf("Total #=%d    # per second=%d\n", pkts_sent, delta/2);
  delta = 0;
  if (cont) 
    alarm(2);
}


/********************/
int quest_map(int occ)
/********************/
{
  unsigned int eret, pciaddr;

  eret = IO_Open();
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }

  eret = IO_PCIDeviceLink(0x10dc, 0x001b, occ, &shandle);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }  

  eret = IO_PCIConfigReadUInt(shandle, 0x10, &pciaddr);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 

  offset = pciaddr & 0xfff;
  pciaddr &= 0xfffff000;
  eret = IO_PCIMemMap(pciaddr, 0x1000, &sreg);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 

  quest = (T_quest_regs *)(sreg + offset); 
  return(0);
}


/*******************/
int quest_unmap(void)
/*******************/
{
  unsigned int eret;

  eret = IO_PCIMemUnmap(sreg, 0x1000);
  if (eret)
    rcc_error_print(stdout, eret);

  eret = IO_Close();
  if (eret)
    rcc_error_print(stdout, eret);
  return(0);
}


/**********/
int setlat()
/**********/
{
  unsigned int eret, latency, data;

  eret = IO_PCIConfigReadUInt(shandle, 0xC, &data);
  if (eret)
  {
    printf(" failed to read register\n");
    return(-1);
  }
  latency = (data >> 8) & 0xff;
  printf("Enter new value: ");
  latency = gethexd(latency);

  data &= 0xffff00ff;
  data |= (latency << 8);
  
  eret = IO_PCIConfigWriteUInt(shandle, 0xC, data);
  if (eret)
  {
    printf(" failed to write register\n");
    return(-1);
  }
  return(0);
}


/****************/
int dumpconf(void)
/****************/
{
  unsigned int loop, eret, data;

  printf("PCI configuration registers:\n\n");
  printf("Offset  |  content     |  Power-up default\n");
  for(loop = 0; loop < 0x40; loop += 4)
  {
    printf("   0x%02x |", loop);
    eret = IO_PCIConfigReadUInt(shandle, loop, &data);      
    if (eret)
      printf(" failed to read register\n");
    else
    {
      printf("   0x%08x |", data);
      if (data == pcidefault[loop >> 2])
        printf(" Yes\n");
      else
        printf(" No (0x%08x)\n", pcidefault[loop >> 2]);
    }
  }
  return(0);
}


/***************/
int dumpmem(void)
/***************/
{
  unsigned int data, d1, d2, d3, d4;
  
  printf("\n=============================================================\n");
  
  data = quest->opctl;
  printf("Operation Control register (0x%08x)\n", data);
  d1 = (data >> 17) & 0x1;
  d2 = (data >> 16) & 0x1;    
  printf("Channel 1: UTEST = %d   URESET = %d\n", d1, d2);
  d1 = (data >> 21) & 0x1;
  d2 = (data >> 20) & 0x1;    
  printf("Channel 2: UTEST = %d   URESET = %d\n", d1, d2);
  d1 = (data >> 25) & 0x1;
  d2 = (data >> 24) & 0x1;    
  printf("Channel 3: UTEST = %d   URESET = %d\n", d1, d2);
  d1 = (data >> 29) & 0x1;
  d2 = (data >> 28) & 0x1;    
  printf("Channel 4: UTEST = %d   URESET = %d\n", d1, d2);
  printf("Word swapping:     %s\n", (data & 0x4)?"Enabled":"Disabled");
  printf("Byte swapping:     %s\n", (data & 0x2)?"Enabled":"Disabled");
  printf("Reset interface:   %s\n", (data & 0x1)?"Active":"Not active");

  data = quest->opstat;
  printf("\nOperation Status register (0x%08x)\n", data);
  d1 = (data >> 10) & 0x1;
  d2 = (data >> 6) & 0xf;
  d3 = (data >> 5) & 0x1;
  d4 = (data >> 4) & 0x1;
  printf("Channel 1: Present = %s   LRL = 0x%1x   LFF = %s   LDOWN = %s\n", d1?"No ":"Yes ", d2, d3?"Yes":"No ", d4?"Yes":"No ");
  d1 = (data >> 17) & 0x1;
  d2 = (data >> 13) & 0xf;
  d3 = (data >> 12) & 0x1;
  d4 = (data >> 11) & 0x1;
  printf("Channel 2: Present = %s   LRL = 0x%1x   LFF = %s   LDOWN = %s\n", d1?"No ":"Yes ", d2, d3?"Yes":"No ", d4?"Yes":"No ");
  d1 = (data >> 24) & 0x1;
  d2 = (data >> 20) & 0xf;
  d3 = (data >> 19) & 0x1;
  d4 = (data >> 18) & 0x1;
  printf("Channel 3: Present = %s   LRL = 0x%1x   LFF = %s   LDOWN = %s\n", d1?"No ":"Yes ", d2, d3?"Yes":"No ", d4?"Yes":"No ");
  d1 = (data >> 31) & 0x1;
  d2 = (data >> 27) & 0xf;
  d3 = (data >> 26) & 0x1;
  d4 = (data >> 25) & 0x1;
  printf("Channel 4: Present = %s   LRL = 0x%1x   LFF = %s   LDOWN = %s\n", d1?"No ":"Yes ", d2, d3?"Yes":"No ", d4?"Yes":"No ");
  printf("REQBLK_DONE:   %s\n", (data & 0x4)?"Yes":"No");
  printf("DONE_SPACE:    %s\n", (data & 0x1)?"Yes":"No");

  data = quest->intmask;
  printf("\nInterupt mask register (0x%08x)\n", data);
  d1 = (data >> 5) & 0x1;
  d2 = (data >> 4) & 0x1;
  printf("Channel 1: LFF = %s   LDOWN = %s\n", d1?"Enabled":"Disabled", d2?"Enabled":"Disabled");
  d1 = (data >> 12) & 0x1;
  d2 = (data >> 11) & 0x1;
  printf("Channel 2: LFF = %s   LDOWN = %s\n", d1?"Enabled":"Disabled", d2?"Enabled":"Disabled");
  d1 = (data >> 19) & 0x1;
  d2 = (data >> 18) & 0x1;
  printf("Channel 3: LFF = %s   LDOWN = %s\n", d1?"Enabled":"Disabled", d2?"Enabled":"Disabled");
  d1 = (data >> 26) & 0x1;
  d2 = (data >> 25) & 0x1;
  printf("Channel 4: LFF = %s   LDOWN = %s\n", d1?"Enabled":"Disabled", d2?"Enabled":"Disabled");
  printf("REQBLK_DONE: %s\n", (data & 0x4)?"Enabled":"Disabled");
  printf("DONE_SPACE:  %s\n", (data & 0x1)?"Enabled":"Disabled");

  data = quest->intctl;
  printf("\nInterrupt control (0x%08x)\n", data);
  printf("Channel 1: interrupt threshold = %d free slots\n", (data >> 8) & 0x3f); 
  printf("Channel 2: interrupt threshold = %d free slots\n", (data >> 14) & 0x3f); 
  printf("Channel 3: interrupt threshold = %d free slots\n", (data >> 20) & 0x3f); 
  printf("Channel 4: interrupt threshold = %d free slots\n", (data >> 26) & 0x3f);

  data = quest->blklen;
  printf("\nRequest length      = 0x%08x\n", data);

  data = quest->reqadd;
  printf("Request address     = 0x%08x\n", data);  
 
  data = quest->sctl;
  printf("\nStart control word  = 0x%08x\n", data);  

  data = quest->ectl;
  printf("End control word    = 0x%08x\n", data);
  
  data = quest->fstat;
  printf("\nFIFO status (0x%08x)\n", data);
  printf("Channel 1 has space for %d entries\n", data & 0xff);
  printf("Channel 2 has space for %d entries\n", (data >> 8) & 0xff);
  printf("Channel 3 has space for %d entries\n", (data >> 16) & 0xff);
  printf("Channel 4 has space for %d entries\n", (data >> 24) & 0xff);
  
  data = quest->estat;
  printf("\nCard temperature      = %d deg. C\n", data & 0xff);

  printf("=============================================================\n");
  return(0);
}


/**************/
int setreg(void)
/**************/
{
  int fun = 1;

  printf("\n=========================================\n");
  while(fun != 0)
    {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 PCI config. register         2 Operation Control register\n"); 
    printf("   3 Block length                 4 Request address\n");
    printf("   5 Begin Control Word register  6 End Control Word register\n");
    printf("   7 Interrupt Mask register      8 Interrupt control\n");
    printf("   9 Request FIFO\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun=getdecd(fun);
    if (fun == 1) setpci();
    if (fun == 2) setocr();
    if (fun == 3) setreqlen();
    if (fun == 4) setreqadd();
    if (fun == 5) setbcw();
    if (fun == 6) setecw();
    if (fun == 7) setim();
    if (fun == 8) setic();
    }
  printf("=========================================\n\n");
  return(0);
}


/**************/
int setpci(void)
/**************/
{
  unsigned int eret, offset=0, data;

  printf("Enter the offset of the register (4-byte alligned) ");
  offset = gethexd(offset);
  offset &= 0x3c;

  eret = IO_PCIConfigReadUInt(shandle, offset, &data);      
  if (eret)
  {
    printf(" failed to read register\n");
    return(1);
  }
  printf("Enter new value for this register ");
  data = gethexd(data);

  eret = IO_PCIConfigWriteUInt(shandle, offset, data);      
  if (eret)
  {
    printf(" failed to write register\n");
    return(2);
  }

  return(0);
}


/**************/
int setocr(void)
/**************/
{
  int data;

  data = quest->opctl;
  printf("Enter the new value for the Operation Control register ");
  data = gethexd(data);
  quest->opctl = data;
  return(0);
}


/*****************/
int setreqlen(void)
/*****************/
{
  int data;

  data = quest->blklen;
  printf("Enter the new value for the Block Length register ");
  data = gethexd(data);
  quest->blklen = data;
  return(0);
}


/*****************/
int setreqadd(void)
/*****************/
{
  int data;

  data = quest->reqadd;
  printf("Enter the new value for the Request Address register ");
  data = gethexd(data);
  quest->reqadd = data;
  return(0);
}


/**************/
int setbcw(void)
/**************/
{
  int data;

  data = quest->sctl;
  printf("Enter the new value for the Start Control Word register ");
  data = gethexd(data);
  quest->sctl = data;
  return(0);
}


/**************/
int setecw(void)
/**************/
{
  int data;

  data = quest->ectl;
  printf("Enter the new value for the End Control Word register ");
  data = gethexd(data);
  quest->ectl = data;
  return(0);
}


/*************/
int setim(void)
/*************/
{
  int data;

  data = quest->intmask;
  printf("Enter the new value for the Interrupt Mask register ");
  data = gethexd(data);
  quest->intmask = data;
  return(0);
}


/**************/
int setic(void)
/**************/
{
  int data;

  data = quest->intctl;
  printf("Enter the new value for the Interrupt Control register ");
  data = gethexd(data);
  quest->intctl = data;
  return(0);
}


/****************/
int uio_init(void)
/****************/
{
  unsigned int *ptr, loop, eret;

  eret = CMEM_Open();
  if (eret)
  {
    printf("Sorry. Failed to open the cmem_rcc library\n");
    rcc_error_print(stdout, eret);
    exit(6);
  }

  eret = CMEM_BPASegmentAllocate(BUFSIZE, (char *)"quest", &bhandle);
  if (eret)
  {
    printf("Sorry. Failed to allocate buffer\n");
    rcc_error_print(stdout, eret);
    exit(7);
  }

  eret = CMEM_SegmentVirtualAddress(bhandle, &uaddr);
  if (eret)
  {
    printf("Sorry. Failed to get virtual address for buffer\n");
    rcc_error_print(stdout, eret);
    exit(8);
  }

  eret = CMEM_SegmentPhysicalAddress(bhandle, &paddr);
  if (eret)
  {
    printf("Sorry. Failed to get physical address for buffer\n");
    rcc_error_print(stdout, eret);
    exit(9);
  }
  
  /*initialise the buffer*/
  ptr = (unsigned int *)uaddr;
  for (loop = 0; loop < (BUFSIZE >> 2); loop++)
    *ptr++ = PREFILL;
    
  eret = CMEM_BPASegmentAllocate(REQSIZE, (char *)"quest2", &reqhandle);
  if (eret)
  {
    printf("Sorry. Failed to allocate buffer\n");
    rcc_error_print(stdout, eret);
    exit(10);
  }

  eret = CMEM_SegmentVirtualAddress(reqhandle, &uaddr_req);
  if (eret)
  {
    printf("Sorry. Failed to get virtual address for buffer\n");
    rcc_error_print(stdout, eret);
    exit(11);
  }

  eret = CMEM_SegmentPhysicalAddress(reqhandle, &paddr_req);
  if (eret)
  {
    printf("Sorry. Failed to get physical address for buffer\n");
    rcc_error_print(stdout, eret);
    exit(12);
  }  
  
 return(0);
}


/****************/
int uio_exit(void)
/****************/
{
  unsigned int eret;

  eret = CMEM_BPASegmentFree(bhandle);
  if (eret)
  {
    printf("Warning: Failed to free buffer\n");
    rcc_error_print(stdout, eret);
  }
  
  eret = CMEM_BPASegmentFree(reqhandle);
  if (eret)
  {
    printf("Warning: Failed to free buffer\n");
    rcc_error_print(stdout, eret);
  }

  eret = CMEM_Close();
  if (eret)
  {
    printf("Warning: Failed to close the CMEM_RCC library\n");
    rcc_error_print(stdout, eret);
  }
  return(0);
}


/****************/
int initbuff(int length)
/****************/
{
  static u_int pattern = 0x12345678;
  static int mode = 1;
  u_int data, *ptr;
  int loop;

  printf("\n=========================================\n");

  printf("Select the data pattern:\n");
  printf("1 = walking 1      2 = walking 0\n");
  printf("3 = FFFF/0000      4 = AAAA/5555\n");
  printf("5 = ROD fragment   6 = fixed pattern\n");
  printf("Your choice: ");
  mode = getdecd(mode);
  
  data = 0;
  ptr = (unsigned int *)uaddr;
  switch (mode) {
  case 1:
  case 2:
    data = 1;
    break;
  case 3: 
    data = 0xFFFFFFFF;
    break;
  case 4: 
    data = 0xAAAAAAAA;
    break;
  case 5: 
    break;
  case 6: 
    printf("Enter the constant pattern: ");
    pattern = gethexd(pattern);
    data = pattern;
    break;
  default:
    return(-1);
  }
  if (mode != 5) {
    for (loop = 0; loop < (BUFSIZE >> 2); loop++) {
      if (mode == 2) *ptr++ = ~data; // walking zero
      else *ptr++ = data;
      if (mode == 3 || mode == 4) // alternating patterns
	data = ~data;
      if (mode == 1 || mode == 2) {
	data <<= 1;
	if (!data) data = 1;
      }
    }
  } else { // ROD fragment
    if (length < 12) length = 12;
    *ptr++ = 0xEE1234EE; // ROD header marker
    *ptr++ = 8;          // header length in 32-bit words
    *ptr++ = 0x03010000; // format version number
    *ptr++ = 0;          // source identifier
    *ptr++ = 0;          // level 1 ID
    *ptr++ = 0;          // bunch crossing ID
    *ptr++ = 0;          // trigger type
    *ptr++ = 0;          // detector event type
    data = 0;
    for (loop = 12; loop < length; loop++)
      *ptr++ = data++;
    *ptr++ = 0;          // status element
    *ptr++ = 1;          // number of status elements
    *ptr++ = length-12;  // number of data elements
    *ptr++ = 1;          // status block position (after data)
  }
  printf("=========================================\n\n");
  return(mode);
}


/*****************/
int cardreset(void)
/*****************/
{
  unsigned int data;

  data = quest->opctl;
  data |= 0x1;
  quest->opctl = data;
  sleep(1);
  data &= 0xfffffffe;
  quest->opctl = data;
  isactive = 0;
  return(0);
}


/*****************/
int linkreset(void)
/*****************/
{
  static unsigned int channel = 1;
  unsigned int status;

  printf("Enter the number of the channel (1..4, 5 = all): ");
  channel = getdecd(channel);
  
  if (channel == 1 || channel == 5)
  { 
    /* clear the URESET bit to make sure that there is a falling edge on URESET_N */
    quest->opctl &= 0xfffeffff;
    /*set the URESET bits*/
    quest->opctl |= 0x00010000;
    ts_delay(20000); /* wait for 20 ms to give the link time to come up */
    /*now wait for LDOWN to come up again*/
    printf("Waiting for link to come up...\n");
    while((status = quest->opstat) & 0x10)
      printf("quest->opstat = 0x%08x\n", status);
    /*reset the URESET bits*/
    quest->opctl &= 0xfffeffff;
  }
  
  if (channel == 2 || channel == 5)
  { 
    quest->opctl &= 0xffefffff;
    quest->opctl |= 0x00100000;
    ts_delay(20000); /* wait for 20 ms to give the link time to come up */
    printf("Waiting for link to come up...\n");
    while((status = quest->opstat) & 0x800)
      printf("quest->opstat = 0x%08x\n", status);
    quest->opctl &= 0xffefffff;
  } 
  
  if (channel == 3 || channel == 5)
  { 
    quest->opctl &= 0xfeffffff;
    quest->opctl |= 0x01000000;
    ts_delay(20000); /* wait for 20 ms to give the link time to come up */
    printf("Waiting for link to come up...\n");
    while((status = quest->opstat) & 0x40000)
      printf("quest->opstat = 0x%08x\n", status);
    quest->opctl &= 0xfeffffff;
  }  
  
  if (channel == 4 || channel == 5)
  { 
    quest->opctl &= 0xefffffff;
    quest->opctl |= 0x10000000;
    ts_delay(20000); /* wait for 20 ms to give the link time to come up */
    printf("Waiting for link to come up...\n");
    while((status = quest->opstat) & 0x1000000)
      printf("quest->opstat = 0x%08x\n", status);
    quest->opctl &= 0xefffffff;
  }  

  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("Contact Markus Joos, 72364, 160663, markus.joos@cern.ch if you need help\n");
  return(0);
}


/*****************/
int questconf(void)
/*****************/
{
  static unsigned int bd = 0, ds = 0, swapw = 0, swapb = 0, lffi = 0, ldi = 0;
  static unsigned int bcw = 0xb0f00000, ecw = 0xe0f00000;
  static unsigned int threshold = 62;
  unsigned int data;
  
  printf("=============================================================\n");
  
  printf("Enable word swapping (1=yes  0=no): ");
  swapw = getdecd(swapw);
  
  printf("Enable byte swapping (1=yes  0=no): ");  
  swapb = getdecd(swapb);

  printf("Enable the LFF interrupt: ");
  lffi = getdecd(lffi);

  printf("Enable the LDOWN interrupt: ");
  ldi = getdecd(ldi);

  printf("Enable the REQBLK_DONE interrupt: ");
  bd = getdecd(bd);
  
  printf("Enable the DONE_SPACE interrupt: ");
  ds = getdecd(ds);
  
  printf("Enter the Begin Control Word: ");
  bcw = gethexd(bcw);

  printf("Enter the End Control Word: ");
  ecw = gethexd(ecw);
  
  printf("Enter the FIFO threshold: ");
  threshold = getdecd(threshold);

  quest->opctl = (swapw << 2) + (swapb << 1);
  data = 0;
  if (lffi)
    data |= 0x02081020;
  if (ldi)
    data |= 0x01040810;
  quest->intmask = data | (bd << 2) | ds;
  quest->sctl = bcw & 0xfffffffc;
  quest->ectl = ecw & 0xfffffffc;

  data = 0;
  data |= (threshold & 0x3f) << 8;
  data |= (threshold & 0x3f) << 14;
  data |= (threshold & 0x3f) << 20;
  data |= (threshold & 0x3f) << 26;
  quest->intctl = data;

  printf("\n=============================================================\n");
  return(0);
}

#define FIFO_STAT_COUNT 16

/***********************/
int stream_data_dma(void)
/***********************/
{
  static int channel[4] = { 1, 0, 0, 0 };
  static int no_pkts = 0, ib = 1, ie = 1, size = 256;
  u_int data, lastdata, intctl;
  int chan, rmode, index = 0;
  int tosend[4];
  u_int fifostat[FIFO_STAT_COUNT];

  for (chan = 0; chan < 4; chan++)
  {  
    printf("Enable channel %d  (1=yes  0=no): ", chan);
    channel[chan] = getdecd(channel[chan]);
  }    
    
  printf("Insert Begin Control Word: ");
  ib = getdecd(ib);
  
  printf("Insert End Control Word: ");
  ie = getdecd(ie);

  printf("Enter the block length (32 bit words): ");
  size = getdecd(size);
  if (size & 0xfff00000)
  {
    printf("Size is to big. Reduced to 0xfffff words\n");
    size = 0xfffff;
  }

  initbuff(size); // initialize the data pattern/

  printf("How many packets do you want to send? (0 = run forever): ");
  no_pkts = getdecd(no_pkts);
  rmode = (!no_pkts) ? 1 : 0;
    
  for (chan = 0; chan < 4; chan++)
    if (channel[chan]) 
      tosend[chan] = (rmode) ? 64 : no_pkts;
    else 
      tosend[chan] = 0;

#ifdef DYNAMIC_THRESHOLD
  intctl = 0;
  for (chan = 0; chan < 4; chan++) 
  {
    int threshold = 63 - tosend[chan];
    if (threshold < 56) threshold = 56;
    if (threshold > 62) threshold = 62;
    intctl |= (threshold & 0x3f) << (8 + 6 * chan);
  }
  intctl |= 5; // acknowledge any outstanding done bits
#else
  intctl = 0xFBEFBE05; // threshold fixed at 62 for all channels
#endif
  quest->intctl = intctl;
  printf("INTCTL = %08X\n", quest->intctl);

  cont = 1;
  delta = 0; // global variable

  if (rmode) 
  {
    printf("Running! Press <ctrl+\\> when finished\n");
    alarm(2);
  }

  while (cont) 
  {
    int i; 
    int req_blk_len = 2; // two 64-bit words for the entry count for 4 channels
    u_int *ptr = (u_int *)uaddr_req;
    u_int fstat = quest->fstat;
    intctl = 0;
    fifostat[index] = fstat;
    index++;
    index &= FIFO_STAT_COUNT-1;
    
    // Build the request block
    for (chan = 0; chan < 4; chan++) 
    {
      int threshold = 62;
      int fifo_free = fstat & 0xff;
      if (tosend[chan]) 
      {
	int n_req;

	n_req = (fifo_free < tosend[chan]) ? fifo_free : tosend[chan];
#ifdef DEBUG
	printf("FIFO channel %d has %d free entries, %d to send, %d requested\n", chan, fifo_free, tosend[chan], n_req);
#endif
	*ptr++ = n_req << 1; // number of entries (32-bit words) in the request block 
	for (i = 0; i < n_req; i++) 
	{
	  *ptr++ = paddr; // address of the data block
	  *ptr++ = (ib << 31) + (ie << 30) + size; // block size and control bits
	}
	req_blk_len += n_req; // Request lenght is in 64-bit words
        if (!rmode) tosend[chan] -= n_req;    
	delta += n_req;
#ifdef DYNAMIC_THRESHOLD
	threshold = 63 - tosend[chan];
	if (threshold < 56) threshold = 56;
	if (threshold > 62) threshold = 62;
#endif
      } else 
	*ptr++ = 0;
      fstat >>= 8;
      intctl |= (threshold & 0x3f) << (8 + 6 * chan);
    }

    if (req_blk_len > 2) 
    { // anything to send
#if DEBUG > 1
      // Print the request block
      ptr = (unsigned int *)uaddr_req;
      for (i = 0; i < 2*req_blk_len; i++) 
      {
	printf("%d %lX: %08X\n", i, (u_long)ptr, *ptr);
	ptr++;
      }
      // Start the REQ block DMA
      printf("REQ block DMA address = %lX, number of 64-bit words = %d\n", paddr_req, req_blk_len);
#endif
      quest->blklen = req_blk_len;
      quest->reqadd = paddr_req;

      // Wait until the request block has been read
      //while (((data = quest->opstat) & 0x4) == 0);
      //printf("FSTAT = %08X\n", quest->fstat);
     
      //for (i = 0; i < 16; i++)
      //fifostat[i] = quest->fstat;

      // Wait until the request block has been read
      lastdata = 0;
      while (((data = quest->opstat) & 0x1) == 0) 
      {
#ifdef DEBUG
	if (data != lastdata) 
	{
	  printf("FSTAT = %08X\n", quest->fstat);
	  printf("OPSTAT = %08X\n", data);
	  lastdata = data;
	}
#endif
      }
      ts_delay(10); /* wait for 10 us to emultate the interrupt latency */
#ifdef DEBUG
      printf("OPSTAT = %08X\n", quest->opstat);
      printf("FSTAT = %08X\n", quest->fstat);
#endif
      // Clear the REQ_BLK_DONE bit in the OPSTAT register
      //      quest->intctl |= 0x5; // REQ_BLK_ACK
      quest->intctl = intctl | 5; // REQ_BLK_ACK
#ifdef DEBUG
      printf("OPSTAT = %08X\n", quest->opstat);
      printf("FSTAT = %08X\n", quest->fstat);
      printf("INTCTL = %08X\n", quest->intctl);
#endif
    }

    // Check if all packets have been sent
    if (!rmode && (tosend[0] + tosend[1] + tosend[2] + tosend[3] == 0))
      break;
  }
  // Wait until all posted packets have been sent
  lastdata = 0;
  while ((data = quest->fstat) != 0x3f3f3f3f) 
  {
#ifdef DEBUG
    if (data != lastdata) printf("FSTAT = %08X\n", data);
    lastdata = data;
#endif
  }
  alarm(0);
  printf("%d packets sent.\n", (rmode) ? pkts_sent : delta);
  for (index = 0; index < FIFO_STAT_COUNT; index++)
    printf("FSTAT[%d] = %08X\n", index, fifostat[index]);
  return(0);
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  static int ret, fun = 1, occ = 1;
  static u_int data;

  if ((argc == 2) && (sscanf(argv[1], "%d", &occ) == 1)) {argc--;} else {occ = 1;}
  if (argc != 1)
  {
    printf("This is QUESTSCOPE.\n\n");
    printf("Usage: questscope [QUEST occurrence]\n");
    exit(0);
  }

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler;
  ret = sigaction(SIGQUIT, &sa, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = AlarmHandler;
  ret = sigaction(SIGALRM, &sa2, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  ts_open(1, TS_DUMMY);
  uio_init();
  quest_map(occ);

  ret = IO_PCIConfigReadUInt(shandle, 0x8, &data);
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }


  printf("\n\n\nThis is QUESTSCOPE running on a card of revision %02x\n", data & 0xff);
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("  1 Print help             2 Dump PCI conf. space  3 Dump PCI MEM registers\n");
    printf("  4 Write to a register    5 Configure QUEST       6 Reset the QUEST\n");
    printf("  7 Reset the S-Link       8 Initialize the buffer 9 Set latency counter\n");
    printf(" 10 Stream S-LINK data\n");
    printf("  0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) dumpconf();
    if (fun == 3) dumpmem();
    if (fun == 4) setreg();
    if (fun == 5) questconf();
    if (fun == 6) cardreset();
    if (fun == 7) linkreset();
    if (fun == 8) initbuff(BUFSIZE >> 2);
    if (fun == 9) setlat();
    if (fun == 10) stream_data_dma();
  }

  quest_unmap();
  uio_exit();
  ts_close(TS_DUMMY);
  exit(0);
}


