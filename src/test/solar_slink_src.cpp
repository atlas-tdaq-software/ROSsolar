/*****************************************************************************/
/*                                                                           */
/* File Name        : solar_slink_src.cpp                                    */
/*                                                                           */
/* Author           : Markus Joos			                     */
/*                                                                           */
/***** C 2003 - A nickel program worth a dime ********************************/

#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include "rcc_error/rcc_error.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSsolar/solar.h"
#include "ROSMemoryPool/MemoryPage.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSEventFragment/RODFragment.h"
#include "ROSEventFragment/ROBFragment.h"
#include "DFDebug/DFDebug.h"

#include <cmdl/cmdargs.h>
#include <tmgr/tmresult.h>

using namespace ROS;
using namespace daq::tmgr;

#ifndef TRUE
  #define TRUE                  0x01
#endif

#ifndef FALSE
  #define FALSE                 0x00
#endif
   
// Standard header and trailer sizes
#define HEADER_SIZE_WRDS        (sizeof(RODFragment::RODHeader) / 4)
#define TRAILER_SIZE_WRDS       (sizeof(RODFragment::RODTrailer) / 4)
#define STATUS_SIZE             1
#define STATUS_SIZE_SLIDAS      3

// Minimum packet size is: header + trailer words + at least 4 words
#define MIN_PACKET_SIZE_WRDS    (HEADER_SIZE_WRDS + TRAILER_SIZE_WRDS + 4)  
#define MAX_PACKET_SIZE_WRDS    1024 

//#define ROB_POOL_NUMB           10                           //Max. number of ROD fragments
#define ROB_POOL_SIZE           (MAX_PACKET_SIZE_WRDS * 4)   //Max. size of ROD fragments


// Prototypes
void wait_a_bit(int ipackets);
int gen_rod_frag(u_int ipackets, u_int *pci_addr, u_int *f_size); 


// Global variables
int print = FALSE;
int size = 256;
int maxsize = MAX_PACKET_SIZE_WRDS;
int minsize = MIN_PACKET_SIZE_WRDS;
int npackets = -1;
int occurence = 1;	    
int flg_random = FALSE;
int flg_rodemul = TRUE;
int flg_size = FALSE;
int flg_minsize = FALSE;
int flg_maxsize = FALSE;
int flg_verbose = FALSE;
int skipevent = 0;
int first_l1id = 0;	
int dblevel = 0;
int dbpackage = DFDB_ROSSOLAR;
int ipackets, level1Id;
unsigned int wait = 0;
unsigned int freq = 0;
unsigned int module_id = 0;
unsigned int detector_id = 0xa0;
unsigned int l1_type = 0x7;
unsigned int det_type = 0xa; 
unsigned int sourceId;
SOLAR_config_t fconfig;
SOLAR_in_t fin;
SOLAR_status_t fstat;
tstamp ts1, ts2;
float elapsed;
MemoryPool *rob_pool; 
// In order to reach a high fragment output rate the generation and the sending of fragments 
// has to happen in parallel. Therefore we will typically have several fragments in memory at any time
// This raises one question: How do we know when a fragment has been send? Potentially it may stay in the Q
// of the SOLAR (H/W or driver) for any amount of time.
// One solution wolud be an acknowledgement from the driver. This, however is slow and would require new
// functions in the library. It seems simpler to use a number of robfragment objects in a cyclic manner:
// old ones are overwritten by new ones. This should work as long as the number of objects is larger than the 
// depth of the FIFOs in the SOLAR. Therefore: 
unsigned int fragindex = 0, maxfrags = (MAXINFIFO + MAXREQFIFO + 10);
ROBFragment *robfragment[MAXINFIFO + MAXREQFIFO + 10];
unsigned int fragused[MAXINFIFO + MAXREQFIFO + 10];

/********************/
int terminate_it(void) 
/********************/
{
  err_type code, timeout = 0;

  //Wait until all packets have been sent
  do 
  {
    code = SOLAR_LinkStatus(occurence - 1, &fstat);
    if (code) 
    {
      rcc_error_print(stdout, code);
      exit(TmFail);
    }
    if (flg_verbose)
      std::cout << "terminate_it: waiting for SOLAR to process posted fragments" << std::endl;
    if (timeout++ == 100000)
      break;      //Just in case.... 
  } while (fstat.infifo || fstat.reqfifo);

  code = SOLAR_Close();
  if (code)
  {
    std::cout << "main: SOLAR_Close() FAILED with error " << std::hex << code << std::endl;
    rcc_error_print(stdout, code);
    exit(TmFail);
  }
  exit(TmPass);
}


/******************************/
void sigquit_handler(int signum)
/******************************/
{
  int code;

  std::cout << "ctrl-\\ received" << std::endl;
  std::cout << "Packets sent: " << ipackets << std::endl;
  code = ts_clock(&ts2);
  if (code)
  {
    rcc_error_print(stdout, code);
    exit(TmUnresolved);
  }
  elapsed = ts_duration(ts1, ts2);
  std::cout << "Elapsed time: " << elapsed << " seconds" << std::endl;
  std::cout << "Rate: " << (float)ipackets / elapsed << " Hz" << std::endl;
}


/*****************************/
void sigint_handler(int signum)
/*****************************/
{
 // sigint handler
 terminate_it();
 exit(TmPass);
}


/**************/
void usage(void)
/**************/
{
  std::cout << "Valid options are ..." << std::endl;
  std::cout << "-o x: Occurence                                    ->Default: " << occurence << std::endl;
  std::cout << "-S  : SLIDAS emulation (otherwise simulate standard ROD)" << std::endl;
  std::cout << "  Modifiers of -e option:" << std::endl;
  std::cout << "  -O x: Module id (Hex format)                     ->Default: 0x" << std::hex << module_id << std::endl;
  std::cout << "  -D x: Detector id (Hex format)                   ->Default: 0x" << std::hex << detector_id << std::endl;
  std::cout << "  -L x: L1 Trigger type (Hex format)               ->Default: 0x" << std::hex << l1_type << std::endl;
  std::cout << "  -T x: Detector event type (Hex format)           ->Default: 0x" << std::hex << det_type << std::endl;
  std::cout << "-f x: First L1id for ROD emulation                 ->Default: " << std::dec << first_l1id << std::endl;
  std::cout << "-s x: Packet size (in words, incompatible with -r) ->Default: " << std::dec << size << std::endl;
  std::cout << "-r  : Random packet size (incompatible with -s)" << std::endl;
  std::cout << "  Modifiers of -r option:" << std::endl;
  std::cout << "  -M x: Max packet size                            ->Default: " << std::dec << maxsize << std::endl;
  std::cout << "  -m x: Min packet size                            ->Default: " << std::dec << minsize << std::endl;
  std::cout << "-w x: Wait before sending (in seconds)             ->Default: " << std::dec << wait << std::endl;
  std::cout << "-n x: Number of packets                            ->Default: infinite loop" << std::endl;
  std::cout << "-F x: Max. fragment generation frequency (Hz)      ->Default: 0 (run as fast as possible)" << std::endl;
  std::cout << "-p  : Print packets" << std::endl;
  std::cout << "-l x: Do not send every x-th event                 ->Default: 0 (disabled)" << std::endl;
  std::cout << "-v  : Verbose output" << std::endl;
  std::cout << "-e x: Debug level (DFEBUG)                         ->Default: 0 (disabled)" << std::endl;
  std::cout << "-E x: Debug package (DFEBUG)                       ->Default: DFDB_ROSSOLAR" << std::endl;
  std::cout << std::endl;
}


/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  err_type code;
  int c;
  unsigned int nfree, f_size;
  struct sigaction saint, sa;
  unsigned int pci_addr, loop;

  while ((c = getopt(argc, argv,"ho:SO:D:L:T:f:s:rM:m:w:n:pvF:l:e:E:")) != -1)
    switch (c) 
    {
    case 'h':
      std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
      usage();
      exit(TmUnresolved);
      break;

    case 'o':
      occurence = atoi(optarg);
      if (occurence <= 0 || occurence >= MAXCARDS) 
      { 
	std::cout << "Occurence exceeds allowed bounds" << std::endl; 
	exit(TmUnresolved);
      }
      break;

    case 'l': skipevent = atoi(optarg);         break; 
    case 'S': flg_rodemul = FALSE;              break;
    case 'O': sscanf(optarg,"%x",&module_id);   break;
    case 'D': sscanf(optarg,"%x",&detector_id); break;
    case 'L': sscanf(optarg,"%x",&l1_type);     break;
    case 'T': sscanf(optarg,"%x",&det_type);    break;
    case 'f': first_l1id = atoi(optarg);        break;
    case 'w': wait = atoi(optarg);              break;
    case 'p': print = TRUE;                     break;
    case 'v': flg_verbose = TRUE;               break;
    case 'F': freq = atoi(optarg);              break;
    case 'e': dblevel = atoi(optarg);           break;
    case 'E': dbpackage = atoi(optarg);         break;

    case 's':
      flg_size = TRUE;
      size = atoi(optarg);
      if (size < MIN_PACKET_SIZE_WRDS || size > MAX_PACKET_SIZE_WRDS) 
      {
	std::cout << "Wrong packet size: " << size << std::endl << "(acceptable range: " << MIN_PACKET_SIZE_WRDS 
             << " - " << MAX_PACKET_SIZE_WRDS << ")" << std::endl;
	exit(TmUnresolved);
      }
      break;

    case 'r':  
      flg_random = TRUE;
      //init random generation
      srand((unsigned int)time(NULL));
      break;

    case 'M':
      flg_maxsize = TRUE;
      maxsize = atoi(optarg);
      if (maxsize > MAX_PACKET_SIZE_WRDS) 
      {
	std::cout << "Wrong max packet size: " << size << std::endl 
             << "(must be < "  << MAX_PACKET_SIZE_WRDS << ")" << std::endl;
	exit(TmUnresolved);
      }
      break;
 
    case 'm':
      flg_minsize = TRUE;
      minsize = atoi(optarg);
      if (minsize < MIN_PACKET_SIZE_WRDS) 
      {
	std::cout << "Wrong min packet size: " << size << std::endl
             << "(must be > " << MIN_PACKET_SIZE_WRDS << ")" << std::endl;
	exit(TmUnresolved);
      }
      break;
 
    case 'n':
      npackets = atoi(optarg);
      if (npackets<0) 
      {
	std::cout << "number of packets must be positive" << std::endl;
	exit(TmUnresolved);
      } 
      break;

    default:
      std::cout << "Invalid option " << c << std::endl;
      std::cout << "Usage: " << argv[0] << "[options]" << std::endl;
      usage();
      exit (TmUnresolved);
    }


  DF::GlobalDebugSettings::setup(dblevel, dbpackage);


  // Check compatibility of options
  if (flg_random && flg_size) 
  { 
    std::cout << "Options -s and -r are incompatible" << std::endl;
    exit (TmUnresolved);
  }
  
  if ( (flg_minsize || flg_maxsize) && !flg_random) 
  {
    std::cout << "-m and -M options can only be used together with -r option" << std::endl;
    exit (TmUnresolved);
  }

  //wait some time to be sure that slink_dst has started
  sleep(wait);

  code = SOLAR_Open();
  if (code) 
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }
  
  code = SOLAR_Reset(occurence - 1);
  if (code)
  {
    std::cout << "main: SOLAR_Reset() FAILED with error " << std::hex << code << std::endl;
    rcc_error_print(stdout, code);
  }

  fconfig.enable[occurence - 1] = 1;
  fconfig.bswap = 0;
  fconfig.wswap = 0;
  fconfig.scw = 0xB0F00000;
  fconfig.ecw = 0xE0F00000; 
  fconfig.udw = 0;
  fconfig.clksel = 0;
  fconfig.clkdiv = 0;
  fconfig.lffi = 0; // Not yet supported
  fconfig.ldi = 0;  // Not yet supported

  if (flg_verbose) 
  {
    std::cout << "SOLAR Parameters : " << std::dec << std::endl;
    std::cout << "data width       : " << std::dec << fconfig.udw << std::endl;
    std::cout << "occurence        : " << std::dec << occurence << std::endl;
    std::cout << "byte swapping    : " << std::dec << fconfig.bswap << std::endl; 
    std::cout << "word swapping    : " << std::dec << fconfig.wswap << std::endl; 
    std::cout << "start word       : " << std::hex << fconfig.scw << std::endl;
    std::cout << "stop word        : " << std::hex << fconfig.ecw << std::endl;
  }

  code = SOLAR_Init(&fconfig);
  if (code)
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }

  code = SOLAR_LinkReset(occurence - 1);
  if (code)
  {
    rcc_error_print(stdout, code);
    exit (TmFail);
  }

  // Install signal handler for SIGQUIT
  sigemptyset(&sa.sa_mask);
  sa.sa_flags   = 0;
  sa.sa_handler = sigquit_handler;

  // Dont block in intercept handler
  if (sigaction(SIGQUIT, &sa, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit (TmUnresolved);
  }

  // Install signal handler for SIGINT
  sigemptyset(&saint.sa_mask);
  saint.sa_flags   = 0;
  saint.sa_handler = sigint_handler;

  if (sigaction(SIGINT, &saint, NULL) < 0) 
  {
    std::cout << "main: sigaction() FAILED with ";
    code = errno;
    if (code == EFAULT) std::cout << "EFAULT" << std::endl;
    if (code == EINVAL) std::cout << "EINVAL" << std::endl;
    exit(TmUnresolved);
  }

  std::cout << std::endl <<"Press ctrl-\\ to output statistics" << std::endl;
  std::cout << "Press ctrl-c to quit" << std::endl << std::endl;

  //Create a memory pool
  if (flg_verbose) 
  {
    std::cout << "Creating MemoryPool_CMEM" << std::endl;
    std::cout << "Number of pages = " << maxfrags << std::endl;
    std::cout << "Page size = " << std::hex << ROB_POOL_SIZE << " bytes" << std::endl;
  }

  try
  {
    rob_pool = new MemoryPool_CMEM(maxfrags, ROB_POOL_SIZE);
  }
  catch (MemoryPoolException& e)
  {
    std::cout << e << std::endl;
    exit(TmUnresolved);
  }
  
  code = ts_open(1, TS_DUMMY);
  if (code)
  {
    rcc_error_print(stdout, code);
    exit(TmUnresolved);
  }
  code = ts_clock(&ts1);
  if (code)
  {
    rcc_error_print(stdout, code);
    exit(TmUnresolved);
  }

  for(loop = 0; loop < maxfrags; loop++)
    fragused[loop] = 0; 

  // Generate some constant / initial parameters
  sourceId = (module_id & 0xff) | ((detector_id & 0xff) << 8);  //module type = 0 (ROD)
  level1Id = first_l1id - 1;
  fin.nvalid = 1;      
  fin.card = occurence - 1;  //remap from 1-N to 0-(N-1)
  fin.scw[0] = 1;
  fin.ecw[0] = 1;  

  // Event loop
  for (ipackets = 1; npackets == -1 || ipackets <= npackets; ipackets++) 
  {    
    // Generate a ROD fragment and get its PCI address and size
    gen_rod_frag(ipackets, &pci_addr, &f_size); 
    if (flg_verbose)
    std::cout << "pci_addr = 0x" << std::hex << pci_addr << std::dec << std::endl;
    
    // Before we post the request we have to be sure there is space for it
    nfree = 0;
    do
    {      
      code = SOLAR_InFree(occurence - 1, &nfree);
      if (code) 
      {
        rcc_error_print(stdout, code);
        exit(TmFail);	
      }
    }
    while(!nfree);            

    fin.pciaddr[0] = pci_addr;
    fin.size[0] = f_size;

    if (freq)
      wait_a_bit(ipackets);

    //Write packet to slink
    if (flg_verbose)
      std::cout << "Sending packet with L1ID = " << level1Id << " and size = " << (f_size >> 2) << " words" << std::endl;

    code = SOLAR_PagesIn(&fin);
    if (code)                         
    {
      rcc_error_print(stdout, code);
      exit(TmFail);
    }
  }

  code = ts_close(TS_DUMMY);
  if (code)
  {
    rcc_error_print(stdout, code);
    exit(TmUnresolved);
  }
  
  terminate_it();

  exit(TmPass);
}


/**************************************************************/
int gen_rod_frag(u_int ipackets, u_int *pci_addr, u_int *f_size) 
/**************************************************************/  
{
  u_int bcid = 0, numberOfDataElements, statusBlockPosition, d_size, numberOfRODStatusElements, bunchCrossingId;
  
  // Generate the L1ID  
  level1Id++;
  if (skipevent && ((level1Id % skipevent) == 0))  //Skip this event
    level1Id++;
  if (!flg_rodemul)                 // wrap a la SLIDAS
    level1Id &= 0xffff;

  // Generate the BCID a la SLIDAS ... yes I know its wrong  
  if (ipackets == 1) 
    bunchCrossingId = bcid = 0;
  else 
    bunchCrossingId = (bcid += 3) & 0xfff;

  if (!flg_rodemul)
    numberOfRODStatusElements = STATUS_SIZE_SLIDAS;
  else
    numberOfRODStatusElements = STATUS_SIZE;

  // Generate the data size
  if (flg_random) 
  {
    d_size = rand() % (maxsize - minsize + 1);  // generate size    
    if (flg_verbose)
      std::cout << "Random fragment size = " << d_size << std::endl;
  }
  else
  {
    d_size = size - HEADER_SIZE_WRDS - TRAILER_SIZE_WRDS - numberOfRODStatusElements;
    if (flg_verbose)
      std::cout << "fixed fragment size = " << d_size << std::endl;
  }

  numberOfDataElements = d_size;
  statusBlockPosition = 0;

  // Check if the old fragment is to be deleted
  if (fragused[fragindex])
    delete robfragment[fragindex];
    
  // Create an emulated ROB fragment
  // I know that we need a ROD fragment but the ROSEventFragment package should not have to deal with ROD fragments
  try
  {
  robfragment[fragindex] = new ROBFragment(rob_pool, sourceId, level1Id,  bunchCrossingId, l1_type, 
                                           det_type,  numberOfRODStatusElements, numberOfDataElements, statusBlockPosition);
  }
  catch (ROSException& e)
  {
    std::cout << "ROSException:" << std::endl;
    std::cout << e << std::endl;
  }
  catch(...)
  {
    std::cout << "Exception received from <new ROBFragment>" << std::endl;
    exit(TmUnresolved);
  }
  if (flg_verbose)
    std::cout << "Event generated with L1ID = " << level1Id << std::endl;
    
  // Fill the data block
  robfragment[fragindex]->fill_data();  

  // Make some modifications to the ROD fragment
  if (!flg_rodemul) 
    robfragment[fragindex]->emulate_SLIDAS();
    
  *f_size = (d_size + HEADER_SIZE_WRDS + TRAILER_SIZE_WRDS + numberOfRODStatusElements);

  // Get the PCI address of the ROD fragment inside the ROB fragment
  *pci_addr = (unsigned int)robfragment[fragindex]->getPCIaddress() + sizeof(ROBFragment::ROBHeader); 
   
  // Print packet
  if (print)
  {
    std::cout << "Printing packet" << std::endl;
    std::cout << *robfragment[fragindex];
    std::cout << std::endl;
  }
        
  //register the new fragment  
  fragused[fragindex] = 1;  
  fragindex++;
  if (fragindex == maxfrags) 
    fragindex = 0;
    
  return(TmPass);
}


/***************************/
void wait_a_bit(int ipackets)
/***************************/
{ 
  err_type code;
  
  while(1)
  {
    code = ts_clock(&ts2);
    if (code)
    {
      rcc_error_print(stdout, code);
      exit(TmUnresolved);
    }
    elapsed = ts_duration(ts1, ts2);
    if (elapsed > ((float)ipackets * (1.0 / (float)freq)))
      break;
  }
}

