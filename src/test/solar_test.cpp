/************************************************************************/
/*									*/
/* File: solar_test.c							*/
/*									*/
/* This is the test program for the ROSsolar package 			*/
/*									*/
/*  3. Apr. 03  MAJO  created						*/
/*									*/
/**************** C 2003 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "ROSsolar/solar.h"
#include "ROSGetInput/get_input.h"
#include "DFDebug/DFDebug.h"


//Globals
u_int ncard[MAXCARDS], cont, received[MAXCARDS] ={0}, delta[MAXCARDS] ={0}, dblevel = 0, dbpackage = DFDB_ROSSOLAR;
u_long virtbase, pcibase;
static struct sigaction sa, sa2;
SOLAR_info_t finfo;

//Prototypes
int mainhelp(void);
int func_menu(void);
int auto_menu(void);
int man_menu(void);
int sendevents(void);
int setdebug(void);

#ifdef DEBUG
  #define debug(x) printf x
#else
  #define debug(x)
#endif

#define BUFSIZE  (4 * 1024 * 1024)       /*bytes*/

/*****************************/
void SigQuitHandler(int signum)
/*****************************/
{
  debug(("SigQuitHandler: ctrl+// sent\n"));
  cont=0;
  alarm(0);
}


/***************************/
void AlarmHandler(int signum)
/***************************/
{
  int loop;
  
  printf("Number of fragments received:\n");
  for(loop = 0; loop < MAXCARDS; loop++)
  {
    if (ncard[loop])
    {
      received[loop] += delta[loop];
      printf("Card %d: total #=%d    # per second=%d\n", loop, received[loop], delta[loop]/2);
      delta[loop] = 0;
    }
  }
  
  if(cont)
    alarm(2);
}


/****************/
int setdebug(void)
/****************/
{
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  return(0);
}


/************/
int main(void)
/************/
{
  int fun = 3;
  int ret;
  int memhandle;
  u_int loop, *ptr;
 
  printf("\n\n\nThis is the test program for the SOLAR library \n");
  printf("===================================================\n");

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler;
  ret = sigaction(SIGQUIT, &sa, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = AlarmHandler;
  ret = sigaction(SIGALRM, &sa2, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  ret = CMEM_BPASegmentAllocate(BUFSIZE, "SOLAR_BUFFER", &memhandle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  
  ret = CMEM_SegmentVirtualAddress(memhandle, &virtbase);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentPhysicalAddress(memhandle, &pcibase);
  if (ret)
    rcc_error_print(stdout, ret);
 
  printf("Contiguous buffer:\n");
  printf("Physical address = 0x%08x\n", pcibase);
  printf("Virtual address  = 0x%08x\n", virtbase);

  // Initialize the buffer
  ptr = (u_int *) virtbase;
  for(loop = 0; loop < (BUFSIZE >> 2); loop++)
    *ptr++ = loop;

  while (fun != 0)  
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Help                        2 Test functions\n");
    printf("   3 Automatic tests             4 Set debug parameters\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) func_menu();
    if (fun == 3) auto_menu();
    if (fun == 4) setdebug();
  }

  ret = CMEM_BPASegmentFree(memhandle);
  if (ret)
    rcc_error_print(stdout, ret);
 
  ret = CMEM_Close();
  if (ret)
    rcc_error_print(stdout, ret);
   
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
  return(0);
}


/*****************/
int auto_menu(void)
/*****************/
{
  int fun = 1;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a test:\n");
    printf("   1 Send events\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);    
    if (fun == 1) sendevents();
  }
  printf("=========================================================================\n\n");
  return(0);
}


/******************/
int sendevents(void)
/******************/
{      
  SOLAR_ErrorCode_t ret;
  SOLAR_config_t fconfig;
  SOLAR_in_t fin[MAXCARDS];
  static u_int ncards = 1, card = 0, size = 0x100;
  u_int nfree, loop, *ptr;
  
  for (loop = 0; loop < MAXCARDS; loop++)
  {
    ncard[loop] = 0;
    fconfig.enable[loop] = 0;
  }
  
  printf("Enter the number of cards: ");
  ncards = getdecd(ncards);
  
  for(loop = 0; loop < ncards; loop++)
  {
    printf("Enter the number of card %d (0..N): ", loop);
    card = getdecd(card);
    fconfig.enable[card] = 1;    
    ncard[card] = 1;
  }
    
  printf("Enter the block size in words (max %d) : ", (BUFSIZE - 4) / 4);
  size = getdecd(size);
  
  ret = SOLAR_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  
  ret = SOLAR_Reset(0);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
  
  ret = SOLAR_LinkReset(0);   
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }
 
  fconfig.bswap = 0;
  fconfig.wswap = 0;
  fconfig.scw = 0xb0f00000;
  fconfig.ecw = 0xe0f00000; 
  fconfig.udw = 0;
  fconfig.clksel = 0;
  fconfig.clkdiv = 0;
  fconfig.lffi = 0;
  fconfig.ldi = 0;

  ret = SOLAR_Init(&fconfig);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  } 
  
  //Initialize the buffer
  ptr = (u_int *) virtbase;
  for (loop = 0; loop < size; loop++)
    *ptr++ = loop;
  
  for (card = 0; card < MAXCARDS; card++)
  {
    if (fconfig.enable[card])
    {
      fin[card].nvalid = 1; 
      fin[card].card = card; 
      fin[card].pciaddr[0] = pcibase;  //we use same page buffer all the time 
      fin[card].size[0] = size;
      fin[card].scw[0] = 1;
      fin[card].ecw[0] = 1;
      delta[card] = 0;
    }
  }

  cont = 1;
  alarm(2);
  printf("Press crtl+\\ to stop this test\n");
        
  while(cont)
  {  
    for (card = 0; card < MAXCARDS; card++)
    {
      if (fconfig.enable[card])
      {  
        nfree = 0;
        while(!nfree)
        {
          ret = SOLAR_InFree(card, &nfree);
          if (ret)
          {
            rcc_error_print(stdout, ret);
            return(-1);
          }
        }

        ret = SOLAR_PagesIn(&fin[card]);
        if (ret == 0)
          delta[card]++;
        else
        {
          rcc_error_print(stdout, ret);
          return(-1);
        }
      }
    }
  }
 
  ret = SOLAR_Close();
  if (ret)
    rcc_error_print(stdout, ret);
  return(0);
}


/*****************/
int func_menu(void)
/*****************/
{
  static u_int bswap = 0, wswap = 0, udw = 0, scw = 0xb0f00000, ecw = 0xe0f00000;
  static u_int ncards = 1, card = 0, nelem = 1, size = 0x100, clksel = 0, clkdiv = 0;
  int fun = 1;
  u_int loop, nfree;
  SOLAR_ErrorCode_t ret;
  SOLAR_config_t fconfig;
  SOLAR_in_t fin;
  SOLAR_status_t fstat;

  printf("\n=========================================================================\n");
  while (fun != 0)  
  {
    printf("\n");
    printf("Select a function of the API:\n");
    printf("   1 SOLAR_Open               2 SOLAR_Close\n");
    printf("   3 SOLAR_Init               4 SOLAR_Info\n");
    printf("   5 SOLAR_PagesIn            6 SOLAR_InFree\n");
    printf("   7 SOLAR_Reset              8 SOLAR_LinkReset\n");
    printf("   9 SOLAR_LinkStatus\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun); 
     
    if (fun == 1)
    {
      ret = SOLAR_Open();
      if (ret)
	rcc_error_print(stdout, ret);
      ret = SOLAR_Info(&finfo);  // needed later
      if (ret)
	rcc_error_print(stdout, ret);
    }
    
    if (fun == 2) 
    {
      ret = SOLAR_Close();
      if (ret)
	rcc_error_print(stdout, ret);
    }    

    if (fun == 3) 
    {
      for (loop = 0; loop < MAXCARDS; loop++)
        fconfig.enable[loop] = 0;
        
      printf("Enter the number of cards: ");
      ncards = getdecd(ncards);
      
      for (loop = 0; loop < ncards; loop++)
      {
        printf("Enable card %d [1=yes  0=no]: ", loop);
        fconfig.enable[loop] = getdecd(1);
      }
      
      printf("Enable byte swapping (1=yes  0=no): ");
      bswap = getdecd(bswap);
            
      printf("Enable word swapping (1=yes  0=no): ");
      bswap = getdecd(wswap);
     
      printf("Enter the start control word: ");
      scw = gethexd(scw);

      printf("Enter the end control word: ");
      ecw = gethexd(ecw);

      printf("Enter the value for UDW (0..2): ");
      udw = getdecd(udw);

      printf("Enter the value for CLKSEL (1=40 MHz  0=66.6 MHz: ");
      clksel = getdecd(clksel);

      printf("Enter the value for CLKDIV (0..3): ");
      clkdiv = getdecd(clkdiv);
             
      fconfig.bswap = bswap;
      fconfig.wswap = wswap;
      fconfig.scw = scw;
      fconfig.ecw = ecw; 
      fconfig.udw = udw;
      fconfig.clksel = clksel;
      fconfig.clkdiv = clkdiv;
      fconfig.lffi = 0; // Not yet supported
      fconfig.ldi = 0;  // Not yet supported
            
      ret = SOLAR_Init(&fconfig);
      if (ret)
	rcc_error_print(stdout, ret);
    }
    
    if (fun == 4)
    {
      ret = SOLAR_Info(&finfo);  
      if (ret)
	rcc_error_print(stdout, ret);
      else
      {  
        printf("There are %d SOLAR cards installed\n", finfo.ncards);
        for(loop = 0; loop < finfo.ncards; loop++)
          printf("Card %d is %s and %s\n", loop, finfo.cards[loop]?"present":"absent", finfo.enabled[loop]?"enabled":"disabled");
      }
    }
    
    if (fun == 5)
    {
      printf("Enter the number of the card (0..%d, %d=all): ", MAXCARDS -1, MAXCARDS);
      card = getdecd((int)card);
      
      printf("Enter the number of entries to write to the IN FIFO: ");
      nelem = getdecd((int)nelem);
      
      printf("Enter the size of the block in words: ");
      size = getdecd((int)size);
      
      if (nelem > MAXINFIFO)
      {
        printf("You can not write more than %d entries\n", MAXINFIFO);
        return(-1);
      }
      
      fin.nvalid = nelem;      
      fin.card = card;
      for(loop = 0; loop < nelem; loop++)
      { 
        fin.pciaddr[loop] = pcibase;
        fin.size[loop] = size;
        fin.scw[loop] = 1;
        fin.ecw[loop] = 1;  
      }
                  
      if (card < (MAXCARDS))
      {
        ret = SOLAR_PagesIn(&fin);
        if (ret)
	  rcc_error_print(stdout, ret);
      }
      else
      {
        for(card = 0; card < MAXCARDS; card++)
        {
          if (finfo.cards[card])
          {
            fin.card = card;
            ret = SOLAR_PagesIn(&fin);
            if (ret)
	      rcc_error_print(stdout, ret);
          }
        }
      }
    }
    
    if (fun == 6)
    {      
      printf("Which card do you want to probe: ");
      card = getdecd((int)card);

      ret = SOLAR_InFree(card, &nfree);   
      if (ret)
	rcc_error_print(stdout, ret);
        
      printf("The IN FIFO of card %d has %d free slots\n", card, nfree);
    }
        
    if (fun == 7)
    {
      printf("Which card do you want to reset: ");
      card = getdecd((int)card);
      ret = SOLAR_Reset(card);
      if (ret)
	rcc_error_print(stdout, ret);
    }  
    
    if (fun == 8) 
    {
      printf("Which card do you want to reset: ");
      card = getdecd((int)card);
      
      ret = SOLAR_LinkReset(card);   
      if (ret)
	rcc_error_print(stdout, ret);        
      printf("Link has been reset\n");
    }
    
    if (fun == 9) 
    {
      printf("Which card do you want to access: ");
      card = getdecd((int)card);
      
      ret = SOLAR_LinkStatus(card, &fstat);   
      if (ret)
	rcc_error_print(stdout, ret);        
      printf("The link is %s\n", (fstat.ldown)?"down":"up");
      printf("The S/W FIFO contains %d entries\n", fstat.infifo);
      printf("The REQ FIFO contains %d entries\n", fstat.reqfifo);
    }
  }
  printf("=========================================================================\n\n");
  return(0);
}












