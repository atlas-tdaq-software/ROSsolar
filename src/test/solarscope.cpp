/****************************************************************/
/*                                                              */
/*  file: solarscope.c                                          */
/*                                                              */
/* This program allows to access the resources of a SOLAR       */
/* card in a user friendly way and includes some test routines  */
/*                                                              */
/*  Author: Markus Joos, CERN-EP                                */
/*                                                              */
/*  2. Apr. 03  MAJO  created                                   */
/*                                                              */
/****************C 2003 - A nickel program worth a dime**********/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>
#include <stdint.h>
#include "rcc_error/rcc_error.h"
#include "io_rcc/io_rcc.h"
#include "cmem_rcc/cmem_rcc.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSGetInput/get_input.h"

#ifdef DEBUG
  #define debug(x) printf x
#else
  #define debug(x)
#endif

/*prototypes*/
void SigQuitHandler(int signum);
int dumpmem(void);
int solar_map(int occ);
int rodemul(void);
int setlat();
int solar_unmap(void);
int dumpconf(void);
int setreq(void);
int setocr(void);
int setofr(void);
int setpci(void);
int setim(void);    
int setbcw(void);
int setecw(void);    
int setreg(void);
int set_slidasctrl(void);
int uio_init(void);
int uio_exit(void);
int initbuff(void);
int cardreset(void);
int linkreset(void);
int mainhelp(void);
int solarconf(void);
int slidas_config(void);
int slidas_menu(void);
int stream_data(void);
int calc_rate(void);

/*constants*/
#define BUFSIZE       0x400000       /*bytes*/
#define PREFILL       0xfeedbabe
#define MAX_BURST_LEN 1

/*types*/
typedef struct
{
unsigned int opctrl;         /*0x000*/
unsigned int opstat;         /*0x004*/
unsigned int intmask;        /*0x008*/
unsigned int slidasctrl;     /*0x00c*/
unsigned int opfeat;         /*0x010*/
unsigned int swordcnt;       /*0x014*/
unsigned int bctrlw;         /*0x018*/
unsigned int ectrlw;         /*0x01c*/
unsigned int reserved[56];   /*0x020 - 0x0fc*/
unsigned int reqfifo1;       /*0x100*/
unsigned int reqfifo2;       /*0x104*/
}T_solar_regs;


/*globals*/
static u_int isactive = 0, cont, shandle, offset, pkts_sent = 0, delta = 0;
static u_long sreg, paddr, uaddr;
static int bhandle;
static T_solar_regs *solar;
static unsigned int pcidefault[16]=
{
  0x001710dc, 0x00800000, 0x02800001, 0x0000ff00, 0xfffffc00, 0x00000000, 0x00000000, 0x00000000,
  0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x00000000, 0x000001ff
};
static struct sigaction sa, sa2;


/*****************************/
void SigQuitHandler(int signum)
/*****************************/
{
  cont = 0;
  debug(("SigQuitHandler: ctrl+// received\n"));
  alarm(0);
}


/***************************/
void AlarmHandler(int signum)
/***************************/
{
  printf("Number of data blocks sent: ");
  pkts_sent += delta;
  printf("Total #=%d    # per second=%d\n", pkts_sent, delta/2);
  delta = 0;
  if (cont) 
    alarm(2);
}


/********************/
int solar_map(int occ)
/********************/
{
  unsigned int eret, pciaddr;

  eret = IO_Open();
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }

  eret = IO_PCIDeviceLink(0x10dc, 0x0017, occ, &shandle);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  }  

  eret = IO_PCIConfigReadUInt(shandle, 0x10, &pciaddr);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 

  offset = pciaddr & 0xfff;
  pciaddr &= 0xfffff000;
  eret = IO_PCIMemMap(pciaddr, 0x1000, &sreg);
  if (eret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, eret);
    exit(-1);
  } 

  solar = (T_solar_regs *)(sreg + offset); 
  return(0);
}


/*******************/
int solar_unmap(void)
/*******************/
{
  unsigned int eret;

  eret = IO_PCIMemUnmap(sreg, 0x1000);
  if (eret)
    rcc_error_print(stdout, eret);

  eret = IO_Close();
  if (eret)
    rcc_error_print(stdout, eret);
  return(0);
}


/**********/
int setlat()
/**********/
{
  unsigned int eret, latency, data;

  eret = IO_PCIConfigReadUInt(shandle, 0xC, &data);
  if (eret)
  {
    printf(" failed to read register\n");
    return(-1);
  }
  latency = (data >> 8) & 0xff;
  printf("Enter new value: ");
  latency = gethexd(latency);

  data &= 0xffff00ff;
  data |= (latency << 8);
  
  eret = IO_PCIConfigWriteUInt(shandle, 0xC, data);
  if (eret)
  {
    printf(" failed to write register\n");
    return(-1);
  }
  return(0);
}


/****************/
int dumpconf(void)
/****************/
{
  unsigned int loop, eret, data;

  printf("PCI configuration registers:\n\n");
  printf("Offset  |  content     |  Power-up default\n");
  for(loop = 0; loop < 0x40; loop += 4)
  {
    printf("   0x%02x |", loop);
    eret = IO_PCIConfigReadUInt(shandle, loop, &data);      
    if (eret)
      printf(" failed to read register\n");
    else
    {
      printf("   0x%08x |", data);
      if (data == pcidefault[loop >> 2])
        printf(" Yes\n");
      else
        printf(" No (0x%08x)\n", pcidefault[loop >> 2]);
    }
  }
  return(0);
}


/***************/
int dumpmem(void)
/***************/
{
  unsigned int data, value;
  
  printf("\n=============================================================\n");
  
  data = solar->opctrl;
  printf("Operation Control register (0x%08x)\n", data);
  printf("SLIDAS mode:                              %s\n", (data & 0x80000000)?"Enabled":"Disabled");
  printf("UTEST bit:                                %s\n", (data & 0x00100000)?"S-Link test mode":"Normal operation");
  value = (data >> 18) & 0x3;
  printf("User Data Width (UDW):                    ");
  if (value == 0) printf("32 bit\n");
  if (value == 1) printf("16 bit\n");
  if (value == 2) printf("8 bit\n");
  if (value == 3) printf("reserved\n");
  printf("URESET bit:                               %s\n", (data & 0x00020000)?"Reset S-Link card":"Normal operation");
  printf("Word swapping:                            %s\n", (data & 0x00000004)?"Enabled":"Disabled");
  printf("Byte swapping:                            %s\n", (data & 0x00000002)?"Enabled":"Disabled");
  printf("Reset interface:                          %s\n", (data & 0x00000001)?"Active":"Not active");

  data = solar->opstat;
  printf("\nOperation Status register (0x%08x)\n", data);
  value = (data >> 28) & 0xf;
  printf("Number of 64-bit words in output buffer:  ");
  if (value < 0xf) printf("%d\n", value);
  else             printf("15 or more\n");
  value = (data >> 24) & 0xf;
  printf("Number of 64-bit words in PCI burst FIFO: ");
  if (value < 0xf) printf("%d\n", value);
  else             printf("15 or more\n");
  printf("Link return line 3:                       %s\n", (data & 0x00800000)?"Set":"Not set");
  printf("Link return line 2:                       %s\n", (data & 0x00400000)?"Set":"Not set");
  printf("Link return line 1:                       %s\n", (data & 0x00200000)?"Set":"Not set");
  printf("Link return line 0:                       %s\n", (data & 0x00100000)?"Set":"Not set");
  printf("Link Full Flag:                           %s\n", (data & 0x00080000)?"Set":"Not set");
  printf("Link Down:                                %s\n", (data & 0x00040000)?"Set":"Not set");
  printf("Number of blocks sent:                    %d\n", (data >> 6) & 0xfff);
  printf("Number of slots in the request FIFO:      %d\n", data & 0x3f);

  data = solar->intmask;
  printf("\nInterupt mask register (0x%08x)\n", data);
  printf("LFF interrupt:                            %s\n", (data & 0x00400000)?"Enabled":"Disabled");
  printf("Link Down interrupt:                      %s\n", (data & 0x00200000)?"Enabled":"Disabled");
  printf("LRL0 interrupt:                           %s\n", (data & 0x00100000)?"Enabled":"Disabled");
  printf("LRL1 interrupt:                           %s\n", (data & 0x00080000)?"Enabled":"Disabled");
  printf("LRL2 interrupt:                           %s\n", (data & 0x00040000)?"Enabled":"Disabled");
  printf("LRL3 interrupt:                           %s\n", (data & 0x00020000)?"Enabled":"Disabled");
  printf("Level of LRL interrupts:                  %s\n", (data & 0x00010000)?"High":"Low");
  value = data & 0x3f;
  printf("Threshold for REQ_AVAILABLE interrupt:    ");
  if (value) printf("%d\n", value);
  else       printf("Disabled\n");

  data = solar->opfeat;
  printf("\nOptional Feature register (0x%08x)\n", data);
  printf("UCLK frequency:                           %s\n", (data & 0x80000000)?"40 MHz":"66.6 MHz");
  value = (data >> 29) & 0x3;
  printf("UCLK divide:                              ");
  if (value == 0) printf("No division\n");
  if (value == 1) printf("Divide by 2\n");
  if (value == 2) printf("Divide by 4\n");
  if (value == 3) printf("Divide by 8\n");
  printf("Value of TST_DOWN_CYCLES:                 %d\n", (data >> 16) & 0x7);
  printf("Value of RES_DOWN_CYCLES:                 %d\n", (data >> 8) & 0x7);
  printf("Value of LDOWN_CYCLES:                    %d\n", data & 0x7);

  data = solar->bctrlw;
  printf("\nBegin Control Word register (0x%08x)\n", data);
  printf("Insert SENT_BLOCKS into control word      %s\n", (data & 0x1)?"Yes":"No");

  data = solar->ectrlw;
  printf("\nEnd Control Word register (0x%08x)\n", data);
  printf("Insert SENT_BLOCKS into control word      %s\n", (data & 0x1)?"Yes":"No");

  data = solar->swordcnt;
  printf("\nS-LINK word counter register:             0x%08x\n", data);

  data = solar->slidasctrl;
  printf("\nSLIDAS control register (0x%08x)\n", data & 0x0000ffff);
  value = (data >> 12) & 0xf;
  printf("Pattern switch:                           ");
  if (value == 0) printf("Testmode\n");
  if (value == 1) printf("Walking 1\n");
  if (value == 2) printf("Walking 0\n");
  if (value == 3) printf("ffffffff/00000000\n");
  if (value == 4) printf("aaaaaaaa/55555555\n");
  if (value == 5) printf("Random data\n");
  if (value == 6) printf("Random data without CTRL\n");
  if (value == 7) printf("Random CTRL without data\n");
  if (value == 8) printf("ROD blocks 1 length\n");
  if (value == 9) printf("ROD blocks 4 lengths\n");
  if (value > 9 ) printf("Invalid\n");
  
  value = (data >> 8) & 0xf;
  printf("Mode switch:                              ");
  if (value == 0) printf("Run\n");
  if (value == 1) printf("Single word\n");
  if (value == 2) printf("Single block\n");
  if (value == 3) printf("50 kHz\n");
  if (value == 4) printf("75 kHz\n");
  if (value == 5) printf("90 kHz\n");
  if (value == 6) printf("100 kHz\n");
  if (value == 7) printf("110 kHz\n");
  if (value == 8) printf("125 kHz\n");
  if (value == 9) printf("150 kHz\n");
  if (value > 9) printf("Invalid\n");
  
  value = (data >> 4) & 0xf;
  printf("Length switch:                            ");
  if (value == 0) printf("52 Byte (header only)\n");
  if (value == 1) printf("64 Byte\n");
  if (value == 2) printf("256 Byte\n");
  if (value == 3) printf("512 Byte\n");
  if (value == 4) printf("1 kByte\n");
  if (value == 5) printf("2 kByte\n");
  if (value == 6) printf("4 kByte\n");
  if (value == 7) printf("8 kByte\n");
  if (value == 8) printf("16 kByte\n");
  if (value == 9) printf("32 kByte\n");
  if (value > 9) printf("Invalid\n");

  printf("Status LED:                               %s\n", (data & 0x2) ? "Running" : "Stopped");

  printf("=============================================================\n");
  return(0);
}


/**************/
int setreg(void)
/**************/
{
  int fun = 1;

  printf("\n=========================================\n");
  while(fun != 0)
    {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 Operation Control register   2 Optional Feature register\n");
    printf("   3 Begin Control Word register  4 End Control Word register\n");
    printf("   5 Interrupt Mask register      6 PCI config. register\n");
    printf("   7 Request FIFO                 8 SLIDAS control register\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun=getdecd(fun);
    if (fun == 1) setocr();
    if (fun == 2) setofr();
    if (fun == 3) setbcw();
    if (fun == 4) setecw();
    if (fun == 5) setim();
    if (fun == 6) setpci();
    if (fun == 7) setreq();
    if (fun == 8) set_slidasctrl();
    }
  printf("=========================================\n\n");
  return(0);
}


/**************/
int setocr(void)
/**************/
{
  int data;

  data = solar->opctrl;
  printf("Enter the new value for the Operation Control register ");
  data = gethexd(data);
  solar->opctrl = data;
  return(0);
}


/**************/
int setofr(void)
/**************/
{
  int data;

  data = solar->opfeat;
  printf("Enter the new value for the Optional Feature register ");
  data = gethexd(data);
  solar->opfeat = data;
  return(0);
}


/**************/
int setbcw(void)
/**************/
{
  int data;

  data = solar->bctrlw;
  printf("Enter the new value for the Begin Control Word register ");
  data = gethexd(data);
  solar->bctrlw = data;
  return(0);
}


/**************/
int setecw(void)
/**************/
{
  int data;

  data = solar->ectrlw;
  printf("Enter the new value for the End Control Word register ");
  data = gethexd(data);
  solar->ectrlw = data;
  return(0);
}


/*************/
int setim(void)
/*************/
{
  int data;

  data = solar->intmask;
  printf("Enter the new value for the Interrupt Mask register ");
  data = gethexd(data);
  solar->intmask = data;
  return(0);
}


/**********************/
int set_slidasctrl(void)
/**********************/
{
  int data;

  data = solar->slidasctrl;
  printf("Enter the new value for the SLIDAS control register ");
  data = gethexd(data);
  solar->slidasctrl = data;
  return(0);
}


/**************/
int setpci(void)
/**************/
{
  unsigned int eret, offset=0, data;

  printf("Enter the offset of the register (4-byte alligned) ");
  offset = gethexd(offset);
  offset &= 0x3c;

  eret = IO_PCIConfigReadUInt(shandle, offset, &data);      
  if (eret)
  {
    printf(" failed to read register\n");
    return(1);
  }
  printf("Enter new value for this register ");
  data = gethexd(data);

  eret = IO_PCIConfigWriteUInt(shandle, offset, data);      
  if (eret)
  {
    printf(" failed to write register\n");
    return(2);
  }

  return(0);
}


/**************/
int setreq(void)
/**************/
{
  static int ib = 1, ie = 1, size = 10;
  unsigned int data;
  
  data = (solar->opstat) & 0x3f;
  if (!data)
  {
    printf("Sorry. The REQ FIFO is full\n");
    return(-1);
  }
  
  printf("Insert Begin Control Word: ");
  ib = getdecd(ib);
  
  printf("Insert End Control Word: ");
  ie = getdecd(ie);

  printf("Enter the block Length (32 bit words): ");
  size = getdecd(size);
  if (size & 0xfff00000)
  {
    printf("Size is to big. Reduced to 0xfffff words\n");
    size = 0xfffff;
  }
  
  printf("PCI address is 0x%08x\n", paddr);
  
  data = (ib << 31) + (ie << 30) + size;
  printf("writing 0x%08x to 0x%16x\n", paddr, (uintptr_t)&solar->reqfifo1);
  solar->reqfifo1 = paddr;
  printf("writing 0x%08x to 0x%16x\n", data, (uintptr_t)&solar->reqfifo2);
  solar->reqfifo2 = data;

  return(0);
}


/****************/
int uio_init(void)
/****************/
{
  unsigned int *ptr, loop, loop2, eret;

  eret = CMEM_Open();
  if (eret)
  {
    printf("Sorry. Failed to open the cmem_rcc library\n");
    rcc_error_print(stdout, eret);
    exit(6);
  }

  eret = CMEM_BPASegmentAllocate(BUFSIZE, "solar", &bhandle);
//  eret = CMEM_SegmentAllocate(BUFSIZE, "solar", &bhandle);
  if (eret)
  {
    printf("Sorry. Failed to allocate buffer\n");
    rcc_error_print(stdout, eret);
    exit(7);
  }

  eret = CMEM_SegmentVirtualAddress(bhandle, &uaddr);
  if (eret)
  {
    printf("Sorry. Failed to get virtual address for buffer\n");
    rcc_error_print(stdout, eret);
    exit(8);
  }

  eret = CMEM_SegmentPhysicalAddress(bhandle, &paddr);
  if (eret)
  {
    printf("Sorry. Failed to get physical address for buffer\n");
    rcc_error_print(stdout, eret);
    exit(9);
  }

  /*initialise the buffer*/
  ptr = (unsigned int *)uaddr;
  for (loop2 = 0; loop2 < (BUFSIZE >> 2); loop2++)
    *ptr++ = PREFILL;

 return(0);
}


/****************/
int uio_exit(void)
/****************/
{
  unsigned int eret;

  eret = CMEM_BPASegmentFree(bhandle);
  if (eret)
  {
    printf("Warning: Failed to free buffer\n");
    rcc_error_print(stdout, eret);
  }


  eret = CMEM_Close();
  if (eret)
  {
    printf("Warning: Failed to close the CMEM_RCC library\n");
    rcc_error_print(stdout, eret);
  }
  return(0);
}


/****************/
int initbuff(void)
/****************/
{
  static unsigned int mode = 1, data = 0;
  unsigned int loop, *ptr;
 
  printf("\n=========================================\n");

  printf("Select the data pattern:\n");
  printf("1 = fixed pattern\n");
  printf("2 = walking 1\n");
  printf("3 = walking 0\n");
  printf("Your choice: ");
  mode = getdecd(mode);
  
  ptr = (unsigned int *)uaddr;

  if (mode == 1)
  {
    printf("Enter the constant pattern: ");
    data = gethexd(data);
      
    for (loop = 0; loop < (BUFSIZE >> 2); loop++)
      *ptr++ = data;
  }
      
  if (mode == 2)
  {
    data = 0x00000001;     
    for (loop = 0; loop < (BUFSIZE >> 2); loop++)
    {
      *ptr++ = data;
      data = data << 1;
      if (!data)
        data = 0x00000001;
    }
  }
   
  if (mode == 3)
  {
    data = 0x00000001;     
    for (loop = 0; loop < (BUFSIZE >> 2); loop++)
    {
      *ptr++ = ~data;
      data = data << 1;
      if (!data)
        data = 0x00000001;
    }
  }   

  printf("=========================================\n\n");
  return(0);
}


/*****************/
int cardreset(void)
/*****************/
{
  unsigned int data;

  data = solar->opctrl;
  data |= 0x1;
  solar->opctrl = data;
  sleep(1);
  data &= 0xfffffffe;
  solar->opctrl = data;
  isactive = 0;
  return(0);
}


/*****************/
int linkreset(void)
/*****************/
{
  unsigned int status;

  /* clear the URESET bit to make sure that there is a falling edge on URESET_N */
  solar->opctrl &= 0xfffdffff;
  /*set the URESET bits*/
  solar->opctrl |= 0x00020000;
  ts_delay(20000); /* wait for 20 ms to give the link time to come up */


  /*now wait for LDOWN to come up again*/
  printf("Waiting for link to come up...\n");
  while((status = solar->opstat) & 0x00040000)
    printf("solar->opstat = 0x%08x\n", status);

  /*reset the URESET bits*/
  solar->opctrl &= 0xfffdffff;
  return(0);
}


/****************/
int mainhelp(void)
/****************/
{
  printf("Call Markus Joos, 72364, 160663 if you need help\n");
  return(0);
}


/*****************/
int solarconf(void)
/*****************/
{
  static unsigned int udw = 0, swapw = 0, swapb = 0, lffi = 0, ldi = 0, reqi = 0;
  static unsigned int bcw = 0xb0f00000, ecw = 0xe0f00000, clksel = 0, clkdiv = 0;
  
  printf("=============================================================\n");

  printf("Enter the UDW mode (0..3): ");
  udw = getdecd(udw);
  udw &= 0x3;
  
  printf("Enable word swapping (1=yes  0=no): ");
  swapw = getdecd(swapw);
  
  printf("Enable byte swapping (1=yes  0=no): ");  
  swapb = getdecd(swapb);
   
  printf("Enable the LFF interrupt: ");
  lffi = getdecd(lffi);

  printf("Enable the LDOWN interrupt: ");
  ldi = getdecd(ldi);

  printf("Set the REQ FIFO interrupt threshold: ");
  reqi = getdecd(reqi);

  printf("Enter the Begin Control Word: ");
  bcw = gethexd(bcw);

  printf("Enter the End Control Word: ");
  ecw = gethexd(ecw);

  printf("Enter the UCLK selection (1=40 MHz  0=66.6 MHz): ");
  clksel = getdecd(clksel);

  printf("Enter the UCLK division (0..3): ");
  clkdiv = getdecd(clkdiv); 
  clkdiv &= 0x3;
  
  solar->opctrl = (udw << 18) + (swapw << 2) + (swapb << 1);
  solar->intmask = (lffi << 22) + (ldi << 21) + (reqi & 0x3f);
  solar->bctrlw = bcw & 0xfffffffc;
  solar->ectrlw = ecw & 0xfffffffc;
  solar->opfeat = (clksel << 31) + (clkdiv << 29);

  printf("\n=============================================================\n");
  return(0);
}


/*******************/
int slidas_menu(void)
/*******************/
{
  int fun = 1;

  solar->opctrl |= 0x80000000; /* enable SLIDAS mode */
  printf("\n=============================================================\n");
  while(fun != 0) 
  {
    printf("\n");
    printf("Select an option:\n");
    printf("  1 Configure SLIDAS          2 Press RUN/STOP\n");
    printf("  3 Dump memory registers     4 (Re)start ROD emulator\n");
    printf("  5 Calculate data rate\n");
    printf("  0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) slidas_config();
    if (fun == 2) 
    {
      solar->slidasctrl |= 1; /* set the START/STOP bit (self-clearing) */
      if (isactive)
      {
        printf("Data generation disabled\n");
        isactive = 0;
      }
      else
      {
        printf("Data generation enabled\n");
        isactive = 1;
      }
    }
    if (fun == 3) dumpmem();
    if (fun == 4) rodemul();    
    if (fun == 5) calc_rate();
  }
  printf("\n=============================================================\n");
  solar->opctrl &= 0x7FFFFFFF; /* disable SLIDAS mode */
  return(0);
}


/***************/
int rodemul(void)
/***************/
{
  unsigned int dummy, pattern, mode, length;

  solar->opctrl &= 0x7FFFFFFF; /* disable SLIDAS mode */
  cardreset();
  sleep(1);
  linkreset();
  solar->opctrl |= 0x80000000; /* enable SLIDAS mode */
  pattern = 8;
  mode    = 0;
  length  = 4;
  solar->slidasctrl = (pattern << 12) | (mode << 8) | (length << 4);
  sleep(1);
  solar->slidasctrl |= 1; /* set the START/STOP bit (self-clearing) */
  printf("Press <return> to exit\n");
  dummy = getdecd(0);
  cardreset();
  linkreset();
  return(0);
}


/*********************/
int slidas_config(void)
/*********************/
{
  static unsigned int pattern = 0, length = 0, mode = 0;
  
  printf("=============================================================\n");
  printf("Select the pattern switch (SW1) setting:\n");
  printf("  0 = Testmode                  1 = Walking 1\n");
  printf("  2 = Walking 0                 3 = FFFF/0000\n");
  printf("  4 = AAAA/5555                 5 = Random data\n");
  printf("  6 = Random data, no control   7 = Random control\n");
  printf("  8 = ROD blocks, 1 lenght      9 = ROD blocks, 3 lengths\n");
  printf("Your choice ");
  pattern = getdecd(pattern);
  pattern &= 0xf;

  printf("=============================================================\n");
  printf("Select the mode switch (SW2) setting:\n");
  printf("  0 = RUN                       1 = Single word\n");
  printf("  2 = Single block              3 = 50 kHz\n");
  printf("  4 = 75 kHz                    5 = 90 kHz\n");
  printf("  6 = 100 kHz                   7 = 110 kHz\n");
  printf("  8 = 125 kHz                   9 = 150 kHz\n");
  printf("Your choice ");
  mode = getdecd(mode);
  mode &= 0xf;

  printf("=============================================================\n");
  printf("Select the length switch (SW3) setting:\n");
  printf("  0 =  52 byte (header only)      1 =  64 byte\n");
  printf("  2 = 256 byte                    3 = 512 byte\n");
  printf("  4 =   1 kbyte                   5 =   2 kbyte\n");
  printf("  6 =   4 kbyte                   7 =   8 kbyte\n");
  printf("  8 =  16 kbyte                   9 =  32 kbyte\n");
  printf("Your choice ");
  length = getdecd(length);
  length &= 0xf;

  solar->slidasctrl = (pattern << 12) | (mode << 8) | (length << 4);

  printf("=============================================================\n");
  return(0);
}


/*****************/
int calc_rate(void)
/*****************/
{
  u_int start, stop;
  double rate;

  start = solar->swordcnt;
  sleep(2);
  stop = solar->swordcnt;
  rate = (stop - start) / 2E6;
  printf("Data rate = %.3f Mwords/sec\n", rate);
  return(0);
}


/*******************/
int stream_data(void)
/*******************/
{
  static unsigned int no_pkts = 0, ib = 1, ie = 1, size = 256;
  unsigned int i, data, rmode, loop, req_free, burst_len;
  unsigned int req_fifo[2 * MAX_BURST_LEN] __attribute__ ((aligned));
    
  printf("Insert Begin Control Word: ");
  ib = getdecd(ib);
  
  printf("Insert End Control Word: ");
  ie = getdecd(ie);

  printf("Enter the block length (32 bit words): ");
  size = getdecd(size);
  if (size & 0xfff00000)
  {
    printf("Size is to big. Reduced to 0xfffff words\n");
    size = 0xfffff;
  }

  initbuff(); /* initialize the data pattern */

  printf("How many packets do you want to send? (0 = run forever): ");
  no_pkts = getdecd(no_pkts);
  if (!no_pkts)
    rmode = 1;
  else
    rmode = 0;

  /* initialize the FIFO buffer */
  printf("PCI address is 0x%08x\n", paddr);
  for (i = 0; i < 2*MAX_BURST_LEN;) 
  {
    req_fifo[i] = paddr;
    i++;
    data = (ib << 31) + (ie << 30) + size;
    req_fifo[i] = data;
    i++;
  }
  
  loop = 0;
  cont = 1;
  pkts_sent = 0;
  delta = 0;
  burst_len = (!rmode && no_pkts < MAX_BURST_LEN) ? no_pkts : MAX_BURST_LEN;
  if (rmode) 
  {
    printf("Running! Press <ctrl+\\> when finished\n");
    alarm(2);
  }
  while(cont) 
  {
    if (loop == 0) 
      printf("Sending data...\n"); 
    loop++;
    if (((solar->opstat) & 0x3f) >= burst_len) 
    {        // check that there are MAX_BURST_LEN entries free in the REQ FIFO
      memcpy(&solar->reqfifo1, req_fifo, 8*burst_len); // Write burst_len entries to the request FIFO */
      delta += burst_len;
      if (!rmode) 
      {
      	no_pkts -= burst_len;
	burst_len = (no_pkts < MAX_BURST_LEN) ? no_pkts : MAX_BURST_LEN;
      	if (no_pkts < 1) break;
      }
    }
  }
  alarm(0);
  printf("%d packets sent.\n", (rmode) ? pkts_sent : delta);
  return(0);
}


/******************************/
int main(int argc, char *argv[])
/******************************/
{
  static int ret, fun = 1, occ = 1;
  static u_int data;

  if ((argc == 2) && (sscanf(argv[1], "%d", &occ) == 1)) {argc--;} else {occ = 1;}
  if (argc != 1)
  {
    printf("This is SOLARSCOPE. \n\n");
    printf("Usage: solarscope [SOLAR occurrence]\n");
    exit(0);
  }

  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = SigQuitHandler;
  ret = sigaction(SIGQUIT, &sa, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  sigemptyset(&sa2.sa_mask);
  sa2.sa_flags = 0;
  sa2.sa_handler = AlarmHandler;
  ret = sigaction(SIGALRM, &sa2, NULL);
  if (ret < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", ret);
    exit(0);
  }
  
  ts_open(1, TS_DUMMY);
  uio_init();
  solar_map(occ);

  ret = IO_PCIConfigReadUInt(shandle, 0x8, &data);
  if (ret != IO_RCC_SUCCESS)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }


  printf("\n\n\nThis is SOLARSCOPE running on a card of revision %02x\n", data & 0xff);
  while(fun != 0)
  {
    printf("\n");
    printf("Select an option:\n");
    printf("  1 Print help           2 Dump PCI conf. space  3 Dump PCI MEM registers\n");
    printf("  4 Write to a register  5 Configure SOLAR       6 Reset the SOLAR\n");
    printf("  7 Reset the S-Link     8 Initialize the buffer 9 Set latency counter\n");
    printf(" 10 SLIDAS menu         11 Stream S-LINK data\n");
    printf("  0 Quit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1) mainhelp();
    if (fun == 2) dumpconf();
    if (fun == 3) dumpmem();
    if (fun == 4) setreg();
    if (fun == 5) solarconf();
    if (fun == 6) cardreset();
    if (fun == 7) linkreset();
    if (fun == 8) initbuff();
    if (fun == 9) setlat();
    if (fun == 10) slidas_menu();
    if (fun == 11) stream_data();
  }

  solar_unmap();
  uio_exit();
  ts_close(TS_DUMMY);
  exit(0);
}


